import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CIcon from '@coreui/icons-react';
import '../AlbumTypeList/style.css'
import { useSelector } from 'react-redux';
import { useTranslate } from 'react-redux-multilingual'

import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CCollapse,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CSwitch,
  CTooltip
  // CPagination
} from '@coreui/react'
import { Link } from 'react-router-dom';

AlbumTypeList.propTypes = {
  albumTypeList: PropTypes.array,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
};

AlbumTypeList.defaultProps = {
  albumTypeList: [],
  onEditClick: null,
  onRemoveClick: null,
};

const getBadge = status => {
  switch (status) {
    case 1: return 'success'
    case true: return 'success'
    case 0: return 'danger'
    case false: return 'danger'
    default: return 'primary'
  }
}

function AlbumTypeList(props) {
  const { albumTypeList, onEditClick, onRemoveClick, onChangeStatusClick } = props;
  const t = useTranslate()
  const handleEditClick = (item) => {
    if (onEditClick) onEditClick(item);
    localStorage.setItem("formValues", JSON.stringify(item));
  }

  const [itemSelected, setItemSelected] = useState();
  const handleRemoveClick = () => {
    if (onRemoveClick) onRemoveClick(itemSelected);
    setModal(!modal);
  }

  const [modal, setModal] = useState(false);
  const toggle = (item) => {
    setItemSelected(item)
    setModal(!modal);
  }

  const handleChangeStatusClick = (item) => {
    if (onChangeStatusClick) onChangeStatusClick(item);
  }

  const fields = [
    // {
    //   key: 'checkBox',
    //   label: '',
    //   _style: { width: '3%' },
    //   sorter: false,
    //   filter: false
    // },
    {
      key: 'No',
      label: t('no'),
      _classes: 'text-center',
      _style: { width: '5%' },
      sorter: false,
    },
    { key: 'name',
    label: t('name'), },
    {
      key: 'description',
      label: t('description'),
      sorter: false,
    },
    {
      key: 'status',
      label: t('status'),
      _style: { width: '9%' },
      sorter: false,
      filter: false
    },
    {
      key: 'functions',
      label: t('actions'),
      _style: { width: '1%' },
      sorter: false,
      filter: false
    }
  ]

  const [selects, setSelects] = useState([])
  const toggleSelects = (item) => {
    const position = selects.indexOf(item)

    let newSelects = selects.slice()
    if (position !== -1) {
      newSelects.splice(position, 1)
    } else {
      newSelects = [...selects, item]
    }
    setSelects(newSelects)
  }

  const loading = useSelector(state => state.albumTypes.loading)
  return (
    <>
      <CModal
        show={modal}
        onClose={toggle}
      >
        <CModalHeader closeButton>{t('deleteAlbumType')}</CModalHeader>
        <CModalBody>
        {t('confirmDeleteAlbumType')} "{itemSelected?.name}"?
  </CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveClick()} color="primary">{t('delete')}</CButton>{' '}
          <CButton
            color="secondary"
            onClick={toggle}
          >{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CRow>
        <CCol xl={12}>
          <CCard>
            <CCardHeader>
              <h4 className="d-inline-block">{t('listAlbumType')}</h4>
              <Link to={{ pathname: `/album/albumTypes/add` }}>
                <CTooltip content={t('addAlbumType')} placement="left">
                <CButton size="sm" color="success" className="ml-1 mr-3 float-right">
                  <i className="fas fa-plus"></i>
                </CButton>
                </CTooltip>
              </Link>
            </CCardHeader>
            <CCardBody>
              {/* {loading ? (<div className="text-center">
                <CSpinner variant="grow" />
              </div>) :  */}
              <CDataTable
                items={albumTypeList}
                fields={fields}
                hover
                pagination
                // tableFilter
                // sorter
                // clickableRows
                loading={loading}
                // itemsPerPageSelect
                // itemsPerPage={10}
                scopedSlots={{
                  // 'checkBox': (item) => {
                  //   return (
                  //     <>
                  //       <td>
                  //         <CFormGroup>
                  //           <CInputCheckbox className="ml-2 cursor-pointer" onClick={() => toggleSelects(item.id)} />
                  //         </CFormGroup>
                  //       </td>
                  //     </>
                  //   )
                  // },
                  'No': (item, index) => (
                    <td className="text-center">
                      {index + 1}
                    </td>
                  ),
                  'status': (item) => (
                    <td>
                      <CBadge color={getBadge(item.status)}>
                        {item.status ? t('active') : t('inactive')}
                      </CBadge>
                    </td>
                  ),
                  'functions':
                    (item, index) => {
                      return (
                        <td className="py-2">
                          <CCollapse className="d-inline-flex">
                          <CSwitch onClick={() => handleChangeStatusClick(item)}
                              className={'mx-1 mt-1 '} shape={'pill'} size={'sm'} color={'warning'}
                              checked={item.status} onChange={() => { }} />
                              <CTooltip content={t('editAlbumType')} placement="top">
                            <CButton onClick={() => handleEditClick(item)} size="sm" color="info" className="ml-1">
                              <CIcon size={'sm'} name="cilPencil" />
                            </CButton>
                            </CTooltip>
                            <CTooltip content={t('deleteAlbumType')} placement="top">
                            <CButton onClick={() => toggle(item)} size="sm" color="danger" className="ml-1">
                              <CIcon size={'sm'} name="cilTrash" />
                            </CButton>
                            </CTooltip>
                          </CCollapse>
                        </td>
                      )
                    },
                }}
              />
              {/* } */}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default AlbumTypeList;
