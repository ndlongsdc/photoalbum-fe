import AlbumTypeForm from '../../components/AlbumTypeForm';
import { addAlbumTypes, updateAlbumTypes } from '../../albumTypeSlice';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

AddEditPage.propTypes = {};

function AddEditPage(props) {
  const dispatch = useDispatch();
  const { id } = useParams();
  const error = useSelector(state => state.albumTypes.error)
  const handleSubmit = async (values) => {
    await id !== 'add' ? dispatch(updateAlbumTypes(values.id,values)) : dispatch(addAlbumTypes(values));

    !error && props.history.push('/album/albumTypes');
  }


  return (
    <AlbumTypeForm
      initialValues={props.location.state !== undefined ? props.location.state.albumType : {
        name: '',
        key: '',
        method: '',
        path: '',
      }}
      onSubmit={handleSubmit}
    />
  );
}

export default AddEditPage;
