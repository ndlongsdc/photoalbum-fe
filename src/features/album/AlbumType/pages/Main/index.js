import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteAlbumTypes, fetchAlbumTypes, changeStatusAlbumTypes } from '../../albumTypeSlice';
import AlbumTypeList from '../../components/AlbumTypeList';
import { useHistory } from 'react-router-dom';

MainPage.propTypes = {};

function MainPage(props) {
  const dispatch = useDispatch();
  const albumTypes = useSelector(state => state.albumTypes.albumTypes);
  const history = useHistory();

  useEffect(() => {
    (props.location.state === undefined || props.location.state === null) && albumTypes.length===0 && dispatch(fetchAlbumTypes());
  }, [dispatch])

  const onEditClick = (albumType) => {

    history.push({
      state: {albumType},
      pathname: `/album/albumTypes/${albumType.id}`
    });
  }

  const onRemoveClick = (albumType) => {
    const removeAlbumTypeId = albumType.id;
    const action = deleteAlbumTypes(removeAlbumTypeId);
    dispatch(action);
  }

  const onChangeStatusClick = (albumType) => {
    const action = changeStatusAlbumTypes(albumType.id);
    dispatch(action);
  }

  return (
    <>
      <AlbumTypeList
        albumTypeList={albumTypes}
        onEditClick={onEditClick}
        onRemoveClick={onRemoveClick}
        onChangeStatusClick={onChangeStatusClick}
      />
    </>
  );
}

export default MainPage;
