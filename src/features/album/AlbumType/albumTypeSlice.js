import { createSlice } from '@reduxjs/toolkit';
import albumTypeApi from '../../../api/albumTypeApi'
import { setToast, clearToast } from '../../../custom_toast/toastSlice'

const albumType = createSlice({
  name: 'albumTypes',
  initialState: {
    albumTypes: [],
    albumType: '',
    error: '',
    loading: false,
  },
  reducers: {
    fetchAlbumTypesStart: (state, action) => {
      state.loading = true
    },
    fetchAlbumTypesSuccess: (state, action) => {
      state.loading = false
      state.albumTypes = action.payload
    },
    fetchAlbumTypesFailure: (state, action) => {
      state.error = action.payload
      // localStorage.clear();
    },
    fetchAlbumTypeByIdStart: (state, action) => {
      state.loading = true
    },
    fetchAlbumTypeByIdSuccess: (state, action) => {
      state.loading = false
      state.albumType = action.payload
    },
    fetchAlbumTypeByIdFailure: (state, action) => {
      state.error = action.payload
      // localStorage.clear();
    },
    addAlbumTypeStart: (state, action) => {
      state.loading = true
    },
    addAlbumTypeSuccess: (state, action) => {
      state.loading = false;
      state.albumTypes.unshift(action.payload)
    },
    addAlbumTypeFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    updateAlbumTypeStart: (state, action) => {
      state.loading = true
    },
    updateAlbumTypeSuccess: (state, action) => {
      state.error = false;
      state.loading = false;
      const foundIndex = state.albumTypes.findIndex(({ id }) => id === action.payload.id)
      state.albumTypes.splice(foundIndex, 1, action.payload)
    },
    updateAlbumTypeFailure: (state, action) => {
      state.error = action.payload
    },
    deleteAlbumTypeStart: (state, action) => {
      state.loading = true
    },
    deleteAlbumTypeSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.albumTypes.findIndex(({ id }) => id === action.payload)
      state.albumTypes.splice(foundIndex, 1)
    },
    deleteAlbumTypeFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
    changeStatusAlbumTypeStart: (state, action) => {
      state.loading = true
    },
    changeStatusAlbumTypeSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.albumTypes.findIndex(({ id }) => id === action.payload.id)
      state.albumTypes.splice(foundIndex, 1, action.payload)
    },
    changeStatusAlbumTypeFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
  }
});

const { reducer, actions } = albumType;
export const {
  fetchAlbumTypesStart, fetchAlbumTypesSuccess, fetchAlbumTypesFailure,
  fetchAlbumTypeByIdStart, fetchAlbumTypeByIdSuccess, fetchAlbumTypeByIdFailure,
  addAlbumTypeStart, addAlbumTypeSuccess, addAlbumTypeFailure,
  updateAlbumTypeStart, updateAlbumTypeSuccess, updateAlbumTypeFailure,
  deleteAlbumTypeStart, deleteAlbumTypeSuccess, deleteAlbumTypeFailure,
  changeStatusAlbumTypeStart, changeStatusAlbumTypeSuccess, changeStatusAlbumTypeFailure,
} = actions;
export const fetchAlbumTypes = () => async dispatch => {
  try {
    dispatch(fetchAlbumTypesStart());
    const response = await albumTypeApi.getAll();
    if (response.status = 'success') {
      localStorage.setItem("albumTypesOption", JSON.stringify(response.data.albumType))
      dispatch(fetchAlbumTypesSuccess(response.data.albumType))
    } else {
      dispatch(fetchAlbumTypesFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(fetchAlbumTypesFailure("error"));
    const valueToast = {
      message: 'Access token has expired',
      type: 'error',
      status: 401
    }
    dispatch(setToast(valueToast));
  }
  // dispatch(clearToast());
}
export const fetchAlbumTypeById = (id) => async dispatch => {
  try {
    dispatch(fetchAlbumTypeByIdStart());
    const response = await albumTypeApi.get(id);
    if (response.status = 'success') {
      dispatch(fetchAlbumTypeByIdSuccess(response.data.albumTypeById))
    } else {
      dispatch(fetchAlbumTypeByIdFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(fetchAlbumTypeByIdFailure(error));
  }
}
export const addAlbumTypes = (body) => async dispatch => {
  try {
    dispatch(addAlbumTypeStart());
    const response = await albumTypeApi.create(body);
    let valueToast = {}
    if (response.success === true) {
      dispatch(addAlbumTypeSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(addAlbumTypeFailure(response.error));
      const messages = response.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(addAlbumTypeFailure(error));
  }
  dispatch(clearToast());
}
export const updateAlbumTypes = (id, body) => async dispatch => {
  try {
    dispatch(updateAlbumTypeStart());
    const response = await albumTypeApi.update(id, body);
    let valueToast = {}
    if (response.success === true) {
      dispatch(updateAlbumTypeSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(updateAlbumTypeFailure(response.message));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(updateAlbumTypeFailure(error));
  }
  dispatch(clearToast());
}
export const deleteAlbumTypes = (id) => async dispatch => {
  try {
    dispatch(deleteAlbumTypeStart());
    const response = await albumTypeApi.delete(id);
    let valueToast = {}
    if (response.success) {
      dispatch(deleteAlbumTypeSuccess(id));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deleteAlbumTypeFailure(id));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deleteAlbumTypeFailure(error));
  }
  dispatch(clearToast());
}
export const changeStatusAlbumTypes = (id) => async dispatch => {
  try {
    dispatch(changeStatusAlbumTypeStart());
    const response = await albumTypeApi.changeStatusAlbumType(id);
    console.log(response);
    let valueToast = {}
    if (response.success) {
      dispatch(changeStatusAlbumTypeSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changeStatusAlbumTypeFailure(response.message));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeStatusAlbumTypeFailure('error'));
  }
  dispatch(clearToast());
}

export default reducer;
