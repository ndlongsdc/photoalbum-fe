import UserForm from '../../components/UserForm';
import { updateUsers, addUsers } from '../../userSlice';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

AddEditPage.propTypes = {};

function AddEditPage(props) {
  const users = useSelector(state => state.users.users);
  const dispatch = useDispatch();
  const { id } = useParams();

  const handleSubmit = async (values) => {
    await id !== 'add' ? dispatch(updateUsers(id, values)) : dispatch(addUsers(values));
  }

  const initialValues = users;
  const formValues = JSON.parse(localStorage.getItem("formValues"));
  const saveValue = formValues !== undefined && formValues !== null ? formValues : {
    password: '',
    fullname: '',
    email: '',
    birthday: '',
    // gender: true,
    phone: '',
    address: '',
    avatar: '',
    role: '1',
    status: '1',
  }
  return (
    <>
      <UserForm
        initialValues={id === 'add' ? initialValues : saveValue}
        // initialValues={props.location.state !== undefined ? initialValues : saveValue}
        onSubmit={handleSubmit}
      />
    </>
  );
}

export default AddEditPage;
