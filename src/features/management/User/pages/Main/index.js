import React, { useEffect, Suspense } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteUsers, deleteMultiUsers, fetchUsers, changeStatusUsers } from '../../userSlice';
import UserList from '../../components/UserList';
import { useHistory } from 'react-router-dom';

MainPage.propTypes = {};

function MainPage(props) {
  const dispatch = useDispatch();
  const users = useSelector(state => state.users.users.data);
  const userData = useSelector(state => state.users.users);
  const history = useHistory();
  const checkLanguage = localStorage.getItem("localeAPI") !== localStorage.getItem("locale");
  useEffect(() => {
    // checkLanguage && dispatch(fetchBusinesses());
    if(!users || checkLanguage){ dispatch(fetchUsers());}

    // userData.length === 0 && dispatch(fetchUsers());
    // // (props.location.state === undefined || props.location.state === null) && userData.length === 0 && dispatch(fetchUsers());
    // checkLanguage && dispatch(fetchUsers());
  }, [dispatch])
  // if (checkLanguage) dispatch(fetchUsers());

  const onEditClick = (user) => {
    history.push({
      state: { user },
      pathname: `/management/users/${user.id}`
    });
  }

  const onRemoveClick = (user) => {
    const removeUserId = user.id;
    const action = deleteUsers(removeUserId);
    dispatch(action);
    dispatch(fetchUsers());
  }

  const onRemoveMultiClick = (ids) => {
    const action = deleteMultiUsers(ids);
    dispatch(action);
    dispatch(fetchUsers());
  }

  const onChangeStatusClick = (user) => {
    const action = changeStatusUsers(user.id);
    dispatch(action);
  }

  return (
    <>
      <Suspense
        fallback={<h1>Loading profile...</h1>}
      >
        <UserList
          userList={users}
          onEditClick={onEditClick}
          onRemoveClick={onRemoveClick}
          onRemoveMultiClick={onRemoveMultiClick}
          onChangeStatusClick={onChangeStatusClick}
        />
      </Suspense>
    </>
  );
}

export default MainPage;
