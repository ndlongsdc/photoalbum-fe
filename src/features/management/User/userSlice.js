import { createSlice } from '@reduxjs/toolkit';
import userApi from '../../../api/userApi'
import { setToast, clearToast } from '../../../custom_toast/toastSlice'
import { post } from 'axios';
const API_ENDPOINT = process.env.REACT_APP_API_URL;

const user = createSlice({
  name: 'users',
  initialState: {
    users: [],
    error: '',
    valueSearch:'',
    loading: false
  },
  reducers: {
    fetchUsersStart: (state, action) => {
      state.loading = true
    },
    fetchUsersSuccess: (state, action) => {
      state.loading = false
      state.users = action.payload
    },
    fetchUsersFailure: (state, action) => {
      state.error = action.payload
    },
    addUserStart: (state, action) => {
      state.loading = true
    },
    addUserSuccess: (state, action) => {
      state.loading = false;
      state.users.data.unshift(action.payload)
    },
    addUserFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
    updateUserStart: (state, action) => {
      state.loading = true
    },
    updateUserSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.users?.data.findIndex(({ id }) => id === action.payload.id)
      state.users.data.splice(foundIndex, 1, action.payload)
    },
    updateUserFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
    deleteUserStart: (state, action) => {
      state.loading = true
    },
    deleteUserSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.users?.data.findIndex(({ id }) => id === action.payload)
      state.users.data.splice(foundIndex, 1)
    },
    deleteUserFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
    deleteMultiUserStart: (state, action) => {
      state.loading = true
    },
    deleteMultiUserSuccess: (state, action) => {
      const ids = action.payload.split(",");
      ids.map((itemId) => {
        const foundIndex = state.users?.data.findIndex(({ id }) => id === itemId)
        state.users.data.splice(foundIndex, 1)
        return null;
      })
      state.loading = false;
    },
    deleteMultiUserFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    changeStatusUserStart: (state, action) => {
      state.loading = true
    },
    changeStatusUserSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.users?.data.findIndex(({ id }) => id === action.payload.id)
      state.users.data.splice(foundIndex, 1, action.payload)
    },
    changeStatusUserFailure: (state, action) => {
      state.error = action.payload
    },
    resetPasswordUserStart: (state, action) => {
      state.loading = true
    },
    resetPasswordUserSuccess: (state, action) => {
      state.loading = false;
    },
    resetPasswordUserFailure: (state, action) => {
      state.loading = false;
    },
    searchUsersStart: (state, action) => {
      state.loading = true
    },
    searchUsersSuccess: (state, action) => {
      state.valueSearch = action.payload
    },
    searchUsersFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    sortUsersStart: (state, action) => {
      state.loading = true
    },
    sortUsersSuccess: (state, action) => {
      state.loading = false
      state.users = action.payload
    },
    sortUsersFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
  }
});

const { reducer, actions } = user;
export const {
  fetchUsersStart, fetchUsersSuccess, fetchUsersFailure,
  addUserStart, addUserSuccess, addUserFailure,
  updateUserStart, updateUserSuccess, updateUserFailure,
  deleteUserStart, deleteUserSuccess, deleteUserFailure,
  deleteMultiUserStart, deleteMultiUserSuccess, deleteMultiUserFailure,
  changeStatusUserStart, changeStatusUserSuccess, changeStatusUserFailure,
  resetPasswordUserStart, resetPasswordUserSuccess, resetPasswordUserFailure,
  searchUsersStart, searchUsersSuccess, searchUsersFailure,
  sortUsersStart, sortUsersSuccess, sortUsersFailure,
} = actions;
export const fetchUsers = (params) => async dispatch => {
  try {
    dispatch(fetchUsersStart());
    const response = await userApi.get(params);
    if (response.success) {
      dispatch(fetchUsersSuccess(response.data.users))
    } else {
      dispatch(fetchUsersFailure(response.message));
    }
  } catch (error) {
    // const valueToast = {
    //   message: 'Access token has expired',
    //   type: 'error',
    //   status: 401
    // }
    // dispatch(setToast(valueToast));
    dispatch(fetchUsersFailure('error'));
  }
  dispatch(clearToast());
}
export const addUsers = (body) => async dispatch => {
  try {
    dispatch(addUserStart());
    delete body.id;
    const formData = new FormData();
    Object.keys(body).forEach(key => formData.append(key, body[key]));
    const url = API_ENDPOINT + '/users';
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: 'Bearer ' + localStorage.getItem("auth:access_token")
      }
    }
    const response = await post(url, formData, config)
    let valueToast = {}
    if (response.data.success === true) {
      dispatch(addUserSuccess(response.data.data[0]));
      valueToast = {
        message: response.data.message,
        type: 'success'
      }
    } else {
      dispatch(addUserFailure(response.data.errors));
      const messages = typeof response.data.message === 'array' ? Object.values(response.data.message) : response.data.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(addUserFailure(error));
  }
  dispatch(clearToast());
}
export const updateUsers = (id, body) => async dispatch => {
  delete body.id;
  try {
    dispatch(updateUserStart());
    delete body.id;
    delete body.password;
    delete body.password_confirmation;
    delete body.birthday;
    delete body.gender;
    body._method = "PUT";
    const formData = new FormData();
    Object.keys(body).forEach(key => formData.append(key, body[key]));
    const url = API_ENDPOINT + '/users/' + id;
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: 'Bearer ' + localStorage.getItem("auth:access_token")
      }
    }
    // for (var pair of formData.entries()) {
    //   console.log(pair[0] + ', ' + pair[1]);
    // }
    const response = await post(url, formData, config)
    let valueToast = {}
    if (response.data.success) {

      dispatch(updateUserSuccess(response.data.data[0]));
      valueToast = {
        message: response.data.message,
        type: 'success'
      }
    } else {
      dispatch(updateUserFailure(response.data.errors));
      const messages = typeof response.data.message === 'array' ? Object.values(response.data.message) : response.data.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    console.log(error);
    dispatch(updateUserFailure(error));
  }
  dispatch(clearToast());
}
export const deleteUsers = (id) => async dispatch => {
  try {
    dispatch(deleteUserStart());
    const response = await userApi.delete(id);
    let valueToast = {}
    if (response.success === true) {
      dispatch(deleteUserSuccess(id));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deleteUserFailure(id));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deleteUserFailure(error));
  }
  dispatch(clearToast());
}
export const deleteMultiUsers = (ids) => async dispatch => {
  try {
    dispatch(deleteMultiUserStart());
    const response = await userApi.deleteMulti(ids);
    let valueToast = {}
    if (response.success) {
      dispatch(deleteMultiUserSuccess(ids));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deleteMultiUserFailure(ids));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deleteMultiUserFailure(error));
  }
  dispatch(clearToast());
}
export const changeStatusUsers = (id) => async dispatch => {
  try {
    dispatch(changeStatusUserStart());
    const response = await userApi.changeStatusUser(id);
    let valueToast = {}
    if (response.success) {
      dispatch(changeStatusUserSuccess(response.data[0]));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changeStatusUserFailure(response.data));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeStatusUserFailure('error'));
  }
  dispatch(clearToast());
}
export const resetPasswordUsers = (params) => async dispatch => {
  try {
    dispatch(resetPasswordUserStart());
    const response = await userApi.resetPasswordUser(params);
    let valueToast = {}
    if (response.success) {
      dispatch(resetPasswordUserSuccess());
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(resetPasswordUserFailure());
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(resetPasswordUserFailure('error'));
  }
  dispatch(clearToast());
}
export const searchUsers = (params) => async dispatch => {
  try {
    dispatch(searchUsersStart());
    const response = await userApi.filter(params);
    if (response.success) {
      dispatch(fetchUsersSuccess(response.data.users))
      dispatch(searchUsersSuccess(params.keyword))
    } else {
      dispatch(searchUsersFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(searchUsersFailure("error"));
  }
  dispatch(clearToast());
}
export const sortUsers = (params) => async dispatch => {
  try {
    dispatch(sortUsersStart());
    const response = await userApi.filter(params);
    if (response.success) {
      dispatch(fetchUsersSuccess(response.data.users))
    } else {
      dispatch(sortUsersFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(sortUsersFailure("error"));
  }
  dispatch(clearToast());
}
export default reducer;
