import React, { useState, useEffect } from 'react'
import InputField from '../../../../../custom_fields/inputField';
import CheckBoxField from '../../../../../custom_fields/checkBoxField';
import { Formik, FastField, Form } from 'formik'
import MultiSelect from '../../../../../custom_fields/multiSelectField';
import * as Yup from 'yup'
import { CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CFormGroup, CSpinner, CLabel } from '@coreui/react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useHistory } from "react-router-dom";
import { useTranslate } from 'react-redux-multilingual'

UserForm.propTypes = {
  onSubmit: PropTypes.func,
};

UserForm.defaultProps = {
  onSubmit: null,
}

const API_ENDPOINT = process.env.REACT_APP_API_URL_IMAGE;
function UserForm(props) {
  const t = useTranslate()
  let history = useHistory();
  const [preview, setPreview] = useState()
  const [avatar, setAvatar] = useState()
  const { initialValues } = props;
  const formValues = localStorage.getItem("formValues");
  const valueForm = initialValues || formValues;
  const { id } = useParams();
  const validationSchema = Yup.object().shape({
    fullname: Yup.string()
      .required(t('fullname_required')),
    email: Yup.string()
      .email(t('email_invalid'))
      .required(t('email_required')),
    role: Yup.string()
      .required(t('role_required')),
    status: Yup.string()
      .required(t('status_required')),
  })

  useEffect(() => {
    if (!avatar) {
      setPreview(undefined)
      return
    }

    const objectUrl = URL.createObjectURL(avatar)
    setPreview(objectUrl)

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl)
  }, [avatar])
  const onChangeAvatar = (value) => {
    setAvatar(value);
  }
  // const genderOption = [
  //   { value: 1, key: 'Male' },
  //   { value: 0, key: 'Female' }
  // ];

  const roles = useSelector(state => state.roles.roles.data);
  let rolesOption = [];
  const roleOfUser = [];
  if (roles) roles.map((obj) => {
    rolesOption.push({ "label": obj.name, "value": obj.id })
    return null;
  })
  rolesOption.length !== 0 && localStorage.setItem("rolesOption", JSON.stringify(rolesOption));
  rolesOption = rolesOption.length !== 0 ? rolesOption : JSON.parse(localStorage.getItem("rolesOption"));

  valueForm.roles && valueForm.roles.map((obj) => {
    roleOfUser.push({ "label": obj.name, "value": obj.id })
    return null;
  })

  // console.log(preview);
  const checkAvatar = avatar || id !== "add";
  const status = useSelector(state => state.toasts?.toastType)
  status === 'success' && history.push('/management/users');
  return (<>
    <Formik
      enableReinitialize
      initialValues={
        {
          id: valueForm.id || '',
          fullname: valueForm.fullname || '',
          email: valueForm.email || '',
          password: '',
          password_confirmation: '',
          birthday: valueForm.birthday || '',
          gender: valueForm.gender,
          phone: valueForm.phone || '',
          address: valueForm.address || '',
          role: roleOfUser || valueForm.role,
          status: valueForm.status,
        }
      }
      validationSchema={validationSchema}
      // validationSchema={Yup.object().shape({
      //     fullname: Yup.string()
      //       .required('Fullname is required'),
      //     email: Yup.string()
      //       .email('Email is invalid')
      //       .required('Email is required'),
      //     role: Yup.string()
      //       .required('Role is required'),
      //     status: Yup.string()
      //       .required('Status is required'),
      //   })}

      onSubmit={
        props.onSubmit
      }
    >
      {formikProps => {
        const { isSubmitting } = formikProps;

        return (
          <>
            <CRow>
              <CCol xs="12">
                <CCard>
                  <CCardHeader>
                    <h4>{id === 'add' ? t('addUser') : t('editUser')}</h4>
                  </CCardHeader>
                  <CCardBody>
                    <Form className="form-horizontal">
                      <CFormGroup>
                        <FastField
                          name="fullname"
                          component={InputField}
                          label={t('fullname')}
                          placeholder={t('fullname')}
                          requiredlabel
                        />
                      </CFormGroup>
                      {/* {id === 'add' && <> */}
                      <CFormGroup row>
                        <CCol md="2">
                          <CLabel htmlFor="avatar">{t('avatar')}</CLabel>
                        </CCol>
                        <CCol xs="12" md="10">
                          {checkAvatar && <><img className="rounded-circle mb-2" src={avatar ? preview : valueForm.avatar} alt="User Avatar" width="130" height="130" /><br /></>}
                          {/* {preview || valueForm.length!==0 && <><img className="rounded-circle mb-2" src={preview || API_ENDPOINT + valueForm.avatar} alt="Business Avatar" width="130" /><br /></>} */}

                          <input
                            name={t('avatar')}
                            type="file"
                            onChange={(event) => {
                              formikProps.setFieldValue("avatar", event.currentTarget.files[0])
                              onChangeAvatar(event.currentTarget.files[0])
                            }}
                            className={`${formikProps.errors.avatar ? "is-invalid" : ""}`}
                          />
                          <div className="text-danger" style={{ fontSize: '80%' }} >{formikProps.errors.avatar}</div></CCol>
                      </CFormGroup>
                      {/* </>} */}
                      <CFormGroup>
                        <FastField
                          name="phone"
                          component={InputField}
                          label={t('phone')}
                          placeholder={t('phone')}
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="email"
                          component={InputField}
                          label={t('email')}
                          placeholder={t('email')}
                          requiredlabel
                        />
                      </CFormGroup>
                      {id === 'add' && <>
                        <CFormGroup>
                          <FastField
                            name="password"
                            component={InputField}
                            type="password"
                            label={t('password')}
                            placeholder={t('password')}
                            className={`form-control ${formikProps.errors.password ? "is-invalid" : ""}`}
                            checkMin={8}
                            requiredlabel
                          />
                        </CFormGroup>
                        <CFormGroup>
                          <FastField
                            name="password_confirmation"
                            component={InputField}
                            type="password"
                            label={t('confirmPassword')}
                            placeholder={t('confirmPassword')}
                            className={`form-control ${formikProps.errors.password_confirmation ? "is-invalid" : ""}`}
                            checkMatch="password"
                            requiredlabel
                          />
                        </CFormGroup>
                      </>}
                      <CFormGroup>
                        <FastField
                          name="address"
                          component={InputField}

                          label={t('address')}
                          placeholder={t('address')}
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="role"
                          component={MultiSelect}
                          options={rolesOption}
                          label={t('role')}
                          placeholder={t('select_role')}
                          requiredLabel
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="status"
                          component={CheckBoxField}
                          label={t('status')}
                          requiredlabel
                        />
                      </CFormGroup>
                      {/* <CFormGroup row>
                        <CCol md="2"><CLabel htmlFor="status">Status</CLabel><span className="text-danger">*</span></CCol>
                        <CCol xs="12" md="10"><FastField name="status" type="checkBox" /></CCol>
                      </CFormGroup> */}
                      <div className="form-actions float-right">
                        <CButton type="submit" color={id === 'add' ? 'success' : 'info'}>{isSubmitting && <CSpinner size="sm" />}
                          {id === 'add' ? t('add') : t('edit')}</CButton>
                        <Link to={{ pathname: `/management/users` }}>
                          <CButton color="secondary ml-1">{t('cancel')}</CButton>
                        </Link>
                      </div>
                    </Form>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </>
        )
      }}
    </Formik>
  </>
  )
}

export default UserForm
