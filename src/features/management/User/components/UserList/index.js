import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CIcon from '@coreui/icons-react';
import '../UserList/style.css'
import { fetchRoles } from '../../../Role/roleSlice';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchUsers, resetPasswordUsers, searchUsers, sortUsers } from '../../userSlice';
import { useForm } from 'react-hook-form'
import { useTranslate } from 'react-redux-multilingual'

import {
  CBadge, CCard, CCardBody, CCardHeader, CCol, CDataTable, CRow, CButton, CCollapse, CModal, CModalHeader,
  CModalBody, CModalFooter, CSwitch, CFormGroup, CInputCheckbox, CPagination, CForm, CTooltip
} from '@coreui/react'

UserList.propTypes = {
  userList: PropTypes.array,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
};

UserList.defaultProps = {
  userList: [],
  onEditClick: null,
  onRemoveClick: null,
};

function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [day, month, year].join('-');
}

function UserList(props) {
  const { userList, onEditClick, onRemoveClick, onRemoveMultiClick, onChangeStatusClick } = props;
  const t = useTranslate()
  const dispatch = useDispatch();
  const dataUser = useSelector(state => state.users.users)
  const valueSearch = useSelector(state => state.users.valueSearch)
  const loading = useSelector(state => state.users.loading)

  const [error, setError] = useState(false);
  const [modal, setModal] = useState(false);
  const [modalNewPassword, setModalNewPassword] = useState(false);
  const [modalDeleteMulti, setmodalDeleteMulti] = useState(false);
  const [searchList, setsearchList] = useState();
  const [sortList, setSortList] = useState();
  const [itemResetPassword, setItemResetPassword] = useState();
  const [itemSelected, setItemSelected] = useState();
  const [selects, setSelects] = useState([])
  const [filters, setFilters] = useState({
    pageSize: 10,
    page: 1,
    keyword: '',
    sortBy: '',
    field: ''
  })

  const { register, errors, handleSubmit, reset } = useForm({
    defaultValues: {
      value: valueSearch,
    }
  });

  useEffect(() => {
    dispatch(fetchRoles());
  }, [dispatch])

  const toggleNewPassword = () => {
    setModalNewPassword(!modalNewPassword);
  }

  const toggleSelects = (item) => {
    const position = selects.indexOf(item)

    let newSelects = selects.slice()
    if (position !== -1) {
      newSelects.splice(position, 1)
    } else {
      newSelects = [...selects, item]
    }
    setSelects(newSelects)
  }

  const toggle = (item) => {
    setItemSelected(item)
    setModal(!modal);
  }

  const toggleDeleteMulti = (item) => {
    setItemSelected(item)
    setmodalDeleteMulti(!modalDeleteMulti);
  }

  const onSubmitSearch = data => {
    filters.keyword = data.value;
    delete filters.page;
    setsearchList(data);
    console.log(filters);
    setTimeout(() => {
      dispatch(searchUsers(filters));
    }, 2000);
  }

  const onSubmitNewPassword = data => {
    const params = { "id": itemResetPassword.id, "password": data.newPassword };
    dispatch(resetPasswordUsers(params));
    setModalNewPassword(!modalNewPassword);
  }

  function handleSortClick(data) {
    filters.sortBy = data.sort;
    setSortList(data)
    dispatch(sortUsers(filters));
  }

  const handleEditClick = (item) => {
    if (onEditClick) onEditClick(item);
    localStorage.setItem("formValues", JSON.stringify(item));
  }

  const handleChangeStatusClick = (item) => {
    if (onChangeStatusClick) onChangeStatusClick(item);
  }

  const handleResetPasswordClick = (item) => {
    setItemResetPassword(item)
    setModalNewPassword(!modalNewPassword);
  }

  const handleRemoveClick = () => {
    if (onRemoveClick) onRemoveClick(itemSelected);
    setModal(!modal);
  }

  const handleRemoveMultiClick = () => {
    if (onRemoveMultiClick) onRemoveMultiClick(selects.toString());
    setmodalDeleteMulti(!modalDeleteMulti)
    setSelects([]);
  }

  function handlePageChange(newPage) {
    setFilters({
      ...filters,
      page: newPage,
    });
    if (searchList) {
      searchList.page = newPage;
      searchList.keyword = filters.keyword;
      dispatch(searchUsers(searchList))
    } else if (sortList) {
      sortList.page = newPage;
      dispatch(sortUsers(sortList));
    } else {
      dispatch(fetchUsers(newPage));
    }
  }

  //custom sort
  useEffect(() => {
    var sort = document.getElementsByClassName('sort')[0]
    var div = document.createElement("div");
    div.innerHTML = '<svg width="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" id="asc" class="position-absolute cursor-pointer CDataTable_icon-transition__1TJkV CDataTable_arrow-position__1hvHj CDataTable_transparent__qAZ-y" role="img"><polygon fill="var(--ci-primary-color, currentColor)" points="390.624 150.625 256 16 121.376 150.625 144.004 173.252 240.001 77.254 240.001 495.236 272.001 495.236 272.001 77.257 367.996 173.252 390.624 150.625" class="ci-primary"></polygon></svg>';
    let idSVG = div.childNodes[0].id;
    div.addEventListener("click", function () {
      let value = { "sort": idSVG }
      handleSortClick(value)
      if (idSVG === 'asc') {
        idSVG = 'desc';
        div.childNodes[0].classList.add('CDataTable_rotate-icon__3Fis6');
      } else {
        idSVG = 'asc';
        div.childNodes[0].classList.remove('CDataTable_rotate-icon__3Fis6');
      }
    })
    sort && sort.appendChild(div)
  }, [])

  const fields = [
    {
      key: 'checkBox',
      label: '',
      _style: { width: '3%' },
      sorter: false,
      filter: false
    },
    {
      key: 'No',
      label: t('no'),
      _style: { width: '1%', textAlign: 'center' },
      sorter: false,
      filter: false
    },
    {
      key: 'fullname',
      label: t('name'),
      _classes: 'position-relative sort'
    },
    {
      key: 'role',
      label: t('role'),
    },
    {
      key: 'phone',
      label: t('phone'),
      _style: { width: '10%' },
      sorter: false,
      filter: false
    },
    {
      key: 'email',
      label: t('email'),
      sorter: false,
      filter: false
    },
    {
      key: 'functions',
      label: t('actions'),
      _style: { width: '1%', textAlign: 'center' },
      sorter: false,
      filter: false
    },
  ]

  return (
    <>
      <CModal show={modal} onClose={toggle}>
        <CModalHeader closeButton>{t('deleteUser')}</CModalHeader>
        <CModalBody>{t('confirmDeleteUser')} "{itemSelected?.fullname}"?</CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveClick()} color="danger">{t('agree')}</CButton>{' '}
          <CButton color="secondary" onClick={toggle}>{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CModal show={modalDeleteMulti} onClose={toggleDeleteMulti}>
        <CModalHeader closeButton>{selects?.length > 1 ? t('deleteUsers') : t('deleteUser')}</CModalHeader>
        <CModalBody>{selects?.length > 1 ? t('confirmDeleteUsers') : t('confirmDeleteUser')} ?</CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveMultiClick()} color="danger">{t('agree')}</CButton>{' '}
          <CButton color="secondary" onClick={toggleDeleteMulti}>{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CModal show={modalNewPassword} onClose={toggleNewPassword}>
        <CModalHeader closeButton>{t('enterNewPassword')}</CModalHeader>
        <CModalBody>
          <div className="justify-content-center">
            <form onSubmit={handleSubmit(onSubmitNewPassword)}>
              <input
                type="password"
                className={!!errors.newPassword || error ? 'form-control is-invalid' : 'form-control'}
                name="newPassword"
                placeholder={t('newPassword')}
                aria-describedby="validationNewPassword"
                autoComplete="off"
                ref={register()} />
              <div className="float-right mt-lg-1">
                <CButton className="" type="submit" color="info">{t('submit')}</CButton>{' '}
                <CButton color="secondary" onClick={toggleNewPassword}>{t('cancel')}</CButton>
              </div>
            </form>
          </div>
        </CModalBody>
      </CModal>
      <CRow>
        <CCol xl={12}>
          <CCard>
            <CCardHeader>
              <h4 className="d-inline-block">{t('listUser')}</h4>
            </CCardHeader>
            <CCardBody>
              <form onSubmit={handleSubmit(onSubmitSearch)} >
                <div className="row my-2 mx-0">
                  <div className="col-sm-6 p-0">
                    <Link to={{ pathname: `/management/users/add` }}>
                      <CTooltip content={t('addUser')} placement="right">
                        <CButton color="success">
                          {t('add')} <i className="fas fa-plus"></i>
                        </CButton>
                      </CTooltip>
                    </Link>
                    <CTooltip content={selects?.length > 1 ? t('deleteUsers') : t('deleteUser')} placement="left">
                      <CButton
                        color="danger"
                        className="ml-2"
                        style={selects.length > 0 ? {} : { display: 'none' }}
                        onClick={() => toggleDeleteMulti()} >
                        {t('delete')} <i className="fas fa-trash"></i>
                      </CButton>
                    </CTooltip>
                  </div>
                  <div className="col-sm-6 form-inline p-0 justify-content-sm-end">
                      <input
                        className="form-control"
                        autoComplete="off"
                        name="value"
                        type="text"
                        placeholder={t('typeString')}
                        onChange={handleSubmit(onSubmitSearch)}
                        ref={register()} />
                  </div>
                </div>
              </form>
              <CDataTable
                items={userList}
                fields={fields}
                hover
                striped
                loading={loading}
                scopedSlots={{
                  'No': (item, index) => (
                    <td className="text-center">
                      {((dataUser.current_page - 1) * 10 + index + 1)}
                    </td>
                  ),
                  'checkBox': (item) => (
                    <td>
                      <CFormGroup>
                        <CInputCheckbox
                          className="ml-2 cursor-pointer"
                          onClick={() => toggleSelects(item.id)}
                          checked={selects.includes(item.id)}
                          onChange={() => { }}
                        />
                      </CFormGroup>
                    </td>
                  ),
                  'birthday': (item) => (
                    <td>
                      {formatDate(new Date(item.birthday).toString())}
                    </td>
                  ),
                  'role': (item) => (
                    <td>
                      {item.roles.map((obj, i) => (
                        <CBadge className="mr-2" color="info" key={i}>
                          {obj.name}
                        </CBadge>
                      ))}
                    </td>
                  ),
                  'functions': (item, index) => {
                    return (
                      <>
                        <td className="py-2">
                          <CCollapse className="d-inline-flex">
                            <CSwitch onClick={() => handleChangeStatusClick(item)} variant="outline" shape="pill"
                              className={'mx-1 mt-1 c-switch-opposite-warning'} shape={'pill'} size={'sm'} color={'warning'}
                              checked={!item.email_verified_at} onChange={() => { }} />
                            <CTooltip content={t('resetPassword')} placement="top">
                              <CButton onClick={() => handleResetPasswordClick(item)} variant="outline" size="sm" color="dark" className="ml-1">
                                <i className="fas fa-key"></i>
                              </CButton>
                            </CTooltip>
                            <CTooltip content={t('editUser')} placement="top">
                              <CButton onClick={() => handleEditClick(item)} variant="outline" size="sm" color="info" className="ml-1">
                                <CIcon size={'sm'} name="cilPencil" />
                              </CButton>
                            </CTooltip>
                            <CTooltip content={t('deleteUser')} placement="top">
                              <CButton onClick={() => toggle(item)} variant="outline" size="sm" color="danger" className="ml-1">
                                <CIcon size={'sm'} name="cilTrash" />
                              </CButton>
                            </CTooltip>
                          </CCollapse>
                        </td>
                      </>
                    )
                  },
                }}
              />
              <CPagination activePage={dataUser.current_page} onActivePageChange={(val) => handlePageChange(val)} pages={dataUser.last_page} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default UserList;
