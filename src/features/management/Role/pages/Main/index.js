import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteRole, fetchRoles, changeStatusRoles } from '../../roleSlice';
import RoleList from '../../components/RoleList';
import { useHistory } from 'react-router-dom';

MainPage.propTypes = {};

function MainPage(props) {
  const dispatch = useDispatch();
  const roles = useSelector(state => state.roles.roles.data);
  const dataRoles = useSelector(state => state.roles.roles);
  // const roles = useSelector(state => state.roles);
  const history = useHistory();
  useEffect(() => {
    (props.location.state === undefined || props.location.state === null) && dataRoles.length===0 && dispatch(fetchRoles());
  }, [dispatch])

  const onEditClick = (role) => {
    history.push({
      state: {role},
      pathname: `/management/roles/${role.id}`
    });
  }

  const onRemoveClick = (role) => {
    const removeRoleId = role.id;
    const action = deleteRole(removeRoleId);
    dispatch(action);
  }

  const onChangeStatusClick = (role) => {
    const action = changeStatusRoles(role.id);
    dispatch(action);
  }

  return (
    <>
      <RoleList
        roleList={roles}
        onEditClick={onEditClick}
        onRemoveClick={onRemoveClick}
        onChangeStatusClick={onChangeStatusClick}
      />
    </>
  );
}

export default MainPage;
