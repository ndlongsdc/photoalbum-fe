import RoleForm from '../../components/RoleForm';
import { addRole, updateRole } from '../../roleSlice';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

AddEditPage.propTypes = {};

function AddEditPage(props) {
  const roles = useSelector(state => state.roles.roles);
  const dispatch = useDispatch();
  const { id } = useParams();

  const handleSubmit = async (values) => {
    await id !== 'add' ? dispatch(updateRole(id, values)) : dispatch(addRole(values));
  }

  const initialValues = roles;
  const formValues = JSON.parse(localStorage.getItem("formValues"));
  const saveValue = formValues !== undefined && formValues !== null ? formValues : {
      permissions: [],
      name: '',
      active: '',
      description: '',
    }
  return (
    <RoleForm
      initialValues={id === 'add' ? initialValues : saveValue}
      onSubmit={handleSubmit}
    />
  );
}

export default AddEditPage;
