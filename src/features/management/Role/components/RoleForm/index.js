import React from 'react'
import InputField from '../../../../../custom_fields/inputField';
import TextareaField from '../../../../../custom_fields/textareaField';
import CheckBoxField from '../../../../../custom_fields/checkBoxField';
import MultiSelect from '../../../../../custom_fields/multiSelectField';
import { Formik, FastField, Form } from 'formik'
import { useSelector } from 'react-redux';
import * as Yup from 'yup'
import { CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CFormGroup, CSpinner } from '@coreui/react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import { useTranslate } from 'react-redux-multilingual'

RoleForm.propTypes = {
  onSubmit: PropTypes.func,
};

RoleForm.defaultProps = {
  onSubmit: null,
}

function RoleForm(props) {
  const t = useTranslate()
  let history = useHistory();
  const { initialValues } = props;

  const formValues = localStorage.getItem("formValues");
  const valueForm = initialValues || formValues;
  const { id } = useParams();

  const validationSchema = Yup.object().shape({
    permission: Yup.string()
      .required(t('permissionName_required')),
    name: Yup.string()
      .required(t('roleName_required')),
  })

  const permissions = useSelector(state => state.permissions.permissions);
  // const resRole = useSelector(state => state.roles.roles);
  let permissionOption = [];
  const permissionOfRole = [];
  permissions.map((obj) => {
    permissionOption.push({ "label": obj.name, "value": obj.id })
    return null;
  })
  permissionOption.length !== 0 && localStorage.setItem("permissionOption", JSON.stringify(permissionOption));
  permissionOption = permissionOption.length !== 0 ? permissionOption : JSON.parse(localStorage.getItem("permissionOption"));

  valueForm.permissions&&valueForm.permissions.map((objPermission) => {
    permissionOfRole.push(objPermission.id)
    return null;
  })
  // const error = useSelector(state => state.roles.error)
  const status = useSelector(state => state.toasts?.toastType)
  status === 'success' && history.push('/management/roles');
  return (
    <Formik
      enableReinitialize
      initialValues={
        {
          id: valueForm.id,
          name: valueForm.name,
          description: valueForm.description || "",
          status: valueForm.active,
          permission: permissionOfRole || valueForm.permissions

        }
      }
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
    >
      {formikProps => {
        const { isSubmitting } = formikProps;

        return (
          <>
            <CRow>
              <CCol xs="12">
                <CCard>
                  <CCardHeader>
                    <h4>{id === 'add' ? t('addRole') : t('editRole')}</h4>
                  </CCardHeader>
                  <CCardBody>
                    <Form className="form-horizontal">
                      <CFormGroup>
                        <FastField
                          name="name"
                          component={InputField}
                          label={t('roleName')}
                          placeholder={t('roleName')}
                          requiredlabel
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="permission"
                          options={permissionOption}
                          component={MultiSelect}
                          label={t('permissionName')}
                          placeholder={t('select_permission')}
                          requiredLabel
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="description"
                          component={TextareaField}
                          label={t('description')}
                          placeholder={t('description')}
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="status"
                          component={CheckBoxField}
                          label={t('status')}
                          requiredlabel
                        />
                      </CFormGroup>
                      <div className="form-actions float-right">
                        <CButton type="submit" color={id === 'add' ? 'primary' : 'success'}>{isSubmitting && <CSpinner size="sm" />}
                          {id === 'add' ? t('add') : t('edit')}</CButton>
                        <Link to={{ pathname: `/management/roles` }}>
                          <CButton color="secondary ml-1">{t('cancel')}</CButton>
                        </Link>
                      </div>
                    </Form>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </>
        )
      }}
    </Formik>

  )
}

export default RoleForm
