import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CIcon from '@coreui/icons-react';
import '../RoleList/style.css'
import { fetchPermissions } from '../../../Permission/permissionSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslate } from 'react-redux-multilingual'

import {
  CBadge, CCard, CCardBody, CCardHeader, CCol, CDataTable, CRow, CButton, CCollapse, CModal,
  CModalHeader, CModalBody, CModalFooter, CTooltip, CSwitch
} from '@coreui/react'
import { Link } from 'react-router-dom';

RoleList.propTypes = {
  roleList: PropTypes.array,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
};

RoleList.defaultProps = {
  roleList: [],
  onEditClick: null,
  onRemoveClick: null,
};

function RoleList(props) {
  const { roleList, onEditClick, onRemoveClick, onChangeStatusClick } = props;
  const t = useTranslate()
  const dispatch = useDispatch();
  const loading = useSelector(state => state.roles.loading)

  const [itemSelected, setItemSelected] = useState();
  const [modal, setModal] = useState(false);

  useEffect(() => {
    dispatch(fetchPermissions());
  }, [dispatch])

  const toggle = (item) => {
    setItemSelected(item)
    setModal(!modal);
  }

  const handleEditClick = (item) => {
    if (onEditClick) onEditClick(item);
    localStorage.setItem("formValues", JSON.stringify(item));
  }

  const handleChangeStatusClick = (item) => {
    if (onChangeStatusClick) onChangeStatusClick(item);
  }

  const handleRemoveClick = () => {
    if (onRemoveClick) onRemoveClick(itemSelected);
    setModal(!modal);
  }

  const fields = [
    {
      key: 'No',
      label: t('no'),
      _classes: 'text-center',
      _style: { width: '5%' },
      sorter: false,
    },
    {
      key: 'name',
      label: t('roleName'),
      _style: { width: '20%' },
    },
    {
      key: 'permissions',
      label: t('permissionName'),
      sorter: false,
    },
    {
      key: 'functions',
      label: t('actions'),
      _style: { width: '9%', textAlign: 'center'  },
      sorter: false,
      filter: false
    }
  ]
  return (
    <>
      <CModal
        show={modal}
        onClose={toggle}
      >
        <CModalHeader closeButton>{t('deleteRole')}</CModalHeader>
        <CModalBody>
          {t('confirmDeleteRole')} "{itemSelected?.name}"?
        </CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveClick()} color="danger">{t('agree')}</CButton>{' '}
          <CButton
            color="secondary"
            onClick={toggle}
          >{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CRow>
        <CCol xl={12}>
          <CCard>
            <CCardHeader>
              <h4 className="d-inline-block">{t('listRole')}</h4>
            </CCardHeader>
            <CCardBody>
              <Link to={{ pathname: `/management/roles/add` }}>
                <CTooltip content={t('addRole')} placement="right">
                  <CButton color="success" className="mb-2">
                  {t('add')} <i className="fas fa-plus"></i>
                  </CButton>
                </CTooltip>
              </Link>
              <CDataTable
                items={roleList}
                fields={fields}
                hover
                pagination
                // tableFilter
                // sorter
                loading={loading}
                activePage={parseInt(localStorage.getItem("currentPage"))}
                onPageChange={(val) => localStorage.setItem("currentPage", val)}
                scopedSlots={{
                  'No': (item, index) => (
                    <td className="text-center">
                      {index + 1}
                    </td>
                  ),
                  // 'checkBox': (item) => {
                  //   return (
                  //     <>
                  //       <td>
                  //         <CFormGroup>
                  //           <CInputCheckbox className="ml-2 cursor-pointer" onClick={() => toggleSelects(item.id)} />
                  //         </CFormGroup>
                  //       </td>
                  //     </>
                  //   )
                  // },
                  'permissions':
                    (item) => (
                      <td>
                        {item.permissions?.map((obj, i) => {
                          return (
                            <span key={i}>
                              <CBadge color="info" className="mr-2">
                                {obj.name}
                              </CBadge>
                            </span>
                          )
                        }
                        )}
                      </td>
                    ),
                  'functions':
                    (item, index) => {
                      return (
                        <>
                          <td className="py-2">
                            <CCollapse className="d-inline-flex">
                            <CSwitch onClick={() => handleChangeStatusClick(item)} variant="outline" shape="pill"
                              className={'mx-1 mt-1 c-switch-opposite-warning'} shape={'pill'} size={'sm'} color={'warning'}
                              checked={!item.email_verified_at} onChange={() => { }} />
                              <CTooltip content={t('editRole')} placement="top">
                                <CButton onClick={() => handleEditClick(item)} variant="outline" size="sm" color="info" className="ml-1">
                                  <CIcon size={'sm'} name="cilPencil" />
                                </CButton>
                              </CTooltip>
                              <CTooltip content={t('deleteRole')} placement="top">
                                <CButton onClick={() => toggle(item)} variant="outline" size="sm" color="danger" className="ml-1">
                                  <CIcon size={'sm'} name="cilTrash" />
                                </CButton>
                              </CTooltip>
                            </CCollapse>
                          </td>
                        </>

                      )
                    },
                }}
              />
              {/* <CPagination
                activePage={1}
                onActivePageChange={handlePageChange}
                pages={1}
                doubleArrows={false}
                align="center"
              /> */}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default RoleList;
