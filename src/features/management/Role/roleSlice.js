import { createSlice } from '@reduxjs/toolkit';
import roleApi from '../../../api/roleApi'
import { setToast, clearToast } from '../../../custom_toast/toastSlice'

const role = createSlice({
  name: 'roles',
  initialState: {
    roles: [],
    error: '',
    loading: false,
  },
  reducers: {
    fetchRolesStart: (state, action) => {
      state.loading = true
    },
    fetchRolesSuccess: (state, action) => {
      state.loading = false
      state.roles = action.payload
    },
    fetchRolesFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    fetchRolesByIdsStart: (state, action) => {
      state.loading = true
    },
    fetchRolesByIdsSuccess: (state, action) => {
      state.loading = false
      // state.roles = action.payload
    },
    fetchRolesByIdsFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    addRoleStart: (state, action) => {
      state.loading = true
    },
    addRoleSuccess: (state, action) => {
      state.error = false
      state.loading = false;
      state.roles.unshift(action.payload)
    },
    addRoleFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    updateRoleStart: (state, action) => {
      state.loading = true
    },
    updateRoleSuccess: (state, action) => {
      state.error = false;
      state.loading = false;
      const foundIndex = state.roles.data.findIndex(({ id }) => id === action.payload.id)
      state.roles.data.splice(foundIndex, 1, action.payload)
    },
    updateRoleFailure: (state, action) => {
      state.error = action.payload
    },
    deleteRoleStart: (state, action) => {
      state.loading = true
    },
    deleteRoleSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.roles.data.findIndex(({ id }) => id === action.payload)
      state.roles.data.splice(foundIndex, 1)
    },
    deleteRoleFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
    changeStatusRoleStart: (state, action) => {
      state.loading = true
    },
    changeStatusRoleSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.roles?.data.findIndex(({ id }) => id === action.payload.id)
      state.roles.data.splice(foundIndex, 1, action.payload)
    },
    changeStatusRoleFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
  }
});

const { reducer, actions } = role;
export const {
  fetchRolesStart, fetchRolesSuccess, fetchRolesFailure,
  fetchRolesByIdsStart, fetchRolesByIdsSuccess, fetchRolesByIdsFailure,
  addRoleStart, addRoleSuccess, addRoleFailure,
  updateRoleStart, updateRoleSuccess, updateRoleFailure,
  deleteRoleStart, deleteRoleSuccess, deleteRoleFailure,
  changeStatusRoleStart, changeStatusRoleSuccess, changeStatusRoleFailure,
} = actions;
export const fetchRoles = () => async dispatch => {
  try {
    dispatch(fetchRolesStart());
    const response = await roleApi.getAll();
    if (response.success) {
      dispatch(fetchRolesSuccess(response.data))
    } else {
      dispatch(fetchRolesFailure(response));
    }
  } catch (error) {
    const valueToast = {
      message: 'Access token has expired',
      type: 'error',
      status: 401
    }
    dispatch(setToast(valueToast));
    dispatch(fetchRolesFailure('error'));
  }
  dispatch(clearToast());
}
export const fecthCurrentPermissions = (ids) => async dispatch => {
  try {
    let curentPermissions = [];
    dispatch(fetchRolesByIdsStart());
    const response = await roleApi.get(ids);
    if (response.success) {
      const userRoles = response.data;
      userRoles.map((userRole) => {
        userRole.permissions.map((permissionRole) => {
          curentPermissions = curentPermissions.concat(permissionRole.path ? permissionRole.path.split(" ") : "*");
        })
      })
      localStorage.setItem("curentPermissions", curentPermissions)
      dispatch(fetchRolesByIdsSuccess(response.data))
    } else {
      dispatch(fetchRolesByIdsFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        // status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(fetchRolesByIdsFailure('error'));
  }
  dispatch(clearToast());
}
export const addRole = (body) => async dispatch => {
  try {
    dispatch(addRoleStart());
    const response = await roleApi.create(body);
    let valueToast = {}
    if (response.success) {
      dispatch(addRoleSuccess(response.data))
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(addRoleFailure(response.error));
      const messages = response.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(addRoleFailure(error));
  }
  dispatch(clearToast());
}
export const updateRole = (id, body) => async dispatch => {
  try {
    dispatch(updateRoleStart());
    const response = await roleApi.update(id, body);
    let valueToast = {}
    if (response.success === true) {
      dispatch(updateRoleSuccess(response.data));
      localStorage.removeItem("formValues");
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(updateRoleFailure(response.data));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(updateRoleFailure(error));
  }
  dispatch(clearToast());
}
export const deleteRole = (id) => async dispatch => {
  try {
    dispatch(deleteRoleStart());
    const response = await roleApi.delete(id);
    let valueToast = {}
    if (response.success) {
      dispatch(deleteRoleSuccess(id));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deleteRoleFailure(id));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deleteRoleFailure(error));
  }
  dispatch(clearToast());
}
export const changeStatusRoles = (id) => async dispatch => {
  try {
    dispatch(changeStatusRoleStart());
    const response = await roleApi.changeStatusRole(id);
    let valueToast = {}
    if (response.success) {
      dispatch(changeStatusRoleSuccess(response.data[0]));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changeStatusRoleFailure(response.data));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeStatusRoleFailure('error'));
  }
  dispatch(clearToast());
}
export default reducer;
