import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deletePermissions, fetchPermissions, changeStatusPermissions } from '../../permissionSlice';
import PermissionList from '../../components/PermissionList';
import { useHistory } from 'react-router-dom';

MainPage.propTypes = {};

function MainPage(props) {
  const dispatch = useDispatch();
  const permissions = useSelector(state => state.permissions.permissions);
  const history = useHistory();

  useEffect(() => {
    (props.location.state === undefined || props.location.state === null) && permissions.length===0 && dispatch(fetchPermissions());
  }, [dispatch])

  const onEditClick = (permission) => {

    history.push({
      state: {permission},
      pathname: `/management/permissions/${permission.id}`
    });
  }

  const onRemoveClick = (permission) => {
    const removePermissionId = permission.id;
    const action = deletePermissions(removePermissionId);
    dispatch(action);
  }

  const onChangeStatusClick = (permission) => {
    const action = changeStatusPermissions(permission.id);
    dispatch(action);
  }

  return (
    <>
      <PermissionList
        permissionList={permissions}
        onEditClick={onEditClick}
        onRemoveClick={onRemoveClick}
        onChangeStatusClick={onChangeStatusClick}
      />
    </>
  );
}

export default MainPage;
