import PermissionForm from '../../components/PermissionForm';
import { addPermissions, updatePermissions } from '../../permissionSlice';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

AddEditPage.propTypes = {};

function AddEditPage(props) {
  const dispatch = useDispatch();
  const { id } = useParams();
  const error = useSelector(state => state.permissions.error)
  const handleSubmit = async (values) => {
    await id !== 'add' ? dispatch(updatePermissions(values.id,values)) : dispatch(addPermissions(values));

    !error && props.history.push('/management/permissions');
  }


  return (
    <PermissionForm
      initialValues={props.location.state !== undefined ? props.location.state.permission : {
        name: '',
        key: '',
        method: '',
        path: '',
      }}
      onSubmit={handleSubmit}
    />
  );
}

export default AddEditPage;
