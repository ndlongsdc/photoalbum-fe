import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import MainPage from './pages/Main';

Permission.propTypes = {};

function Permission(props) {
  const match = useRouteMatch();
  return (
    <>
      <Switch>
        <Route exact path={match.url} component={MainPage} />
      </Switch>
    </>
  );
}

export default Permission;
