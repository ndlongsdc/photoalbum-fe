import React from 'react'
import InputField from '../../../../../custom_fields/inputField';
import TextareaField from '../../../../../custom_fields/textareaField';
import { Formik, FastField, Form } from 'formik'
import * as Yup from 'yup'
import { CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CFormGroup, CSpinner } from '@coreui/react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { useTranslate } from 'react-redux-multilingual'

PermissionForm.propTypes = {
  onSubmit: PropTypes.func,
};

PermissionForm.defaultProps = {
  onSubmit: null,
}

function PermissionForm(props) {
  const t = useTranslate()
  const { initialValues } = props;
  const formValues = localStorage.getItem("formValues");
  const valueForm = initialValues || formValues;
  const { id } = useParams();

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .required(t('roleName_required')),
    key: Yup.string()
      .required(t('key_required')),
  })
  return (
    <Formik
      enableReinitialize
      initialValues={
        {
          id:valueForm.id,
          name:valueForm.name || '',
          key:valueForm.key || '',
          method:valueForm.method || '',
          path:valueForm.path || ''
        }
      }
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
    >
      {formikProps => {
        const { isSubmitting } = formikProps;

        return (
          <>
            <CRow>
              <CCol xs="12">
                <CCard>
                  <CCardHeader>
                  <h4>{id ==='add' ? t('addPermission') : t('editPermission')}</h4>
              </CCardHeader>
                  <CCardBody>
                    <Form className="form-horizontal">
                      <CFormGroup>
                        <FastField
                          name="name"
                          component={InputField}
                          label={t('permissionName')}
                          placeholder={t('permissionName')}
                          requiredlabel
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="key"
                          component={InputField}

                          label={t('key')}
                          placeholder={t('key')}
                          requiredlabel
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="method"
                          component={InputField}

                          label={t('method')}
                          placeholder={t('method')}
                          autoCapitalize
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="path"
                          component={TextareaField}

                          label={t('path')}
                          placeholder={t('path')}
                        />
                      </CFormGroup>
                      <div className="form-actions float-right">
                        <CButton type="submit" color={id ==='add' ? 'primary' : 'success'}>{isSubmitting && <CSpinner size="sm" />}
                          {id ==='add' ? t('add') : t('edit')}</CButton>
                        <Link to={{ pathname: `/management/permissions` }}>
                          <CButton color="secondary ml-1">{t('cancel')}</CButton>
                        </Link>
                      </div>
                    </Form>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </>
        )
      }}
    </Formik>

  )
}

export default PermissionForm
