import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CIcon from '@coreui/icons-react';
import '../PermissionList/style.css'
import { useSelector } from 'react-redux';
import { useTranslate } from 'react-redux-multilingual'

import {
  CBadge, CCard, CCardBody, CCardHeader, CCol, CDataTable, CRow, CButton, CCollapse, CModal,
  CModalHeader, CModalBody, CModalFooter, CTooltip, CSwitch
} from '@coreui/react'
import { Link } from 'react-router-dom';

PermissionList.propTypes = {
  permissionList: PropTypes.array,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
};

PermissionList.defaultProps = {
  permissionList: [],
  onEditClick: null,
  onRemoveClick: null,
};

function PermissionList(props) {
  const { permissionList, onEditClick, onRemoveClick, onChangeStatusClick } = props;
  const t = useTranslate()
  const loading = useSelector(state => state.permissions.loading)

  const [modal, setModal] = useState(false);
  const [itemSelected, setItemSelected] = useState();

  const handleRemoveClick = () => {
    if (onRemoveClick) onRemoveClick(itemSelected);
    setModal(!modal);
  }

  const handleEditClick = (item) => {
    if (onEditClick) onEditClick(item);
    localStorage.setItem("formValues", JSON.stringify(item));
  }

  const handleChangeStatusClick = (item) => {
    if (onChangeStatusClick) onChangeStatusClick(item);
  }

  const toggle = (item) => {
    setItemSelected(item)
    setModal(!modal);
  }

  // const toggleSelects = (item) => {
  //   const position = selects.indexOf(item)

  //   let newSelects = selects.slice()
  //   if (position !== -1) {
  //     newSelects.splice(position, 1)
  //   } else {
  //     newSelects = [...selects, item]
  //   }
  //   setSelects(newSelects)
  // }

  const methodColor = (value) => {
    switch (value) {
      case "ANY": return 'secondary'
      case "GET": return 'info'
      case "POST": return 'success'
      case "PUT": return 'warning'
      case "DELETE": return 'danger'
      default: return 'secondary'
    }
  }

  const fields = [
    {
      key: 'No',
      label: t('no'),
      _classes: 'text-center',
      _style: { width: '5%' },
      sorter: false,
    },
    {
      key: 'name',
      label: t('name'),
    },
    {
      key: 'key',
      label: t('key'),
      sorter: false,
    },
    {
      key: 'method',
      label: t('path'),
      // _style: { width: '1%' },
      sorter: false,
      filter: false
    },
    {
      key: 'functions',
      label: t('actions'),
      _style: { width: '9%', textAlign: 'center'  },
      sorter: false,
      filter: false
    }
  ]

  return (
    <>
      <CModal show={modal} onClose={toggle}>
        <CModalHeader closeButton>{t('deletePermission')}</CModalHeader>
        <CModalBody>{t('confirmDeletePermission')} "{itemSelected?.name}"?</CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveClick()} color="danger">{t('agree')}</CButton>{' '}
          <CButton color="secondary" onClick={toggle}>{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CRow>
        <CCol xl={12}>
          <CCard>
            <CCardHeader>
              <h4 className="d-inline-block">{t('listPermission')}</h4>
            </CCardHeader>
            <CCardBody>
              <Link to={{ pathname: `/management/permissions/add` }}>
                <CTooltip content={t('addPermission')} placement="right">
                  <CButton color="success" className="mb-2">
                  {t('add')} <i className="fas fa-plus"></i>
                  </CButton>
                </CTooltip>
              </Link>
              <CDataTable
                items={permissionList}
                fields={fields}
                hover
                pagination
                // tableFilter
                // sorter
                loading={loading}
                scopedSlots={{
                  'No': (item, index) => (
                    <td className="text-center">
                      {index + 1}
                    </td>
                  ),
                  'method':
                    (item) => (
                      <td>
                        {
                          item.method.replaceAll('"', '').split(',').map((objMethod, i) => (
                            item.path?.split(' ').map((objPath, j) => {
                              return (
                                <div key={j}>
                                  <CBadge color={methodColor(objMethod)} className="mr-2" style={{ width: "3rem" }} >
                                    {objMethod}
                                  </CBadge>
                                  <span style={{ fontWeight: "600", color: "#39f" }}>{objPath}</span>
                                  <br />
                                </div>
                              )
                            })
                          ))
                        }
                      </td>
                    ),
                  'functions':
                    (item, index) => {
                      return (
                        <td className="py-2">
                          <CCollapse className="d-inline-flex">
                            <CSwitch onClick={() => handleChangeStatusClick(item)} variant="outline" shape="pill"
                              className={'mx-1 mt-1 c-switch-opposite-warning'} shape={'pill'} size={'sm'} color={'warning'}
                              checked={!item.email_verified_at} onChange={() => { }} />
                            <CTooltip content={t('editPermission')} placement="top">
                              <CButton onClick={() => handleEditClick(item)} variant="outline" size="sm" color="info" className="ml-1">
                                <CIcon size={'sm'} name="cilPencil" />
                              </CButton>
                            </CTooltip>
                            <CTooltip content={t('deletePermission')} placement="top">
                              <CButton onClick={() => toggle(item)} variant="outline" size="sm" color="danger" className="ml-1">
                                <CIcon size={'sm'} name="cilTrash" />
                              </CButton>
                            </CTooltip>
                          </CCollapse>
                        </td>
                      )
                    },
                }}
              />
              {/* } */}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default PermissionList;
