import { createSlice } from '@reduxjs/toolkit';
import permissionApi from '../../../api/permissionApi'
import { setToast, clearToast } from '../../../custom_toast/toastSlice'

const permission = createSlice({
  name: 'permissions',
  initialState: {
    permissions: [],
    error: '',
    loading: false,
  },
  reducers: {
    fetchPermissionsStart: (state, action) => {
      state.loading = true
    },
    fetchPermissionsSuccess: (state, action) => {
      state.loading = false
      state.permissions = action.payload
    },
    fetchPermissionsFailure: (state, action) => {
      state.error = action.payload
      // localStorage.clear();
    },
    addPermissionStart: (state, action) => {
      state.loading = true
    },
    addPermissionSuccess: (state, action) => {
      state.loading = false;
      state.permissions.unshift(action.payload)
    },
    addPermissionFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    updatePermissionStart: (state, action) => {
      state.loading = true
    },
    updatePermissionSuccess: (state, action) => {
      state.error = false;
      state.loading = false;
      const foundIndex = state.permissions.findIndex(({ id }) => id === action.payload.id)
      state.permissions.splice(foundIndex, 1, action.payload)
    },
    updatePermissionFailure: (state, action) => {
      state.error = action.payload
    },
    deletePermissionStart: (state, action) => {
      state.loading = true
    },
    deletePermissionSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.permissions.findIndex(({ id }) => id === action.payload)
      state.permissions.splice(foundIndex, 1)
    },
    deletePermissionFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
    changeStatusPermissionStart: (state, action) => {
      state.loading = true
    },
    changeStatusPermissionSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.users?.data.findIndex(({ id }) => id === action.payload.id)
      state.users.data.splice(foundIndex, 1, action.payload)
    },
    changeStatusPermissionFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
  }
});

const { reducer, actions } = permission;
export const {
  fetchPermissionsStart, fetchPermissionsSuccess, fetchPermissionsFailure,
  addPermissionStart   , addPermissionSuccess   , addPermissionFailure,
  updatePermissionStart, updatePermissionSuccess, updatePermissionFailure,
  deletePermissionStart, deletePermissionSuccess, deletePermissionFailure,
  changeStatusPermissionStart, changeStatusPermissionSuccess, changeStatusPermissionFailure,
} = actions;
export const fetchPermissions = () => async dispatch => {
  try {
    dispatch(fetchPermissionsStart());
    const response = await permissionApi.getAll();
    if (response.success) {
      dispatch(fetchPermissionsSuccess(response.data))
    } else {
      dispatch(fetchPermissionsFailure(response));
    }
  } catch (error) {
      const valueToast = {
        message: 'Access token has expired',
        type: 'error',
        status: 401
      }
      dispatch(setToast(valueToast));
    dispatch(fetchPermissionsFailure('error'));
  }
  // dispatch(clearToast());
}
export const addPermissions = (body) => async dispatch => {
  try {
    dispatch(addPermissionStart());
    const response = await permissionApi.create(body);
    let valueToast = {}
    if (response.success === true) {
      dispatch(addPermissionSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(addPermissionFailure(response.error));
      const messages = Object.values(response.message);
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(addPermissionFailure(error));
  }
  dispatch(clearToast());
}
export const updatePermissions = (id, body) => async dispatch => {
  try {
    dispatch(updatePermissionStart());
    const response = await permissionApi.update(id, body);
    let valueToast = {}
    if (response.success) {
      dispatch(updatePermissionSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(updatePermissionFailure(response.data));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(updatePermissionFailure(error));
  }
  dispatch(clearToast());
}
export const deletePermissions = (id) => async dispatch => {
  try {
    dispatch(deletePermissionStart());
    const response = await permissionApi.delete(id);
    let valueToast = {}
    if (response.success) {
      dispatch(deletePermissionSuccess(id));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deletePermissionFailure(id));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deletePermissionFailure(error));
  }
  dispatch(clearToast());
}
export const changeStatusPermissions = (id) => async dispatch => {
  try {
    dispatch(changeStatusPermissionStart());
    const response = await permissionApi.changeStatusPermission(id);
    let valueToast = {}
    if (response.success) {
      dispatch(changeStatusPermissionSuccess(response.data[0]));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changeStatusPermissionFailure(response.data));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeStatusPermissionFailure('error'));
  }
  dispatch(clearToast());
}
export default reducer;
