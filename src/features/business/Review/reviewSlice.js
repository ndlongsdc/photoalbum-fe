import { createSlice } from '@reduxjs/toolkit';
import reviewApi from '../../../api/reviewApi'
import { setToast, clearToast } from '../../../custom_toast/toastSlice'

const review = createSlice({
  name: 'reviews',
  initialState: {
    reviews: [],
    review: '',
    reviewByBusiness: '',
    error: '',
    loading: false,
  },
  reducers: {
    fetchReviewsStart: (state, action) => {
      state.loading = true
    },
    fetchReviewsSuccess: (state, action) => {
      state.loading = false
      state.reviews = action.payload
    },
    fetchReviewsFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    fetchReviewByIdBusinessStart: (state, action) => {
      state.loading = true
      state.reviewByBusiness = []
    },
    fetchReviewByIdBusinessSuccess: (state, action) => {
      state.loading = false
      state.reviewByBusiness = action.payload
    },
    fetchReviewByIdBusinessFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    fetchReviewByIdStart: (state, action) => {
      state.loading = true
    },
    fetchReviewByIdSuccess: (state, action) => {
      state.loading = false
      state.review = action.payload
    },
    fetchReviewByIdFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    changeStatusReviewStart: (state, action) => {
      state.loading = true
    },
    changeStatusReviewSuccess: (state, action) => {
      state.loading = false;
      const foundIndexByBusiness = state.reviewByBusiness?.data?.findIndex(({ id }) => id === action.payload.id)
      const foundIndex = state.reviews?.data?.findIndex(({ id }) => id === action.payload.id)
      action.payload.byIdBusiness ? state.reviewByBusiness.data.splice(foundIndexByBusiness, 1, action.payload) : state.reviews.data.splice(foundIndex, 1, action.payload)

    },
    changeStatusReviewFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },

    changeStatusReplyStart: (state, action) => {
      state.loading = true
    },
    changeStatusReplySuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.reviewByBusiness?.findIndex(({ id }) => id === action.payload.id)
      state.reviewByBusiness.splice(foundIndex, 1, action.payload)
    },
    changeStatusReplyFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
  }
});

const { reducer, actions } = review;
export const {
  fetchReviewsStart, fetchReviewsSuccess, fetchReviewsFailure,
  fetchReviewByIdStart, fetchReviewByIdSuccess, fetchReviewByIdFailure,
  fetchReviewByIdBusinessStart, fetchReviewByIdBusinessSuccess, fetchReviewByIdBusinessFailure,
  changeStatusReviewStart, changeStatusReviewSuccess, changeStatusReviewFailure,
  changeStatusReplyStart, changeStatusReplySuccess, changeStatusReplyFailure,
} = actions;
export const fetchReviews = (param) => async dispatch => {
  try {
    dispatch(fetchReviewsStart());
    const response = await reviewApi.getAll(param);
    if (response.status = 'success') {
      dispatch(fetchReviewsSuccess(response.data.business_rating))
    } else {
      dispatch(fetchReviewsFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(fetchReviewsFailure("error"));
  }
  // dispatch(clearToast());
}
export const fetchReviewById = (id) => async dispatch => {
  try {
    dispatch(fetchReviewByIdStart());
    const response = await reviewApi.get(id);
    if (response.status = 'success') {
      dispatch(fetchReviewByIdSuccess(response.data.ratting))
    } else {
      dispatch(fetchReviewByIdFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(fetchReviewByIdFailure(error));
  }
}
export const fetchReviewByIdBusiness = (id) => async dispatch => {
  try {
    dispatch(fetchReviewByIdBusinessStart());
    const response = await reviewApi.getByBusinessId(id);
    if (response.success) {
      dispatch(fetchReviewByIdBusinessSuccess(response.data.business_rating))
    } else {
      dispatch(fetchReviewByIdBusinessFailure(response.message));
    }
  } catch (error) {
    dispatch(fetchReviewByIdBusinessFailure(error));
  }
}
export const changeStatusReviews = (id, byIdBusiness) => async dispatch => {
  try {
    dispatch(changeStatusReviewStart());
    const response = await reviewApi.changeStatus(id);
    let valueToast = {}
    if (response.success) {
      dispatch(changeStatusReviewSuccess(Object.assign(response.data, { byIdBusiness: byIdBusiness })));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changeStatusReviewFailure(response.message));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeStatusReviewFailure(error));
  }
  dispatch(clearToast());
}
export const changeStatusReplys = (id) => async dispatch => {
  try {
    dispatch(changeStatusReviewStart());
    const response = await reviewApi.changeStatusReply(id);
    let valueToast = {}
    if (response.success) {
      dispatch(changeStatusReplySuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changeStatusReplyFailure(response.message));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeStatusReplyFailure(error));
  }
  dispatch(clearToast());
}
export default reducer;
