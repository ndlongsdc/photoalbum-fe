import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import MainPage from './pages/Main';

Review.propTypes = {};

function Review(props) {
  const match = useRouteMatch();
  return (
    <>
      <Switch>
        <Route exact path={match.url} component={MainPage} />
      </Switch>
    </>
  );
}

export default Review;
