import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { changeStatusReviews, fetchReviews, fetchReviewByIdBusiness } from '../../reviewSlice';
import ReviewList from '../../components/ReviewList';

MainPage.propTypes = {};

function MainPage(props) {
  const { location } = props;
  const id = location.pathname.split('/')[location.pathname.split('/').length - 1];
  const dispatch = useDispatch();
  const reviews = useSelector(state => state.reviews.reviews);
  const reviewByBusiness = useSelector(state => state.reviews.reviewByBusiness);
  const data = Number.isInteger(parseInt(id) / 1) ? reviewByBusiness : reviews;
  useEffect(() => {
    Number.isInteger(parseInt(id) / 1) ? dispatch(fetchReviewByIdBusiness(id)) : reviews.length===0 && id==="reviews" && dispatch(fetchReviews());
  }, [dispatch])

  const onChangeStatusClick = (review) => {
    const byIdBusiness = Number.isInteger(parseInt(id) / 1);
    const reviewId = review.id;
    const action = changeStatusReviews(reviewId,byIdBusiness);
    dispatch(action);
  }

  const onPageChange = (param) => {
    const action = fetchReviews(param);
    dispatch(action);
  }
  return (
    <>
      <ReviewList
        reviewList={[data]}
        onChangeStatusClick={onChangeStatusClick}
        onPageChange={onPageChange}
      />
    </>
  );
}

export default MainPage;
