import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
// import { changeStatusReplys } from '../../reviewSlice';
import '../ReviewList/style.css'
import ReviewDetail from '../ReviewDetail/index'
import { useTranslate } from 'react-redux-multilingual'

import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CCollapse,
  CPagination,
  CSwitch,
  CTooltip
  // CPagination
} from '@coreui/react'
import { Link } from 'react-router-dom';

ReviewList.propTypes = {
  reviewList: PropTypes.array,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
};

ReviewList.defaultProps = {
  reviewList: [],
  onEditClick: null,
  onRemoveClick: null,
};

// const getBadge = status => {
//   switch (status) {
//     case 1: return 'success'
//     case true: return 'success'
//     case 0: return 'danger'
//     case false: return 'danger'
//     default: return 'primary'
//   }
// }

function ReviewList(props) {
  const { reviewList, onChangeStatusClick, onPageChange } = props;
  const dispatch = useDispatch();
  const reviewData = reviewList[0] ? reviewList[0] : [];
  let valueList = reviewList[0] ? reviewList[0].data : [];
  const t = useTranslate()


  const [modal, setModal] = useState(false);
  const [currentReview, setcurrentReview] = useState();
  const toggle = () => {
    setModal(!modal);
  }

  const handleChangeStatusClick = (item) => {
    if (onChangeStatusClick) onChangeStatusClick(item);
  }

  const handleReviewDetailClick = (item) => {
    toggle();
    setcurrentReview(item);
  }

  function handlePageChange(newPage) {
    if (onPageChange) onPageChange(newPage);
  }

  const fields = [
    // {
    //   key: 'checkBox',
    //   label: '',
    //   _style: { width: '3%' },
    //   sorter: false,
    //   filter: false
    // },
    {
      key: 'No',
      label: t('no'),
      _classes: 'text-center',
      _style: { width: '5%' },
    },
    {
      key: 'business',
      label: t('businessName'),
    },
    {
      key: 'user',
      label: t('reviewer'),
    },
    {
      key: 'content',
      label: t('content'),
      _style: { width: '50%' },
    },
    {
      key: 'functions',
      label: t('actions'),
      _style: { width: '9%', textAlign: 'center' },
    }
  ]

  const oncloseModal = (params) => {
    setModal(params)
  }

  const changeStatusReplys = (params) => {
    dispatch(changeStatusReplys(params.id))
  }
  const loading = useSelector(state => state.reviews.loading)
  return (
    <>
      <ReviewDetail currentReview={currentReview} showModal={modal} closeModal={oncloseModal} onChangeStatusReply={changeStatusReplys} />
      <CRow>
        <CCol xl={12}>
          <CCard>
            <CCardHeader>
              <h4 className="d-inline-block">{t('listReview')}</h4>
              <Link to={{ pathname: `/business/reviews/add` }}>
              </Link>
            </CCardHeader>
            <CCardBody>
              <CDataTable
                items={reviewData.length === 0 ? [] : valueList}
                fields={fields}
                hover
                loading={loading}
                scopedSlots={{
                  'No': (item, index) => (
                    <td className="text-center">
                      {index + 1}
                    </td>
                  ),
                  'business': (item) => (
                    <td>{item.business.name}</td>
                  ),
                  'user': (item) => (
                    <td>{item.user?.fullname}</td>
                  ),
                  'functions':
                    (item, index) => {
                      return (
                        <td className="py-2 text-center">
                          <CCollapse className="d-inline-flex">
                            <CSwitch onClick={() => handleChangeStatusClick(item)}
                              className={'mx-1 mt-1 c-switch-opposite-warning'} shape={'pill'} size={'sm'} color={'warning'}
                              checked={item.status} onChange={() => { }} />
                            <CTooltip content={t('detailReview')} placement="top">
                              <CButton onClick={() => handleReviewDetailClick(item)} variant="outline" size="sm" color="dark" className="ml-1">
                                <i className="far fa-eye"></i>
                              </CButton>
                            </CTooltip>
                          </CCollapse>
                        </td>
                      )
                    },
                }}
              />
              {reviewData.length !== 0 && <CPagination activePage={reviewData.current_page} onActivePageChange={handlePageChange} pages={reviewList[0].last_page} />}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default ReviewList;
