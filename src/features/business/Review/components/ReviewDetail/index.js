import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  CButton, CModal,
  CModalHeader, CModalBody, CModalFooter, CSwitch,
} from '@coreui/react'
import './style.scss'
import { fetchReviewById } from '../../reviewSlice'
import { useTranslate } from 'react-redux-multilingual'

const API_ENDPOINT = process.env.REACT_APP_API_URL_IMAGE;

function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [day, month, year].join('-');
}
function ReviewDetail(props) {
  const dispatch = useDispatch();
  const t = useTranslate()
  const review = useSelector(state => state.reviews.review);
  const { showModal, closeModal, currentReview, onChangeStatusReply } = props;
  const id = currentReview?.id;
  useEffect(() => {
    showModal && dispatch(fetchReviewById(id));
  }, [showModal])

  const changeStatusReplys = (data) => {
    onChangeStatusReply(data);
  }
  return (
    <CModal show={showModal} onClose={() => closeModal(!showModal)} size="xl">
      <CModalHeader closeButton>Review</CModalHeader>
      <CModalBody>
        <div className="chatContainer">
          {/* <div className="chatTitleContainer">Comments</div> */}
          <div className="chatHistoryContainer">
            <ul className="formComments">
              <li className="commentLi commentstep-1" data-commentid="4">
                <table className="form-comments-table">
                  <tbody>
                    <tr>
                      <td><div className="comment-timestamp">{formatDate(review.updated_at)}</div></td>
                      <td><div className="comment-user">{review.user?.fullname}</div></td>
                      <td>
                        <div id="comment-4" data-commentid="4" className="comment comment-step1">
                          {review.content}
                        </div>
                        {review.images && review.images.length !== 0 && <div className="comment-avatar">
                          {review.images?.map((obj, index) => (
                            <a href={obj.path + "/" + obj.name} target="_blank" key={index}>
                              <img src={obj.path + "/" + obj.name} />
                            </a>
                          ))}
                        </div>}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </li>
              {review.comment_ratings?.map((obj, index) => (
                <li className="commentLi commentstep-2" data-commentid="7" key={index}>
                  <table className="form-comments-table">
                    <tbody>
                      <tr>
                        <td><div className="comment-timestamp">{formatDate(obj.updated_at)}</div></td>
                        <td><div className="comment-user">Ollie Bott</div></td>
                        <td>
                          <div id="comment-7" data-commentid="7" className="comment">
                            {obj.comment}
                          </div>
                        </td>
                        <td>
                          {/* <CSwitch onClick={() => changeStatusReplys(obj)}
                            className={'mx-1 mt-1 '} shape={'pill'} size={'sm'} color={'warning'}
                            checked={obj.status} onChange={() => { }} /> */}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </li>

              ))}
            </ul>
          </div>
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton color="secondary" onClick={() => closeModal(!showModal)}>close</CButton>
      </CModalFooter>
    </CModal>

  )
}
export default ReviewDetail;
