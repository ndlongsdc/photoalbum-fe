import { createSlice } from '@reduxjs/toolkit';
import businessApi from '../../../api/businessApi'
import { setToast, clearToast } from '../../../custom_toast/toastSlice'
import { post } from 'axios';
// import { reset } from 'enzyme/build/configuration';
const API_ENDPOINT = process.env.REACT_APP_API_URL;

const business = createSlice({
  name: 'businesses',
  initialState: {
    businesses: [],
    userEmployee: [],
    error: '',
    loading: false
  },
  reducers: {
    fetchBusinessesStart: (state, action) => {
      state.loading = true
    },
    fetchBusinessesSuccess: (state, action) => {
      state.loading = false
      state.businesses = action.payload
    },
    fetchBusinessesFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    addBusinessStart: (state, action) => {
      state.loading = true
    },
    addBusinessSuccess: (state, action) => {
      state.loading = false;
      state.businesses.data.unshift(action.payload)
    },
    addBusinessFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    updateBusinessStart: (state, action) => {
      state.loading = true
    },
    updateBusinessSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.businesses?.data.findIndex(({ id }) => id === action.payload.id)
      state.businesses.data.splice(foundIndex, 1, action.payload)
    },
    updateBusinessFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    deleteBusinessStart: (state, action) => {
      state.loading = true
    },
    deleteBusinessSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.businesses?.data.findIndex(({ id }) => id === action.payload)
      state.businesses.data.splice(foundIndex, 1)
    },
    deleteBusinessFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    deleteMultiBusinessStart: (state, action) => {
      state.loading = true
    },
    deleteMultiBusinessSuccess: (state, action) => {
      const ids = action.payload.split(",");
      ids.map((itemId) => {
        const foundIndex = state.businesses?.data.findIndex(({ id }) => id === itemId)
        state.businesses.data.splice(foundIndex, 1)
        return null;
      })
      state.loading = false;

    },
    deleteMultiBusinessFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    changeStatusBusinessStart: (state, action) => {
      state.loading = true
    },
    changeStatusBusinessSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.businesses?.data.findIndex(({ id }) => id === action.payload.id)
      state.businesses.data.splice(foundIndex, 1, action.payload)
    },
    changeStatusBusinessFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    searchBusinessesStart: (state, action) => {
      state.loading = true
    },
    searchBusinessesSuccess: (state, action) => {
    state.loading = false
      state.businesses = action.payload
    },
    searchBusinessesFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    sortBusinessesStart: (state, action) => {
      state.loading = true
    },
    sortBusinessesSuccess: (state, action) => {
      state.loading = false
      state.businesses = action.payload
    },
    sortBusinessesFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    listAddUserEmployeeStart: (state, action) => {
      state.loading = true
    },
    listAddUserEmployeeSuccess: (state, action) => {
      state.loading = false
      state.userEmployee = action.payload
    },
    listAddUserEmployeeFailure: (state, action) => {
      state.error = action.payload
    },
  }
});

const { reducer, actions } = business;
export const {
  fetchBusinessesStart, fetchBusinessesSuccess, fetchBusinessesFailure,
  addBusinessStart, addBusinessSuccess, addBusinessFailure,
  updateBusinessStart, updateBusinessSuccess, updateBusinessFailure,
  deleteBusinessStart, deleteBusinessSuccess, deleteBusinessFailure,
  deleteMultiBusinessStart, deleteMultiBusinessSuccess, deleteMultiBusinessFailure,
  changeStatusBusinessStart, changeStatusBusinessSuccess, changeStatusBusinessFailure,
  searchBusinessesStart, searchBusinessesSuccess, searchBusinessesFailure,
  sortBusinessesStart, sortBusinessesSuccess, sortBusinessesFailure,
  listAddUserEmployeeStart, listAddUserEmployeeSuccess, listAddUserEmployeeFailure,
} = actions;
export const fetchBusinesses = (params) => async dispatch => {
  try {
    dispatch(fetchBusinessesStart());
    const response = await businessApi.get(params);
    if (response.success) {
      dispatch(fetchBusinessesSuccess(response.data.businesses))
    } else {
      const valueToast = {
        message: response.message,
        type: 'error',
      }
      dispatch(setToast(valueToast));
      dispatch(fetchBusinessesFailure(response.message));
    }
  } catch (error) {
    dispatch(fetchBusinessesFailure('error'));
    const valueToast = {
      message: 'Access token has expired',
      type: 'error',
      status: 401
    }
    dispatch(setToast(valueToast));
  }
  dispatch(clearToast());
}
export const addBusinesses = (body) => async dispatch => {
  try {
    dispatch(addBusinessStart());
    delete body.id;
    body.latitudes = 0;
    body.longitudes = 0;
    const formData = new FormData();
    Object.keys(body).forEach(key => formData.append(key, body[key]));
    const url = API_ENDPOINT + '/businesses';
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: 'Bearer ' + localStorage.getItem("auth:access_token")
      }
    }
    const response = await post(url, formData, config)
    let valueToast = {}
    if (response.data.success === true) {
      dispatch(addBusinessSuccess(response.data.data.businesses));
      valueToast = {
        message: response.data.message,
        type: 'success'
      }
    } else {
      dispatch(addBusinessFailure(response.data.errors));
      const messages = typeof response.data.message === 'array' ? Object.values(response.data.message) : response.data.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(addBusinessFailure(error));
  }
  dispatch(clearToast());
}
export const updateBusinesses = (id, body) => async dispatch => {
  console.log(body);
  delete body.id;
  body.latitudes = 0;
  body.longitudes = 0;
  try {
    dispatch(updateBusinessStart());
    body._method = "PUT";
    const formData = new FormData();
    Object.keys(body).forEach(key => formData.append(key, body[key]));
    const url = API_ENDPOINT + '/businesses/' + id;
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: 'Bearer ' + localStorage.getItem("auth:access_token")
      }
    }
    // for (var pair of formData.entries()) {
    //   console.log(pair[0] + ', ' + pair[1]);
    // }
    const response = await post(url, formData, config)
    let valueToast = {}
    if (response.data.success === true) {
      dispatch(updateBusinessSuccess(response.data.data.business));
      valueToast = {
        message: response.data.message,
        type: 'success'
      }
    } else {
      dispatch(updateBusinessFailure(response.data.errors));
      const messages = typeof response.data.message === 'array' ? Object.values(response.data.message) : response.data.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    console.log(error);
    dispatch(updateBusinessFailure(error));
  }
  dispatch(clearToast());
}
export const deleteBusinesses = (id) => async dispatch => {
  try {
    dispatch(deleteBusinessStart());
    const response = await businessApi.delete(id);
    let valueToast = {}
    if (response.success) {
      dispatch(deleteBusinessSuccess(id));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deleteBusinessFailure(id));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deleteBusinessFailure(error));
  }
  dispatch(clearToast());
}
export const deleteMultiBusinesses = (ids) => async dispatch => {
  try {
    dispatch(deleteMultiBusinessStart());
    const response = await businessApi.deleteMulti(ids);
    let valueToast = {}
    if (response.success) {
      dispatch(deleteMultiBusinessSuccess(ids));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deleteMultiBusinessFailure(ids));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deleteMultiBusinessFailure(error));
  }
  dispatch(clearToast());
}
export const changeStatusBusinesses = (id) => async dispatch => {
  try {
    dispatch(changeStatusBusinessStart());
    const response = await businessApi.changeStatusBusiness(id);
    console.log(response);
    let valueToast = {}
    if (response.success) {
      dispatch(changeStatusBusinessSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changeStatusBusinessFailure(response.data));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeStatusBusinessFailure('error'));
  }
  dispatch(clearToast());
}
export const searchBusinesses = (params) => async dispatch => {
  try {
    dispatch(searchBusinessesStart());
    const response = await businessApi.filter(params);
    if (response.success) {
      dispatch(searchBusinessesSuccess(response.data.bussinesses))
    } else {
      dispatch(searchBusinessesFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: false
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(searchBusinessesFailure("error"));
  }
  dispatch(clearToast());
}
export const sortBusinesses = (params) => async dispatch => {
  try {
    dispatch(sortBusinessesStart());
    const response = await businessApi.filter(params);
    if (response.success === true) {
      dispatch(sortBusinessesSuccess(response.data.bussinesses))
    } else {
      dispatch(sortBusinessesFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: false
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(sortBusinessesFailure("error"));
  }
  dispatch(clearToast());
}
export const listAddUserEmployee = () => async dispatch => {
  try {
    dispatch(listAddUserEmployeeStart());
    const response = await businessApi.listAddUserEmployee();
    if (response.success) {
      dispatch(listAddUserEmployeeSuccess(response.data.listEmployeesUser))
    } else {
      dispatch(listAddUserEmployeeFailure(response.message));
    }
  } catch (error) {
    dispatch(listAddUserEmployeeFailure('error'));
  }
  dispatch(clearToast());
}
export default reducer;
