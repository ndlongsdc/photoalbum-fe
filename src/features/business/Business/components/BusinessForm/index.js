import React, { useState } from 'react'
import InputField from '../../../../../custom_fields/inputField';
import TextareaField from '../../../../../custom_fields/textareaField';
import { Formik, FastField, Form } from 'formik'
import selectField from '../../../../../custom_fields/selectField';
import * as Yup from 'yup'
import '../BusinessForm/style.scss'
import {
  CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CFormGroup, CSpinner, CLabel,
  CTabs, CNav, CNavItem, CNavLink, CTabContent, CTabPane
} from '@coreui/react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from "react-router-dom";
import { useTranslate } from 'react-redux-multilingual'

BusinessForm.propTypes = {
  onSubmit: PropTypes.func,
};

BusinessForm.defaultProps = {
  onSubmit: null,
}

function BusinessForm(props) {
  const t = useTranslate()
  const { id } = useParams();
  let history = useHistory();
  // const dispatch = useDispatch();
  const { initialValues } = props;
  const formValues = localStorage.getItem("formValues");
  const valueForm = initialValues || formValues;
  id !== 'add' && valueForm.translations.map((obj) => {
    if (obj.language_id === "vi") {
      valueForm.nameVi = obj.name;
      valueForm.addressVi = obj.address;
      valueForm.descriptionVi = obj.description;
    } else if (obj.language_id === "en") {
      valueForm.nameEn = obj.name;
      valueForm.addressEn = obj.address;
      valueForm.descriptionEn = obj.description;
    }
  })
  const moment = require("moment");
  const validationSchema = Yup.object().shape({
    nameVi: Yup.string()
      .required(t('businessName_required')),
    addressVi: Yup.string()
      .required(t('address_required')),
    nameEn: Yup.string()
      .required(t('businessName_required')),
    addressEn: Yup.string()
      .required(t('address_required')),
    phone: Yup.string()
      .required(t('phone_required')),
    // open_hour: Yup.string().required("start time cannot be empty"),
    close_hour:Yup
    .string()
    // .required("end time cannot be empty")
    .test("is-greater", t('close_hour_should_be_greater'), function(value) {
      const { open_hour } = this.parent;
      return moment(value, "HH:mm").isSameOrAfter(moment(open_hour, "HH:mm"));
    })
  })

  // const genderOption = [
  //   { value: 1, key: 'Male' },
  //   { value: 0, key: 'Female' }
  // ];
  // dispatch(fetchBusinessTypes());
  const businessTypes = useSelector(state => state.businessTypes.businessTypes);
  let businessTypesOption = [];
  businessTypes.map((obj) => {
    businessTypesOption.push({ "label": obj.name, "value": obj.id })
    return null;
  })
  businessTypesOption.length !== 0 && localStorage.setItem("businessTypesOption", JSON.stringify(businessTypesOption));
  businessTypesOption = businessTypesOption.length !== 0 ? businessTypesOption : JSON.parse(localStorage.getItem("businessTypesOption"));

  const status = useSelector(state => state.toasts?.toastType)
  status === 'success' && history.push('/business/businesses');
  const [showMap, setShowMap] = useState(false)
  const toggle = () => {
    setShowMap(!showMap);
  }
  function initMap() {
    const google = window.google;
    const myLatlng = { lat: 16.071016734350295, lng: 108.21983277797699 };
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 14,
      center: myLatlng,
    });
    // Create the initial InfoWindow.
    let infoWindow = new google.maps.InfoWindow({
      content: "Click the map to get Lat/Lng!",
      position: myLatlng,
    });
    console.log(map)
    infoWindow.open(map);
    // Configure the click listener.
    map.addListener("click", (mapsMouseEvent) => {
      // Close the current InfoWindow.
      infoWindow.close();
      // Create a new InfoWindow.
      infoWindow = new google.maps.InfoWindow({
        position: mapsMouseEvent.latLng,
      });
      infoWindow.setContent(
        JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
      );
      console.log(JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2))
      console.log(mapsMouseEvent)

      infoWindow.open(map);
    });
  }
  return (<>
    <Formik
      enableReinitialize
      initialValues={
        {
          id: valueForm.id || '',
          nameVi: valueForm.nameVi || '',
          addressVi: valueForm.addressVi || '',
          descriptionVi: valueForm.descriptionVi || '',
          nameEn: valueForm.nameEn || '',
          addressEn: valueForm.addressEn || '',
          descriptionEn: valueForm.descriptionEn || '',
          phone: valueForm.phone || '',
          open_hour: valueForm.open_hour || '',
          close_hour: valueForm.close_hour || '',
          type: valueForm.type,
        }
      }
      validationSchema={validationSchema}
      onSubmit={
        props.onSubmit
      }
    >
      {formikProps => {
        const { isSubmitting } = formikProps;
        const { submitCount } = formikProps;
        const { errors } = formikProps;
        const errorTabVi = errors.addressVi || errors.nameVi || errors.phone;
        const errorTabEn = errors.addressEn || errors.nameEn || errors.phone;
        const flickerVi = errorTabVi && submitCount;
        const flickerEn = errorTabEn && submitCount;
        const checkErrTab = (value) => {
          if (value) {
            return "blink-bg";
          } else {
            return "";
          }
        }


        const locationClick = () => {
          setShowMap(true);
          initMap();
        }
        return (
          <>
            {/* <CModal
              show={showMap}
              onClose={toggle}
            >
              <div id="map" style={{height:"100%"}}></div>
            </CModal> */}
            <div style={{ height: "100%", width: "100%" }}>
              <div id="map"></div>
            </div>
            {/* <div id="map" style={{ height: "100%" }}></div> */}
            <CRow>
              <CCol xs="12">
                <CCard>
                  <CCardHeader>
                    <h4>{id === 'add' ? t('addBusiness') : t('editBusiness')}</h4>
                  </CCardHeader>
                  <CCardBody>
                    <Form className="form-horizontal">
                      <CTabs activeTab="vi">
                        <CNav variant="tabs">
                          <CNavItem>
                            <CNavLink data-tab="vi" className={checkErrTab(flickerVi)}>
                              {t('vietnamese')} {errorTabVi && <span className="text-danger">*</span>}
                            </CNavLink>
                          </CNavItem>
                          <CNavItem>
                            <CNavLink data-tab="en" className={checkErrTab(flickerEn)}>
                              {t('english')} {errorTabEn && <span className="text-danger">*</span>}
                            </CNavLink>
                          </CNavItem>
                        </CNav>
                        <CTabContent style={{ marginBottom: "-45px" }}>
                          <CTabPane data-tab="vi">
                            <CCard style={{ border: 0 }}>
                              <CCardBody>
                                <CFormGroup>
                                  <FastField
                                    name="nameVi"
                                    component={InputField}
                                    label={t('name')}
                                    placeholder={t('name')}
                                    requiredlabel
                                  />
                                </CFormGroup>
                                <CFormGroup>
                                  <FastField
                                    name="addressVi"
                                    component={InputField}
                                    label={t('address')}
                                    placeholder={t('address')}
                                    requiredlabel
                                  />
                                </CFormGroup>
                                <CFormGroup>
                                  <FastField
                                    name="descriptionVi"
                                    component={TextareaField}
                                    label={t('description')}
                                    placeholder={t('description')}
                                  />
                                </CFormGroup>
                              </CCardBody>
                            </CCard>
                          </CTabPane>
                          <CTabPane data-tab="en">
                            <CCard style={{ border: 0 }}>
                              <CCardBody>
                                <CFormGroup>
                                  <FastField
                                    name="nameEn"
                                    component={InputField}
                                    label={t('name')}
                                    placeholder={t('name')}
                                    requiredlabel
                                  />
                                </CFormGroup>
                                <CFormGroup>
                                  <FastField
                                    name="addressEn"
                                    component={InputField}
                                    label={t('address')}
                                    placeholder={t('address')}
                                    requiredlabel
                                  />
                                </CFormGroup>
                                <CFormGroup>
                                  <FastField
                                    name="descriptionEn"
                                    component={TextareaField}
                                    label={t('description')}
                                    placeholder={t('description')}
                                  />
                                </CFormGroup>
                              </CCardBody>
                            </CCard>
                          </CTabPane>
                        </CTabContent>
                      </CTabs>
                      <div style={{ margin: "0 1.2rem 0 1.3rem" }}>
                        <CFormGroup row>
                          <CCol md="2">
                            <CLabel htmlFor="featured_image">{t('image')}</CLabel>
                          </CCol>
                          <CCol xs="12" md="10">
                            <input
                              name="featured_image"
                              type="file"
                              onChange={(event) => formikProps.setFieldValue("featured_image", event.currentTarget.files[0])}
                              className={`${formikProps.errors.featured_image ? "is-invalid" : ""}`}
                            />
                            <div className="text-danger" style={{ fontSize: '80%' }} >{formikProps.errors.featured_image}</div></CCol>
                        </CFormGroup>
                        <CFormGroup>
                          <FastField
                            name="phone"
                            component={InputField}
                            label={t('phone')}
                            placeholder={t('phone')}
                            requiredlabel
                          />
                        </CFormGroup>
                        {/* <CRow>
                          <CCol md="2"><CLabel htmlFor='location'>Location</CLabel><span className="text-danger">*</span></CCol>
                          <CCol>
                            <div className="input-group mb-3">
                              <input name="location" type="text" autoComplete="off" className="form-control" placeholder="Location" onClick={() => locationClick()} />
                              <div className="input-group-append">
                                <span className="input-group-text" id="basic-addon2"><i className="fas fa-map-marked-alt"></i></span>
                              </div>
                            </div>
                          </CCol>

                        </CRow> */}
                        {/* location */}
                        <CRow>
                          <CCol md="2"><CLabel htmlFor='open_hour'>{t('open')}</CLabel></CCol>
                          <CCol md="4" lg="4" xl="3">
                            <CFormGroup>
                              <FastField
                                type="time"
                                component={InputField}
                                // onChange={(event) => formikProps.setFieldValue("open_hour", event.currentTarget.value)}
                                className='form-control'
                                name="open_hour" />
                            </CFormGroup>
                          </CCol>
                          <CCol style={{ maxWidth: "100px" }} className="mt-2">{t('to')}</CCol>
                          <CCol md="4" lg="4" xl="3">
                            <CFormGroup>
                              <FastField
                                type="time"
                                component={InputField}
                                // onChange={(event) => formikProps.setFieldValue("close_hour", event.currentTarget.value)}
                                className='form-control'
                                name="close_hour" />
                            </CFormGroup>
                          </CCol>
                        </CRow>
                        <CFormGroup>
                          <FastField
                            name="type"
                            component={selectField}
                            options={businessTypesOption}
                            placeholder={t('businessType')}
                            label={t('businessType')}
                          />
                        </CFormGroup>
                      </div>
                      <div className="form-actions float-right">
                        <CButton type="submit" color={id === 'add' ? 'primary' : 'success'}>{isSubmitting && <CSpinner size="sm" />}
                          {id === 'add' ? t('add') : t('edit')}</CButton>
                        <Link to={{ pathname: `/business/businesses` }}>
                          <CButton color="secondary ml-1">{t('cancel')}</CButton>
                        </Link>
                      </div>
                    </Form>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </>
        )
      }}
    </Formik>
  </>
  )
}

export default BusinessForm
