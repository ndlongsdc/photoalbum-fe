import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CIcon from '@coreui/icons-react';
import '../BusinessList/style.css'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchBusinesses, searchBusinesses, sortBusinesses } from '../../businessSlice';
import { useForm } from 'react-hook-form'
import { fetchBusinessTypes } from '../../../BusinessType/businessTypeSlice';
import { useTranslate } from 'react-redux-multilingual'

import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CCollapse,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CSwitch,
  CFormGroup,
  CInputCheckbox,
  CPagination,
  CTooltip
} from '@coreui/react'

BusinessList.propTypes = {
  businessList: PropTypes.array,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
};

BusinessList.defaultProps = {
  businessList: [],
  onEditClick: null,
  onRemoveClick: null,
};

const API_ENDPOINT = process.env.REACT_APP_API_URL_IMAGE;

function BusinessList(props) {
  const { businessList, onEditClick, onRemoveClick, onRemoveMultiClick, onChangeStatusClick, onViewClick, onReviewClick } = props;
  const t = useTranslate()
  const dataBusiness = useSelector(state => state.businesses.businesses)
  const loading = useSelector(state => state.businesses.loading)
  const dataBusinessTypes = useSelector(state => state.businessTypes.businessTypes)
  const dispatch = useDispatch();
  const { register, handleSubmit } = useForm();

  const [searchList, setsearchList] = useState();
  const [sortList, setSortList] = useState();
  const [itemSelected, setItemSelected] = useState();
  const [selects, setSelects] = useState([])
  const [modal, setModal] = useState(false);
  const [modalDeleteMulti, setmodalDeleteMulti] = useState(false);
  const [filters, setFilters] = useState({
    pageSize: 10,
    page: 1,
    keyword: '',
    sortBy: '',
    field: ''
  })

  useEffect(() => {
    dataBusinessTypes.length === 0 && dispatch(fetchBusinessTypes());
  }, [dispatch])

  if (localStorage.getItem('business')) {
    localStorage.removeItem('business')
  }

  let valueList = [];
  if (businessList.length) {
    businessList.map((business, i) => {
      business.translations.map((obj, j) => {
        if (obj.language_id === localStorage.getItem('locale')) {
          valueList.push(Object.assign({ "name": obj.name, "address": obj.address, "description": obj.description }, businessList[i]));
        }
      })
    })
  }

  const onSubmitSearch = data => {
    filters.keyword = data.value;
    filters.field = 1;
    setsearchList(data);
    setTimeout(() => {
      dispatch(searchBusinesses(filters));
    }, 1234);
  }

  const handleReviewClick = (item) => {
    if (onReviewClick) onReviewClick(item);
    // localStorage.setItem("dataViewBusiness", JSON.stringify(item));
  }

  const handleViewClick = (item) => {
    if (onViewClick) onViewClick(item);
    localStorage.setItem("dataViewBusiness", JSON.stringify(item));
  }

  const handleEditClick = (item) => {
    if (onEditClick) onEditClick(item);
    localStorage.setItem("formValues", JSON.stringify(item));
  }

  const handleChangeStatusClick = (item) => {
    if (onChangeStatusClick) onChangeStatusClick(item);
  }

  const handleRemoveClick = () => {
    if (onRemoveClick) onRemoveClick(itemSelected);
    setModal(!modal);
  }

  const toggleSelects = (item) => {
    const position = selects.indexOf(item)

    let newSelects = selects.slice()
    if (position !== -1) {
      newSelects.splice(position, 1)
    } else {
      newSelects = [...selects, item]
    }
    setSelects(newSelects)
  }

  const handleRemoveMultiClick = () => {
    if (onRemoveMultiClick) onRemoveMultiClick(selects.toString());
    setmodalDeleteMulti(!modalDeleteMulti)
    setSelects([]);
  }

  const toggle = (item) => {
    setItemSelected(item)
    setModal(!modal);
  }

  const toggleDeleteMulti = (item) => {
    setItemSelected(item)
    setmodalDeleteMulti(!modalDeleteMulti);
  }

  function handlePageChange(newPage) {
    setFilters({
      ...filters,
      pageNumber: newPage,
    });
    if (searchList) {
      searchList.page = newPage;
      dispatch(searchBusinesses(searchList))
    } else if (sortList) {
      sortList.page = newPage;
      dispatch(sortBusinesses(sortList));
    } else {
      dispatch(fetchBusinesses(newPage));
    }
  }

  function handleSortNameClick(data) {
    filters.sortBy = data.sort;
    filters.field = 1;
    setSortList(data)
    dispatch(sortBusinesses(filters));
  }

  function handleSortTypeClick(data) {
    filters.sortBy = data.sort;
    filters.field = 2;
    setSortList(data)
    dispatch(sortBusinesses(filters));
  }
  const fields = [
    // {
    //   key: 'checkBox',
    //   label: '',
    //   _style: { width: '3%' },
    //   sorter: false,
    //   filter: false
    // },
    {
      key: 'No',
      label: t('no'),
      sorter: false,
      filter: false,
      _style: { width: '1%' },
    },
    {
      key: 'featured_image',
      label: t('image'),
      sorter: false,
      filter: false,
      _style: { width: '1%' },
    },
    {
      key: 'name',
      label: t('name'),
      _classes: 'position-relative sortName'
    },
    {
      key: 'type',
      label: t('type'),
      _classes: 'position-relative sortType'
    },
    {
      key: 'description',
      label: t('description'),
      sorter: false,
      filter: false
    },
    {
      key: 'rating',
      label: t('rating'),
      sorter: false,
      filter: false,
      _style: { width: '8%', textAlign: 'center' },
    },
    {
      key: 'active',
      label: t('active'),
      _style: { width: '9%' },
      sorter: false,
      filter: false
    },
    {
      key: 'functions',
      label: t('actions'),
      _style: { width: '1%', textAlign: 'center' },
      sorter: false,
      filter: false
    },
  ]

  //custom sort
  useEffect(() => {
    var sortName = document.getElementsByClassName('sortName')[0]
    var sortType = document.getElementsByClassName('sortType')[0]
    var divName = document.createElement("div");
    var divType = document.createElement("div");
    divName.innerHTML = '<svg width="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" id="asc" class="position-absolute cursor-pointer CDataTable_icon-transition__1TJkV CDataTable_arrow-position__1hvHj CDataTable_transparent__qAZ-y" role="img"><polygon fill="var(--ci-primary-color, currentColor)" points="390.624 150.625 256 16 121.376 150.625 144.004 173.252 240.001 77.254 240.001 495.236 272.001 495.236 272.001 77.257 367.996 173.252 390.624 150.625" class="ci-primary"></polygon></svg>';
    let idSVGName = divName.childNodes[0].id;
    divName.addEventListener("click", function () {
      let value = { "sort": idSVGName }
      handleSortNameClick(value)
      if (idSVGName === 'asc') {
        idSVGName = 'desc';
        divName.childNodes[0].classList.add('CDataTable_rotate-icon__3Fis6');
      } else {
        idSVGName = 'asc';
        divName.childNodes[0].classList.remove('CDataTable_rotate-icon__3Fis6');
      }
    })
    divType.innerHTML = '<svg width="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" id="asc" class="position-absolute cursor-pointer CDataTable_icon-transition__1TJkV CDataTable_arrow-position__1hvHj CDataTable_transparent__qAZ-y" role="img"><polygon fill="var(--ci-primary-color, currentColor)" points="390.624 150.625 256 16 121.376 150.625 144.004 173.252 240.001 77.254 240.001 495.236 272.001 495.236 272.001 77.257 367.996 173.252 390.624 150.625" class="ci-primary"></polygon></svg>';
    let idSVGType = divType.childNodes[0].id;
    divType.addEventListener("click", function () {
      let value = { "sort": idSVGType }
      handleSortTypeClick(value)
      if (idSVGType === 'asc') {
        idSVGType = 'desc';
        divType.childNodes[0].classList.add('CDataTable_rotate-icon__3Fis6');
      } else {
        idSVGType = 'asc';
        divType.childNodes[0].classList.remove('CDataTable_rotate-icon__3Fis6');
      }
    })

    sortType && sortType.appendChild(divType)
    sortName && sortName.appendChild(divName)
  }, [])


  return (
    <>
      <CModal show={modal} onClose={toggle}>
        <CModalHeader closeButton>{t('deleteBusiness')}</CModalHeader>
        <CModalBody>{t('confirmDeleteBusiness')} "{itemSelected?.name}"?</CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveClick()} color="danger">{t('agree')}</CButton>{' '}
          <CButton color="secondary" onClick={toggle}>{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CModal show={modalDeleteMulti} onClose={toggleDeleteMulti}>
        <CModalHeader closeButton>{selects?.length > 1 ? t('deleteBusinesses') : t('deleteBusiness')}</CModalHeader>
        <CModalBody>{selects?.length > 1 ? t('confirmDeleteBusinesses') : t('confirmDeleteBusiness')} ?</CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveMultiClick()} color="danger">{t('agree')}</CButton>{' '}
          <CButton color="secondary" onClick={toggleDeleteMulti}>{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CRow>
        <CCol xl={12}>
          <CCard>
            <CCardHeader>
              <h4 className="d-inline-block">{t('listBusiness')}</h4>
            </CCardHeader>
            <CCardBody>
              <form onSubmit={handleSubmit(onSubmitSearch)} >
                <div className="row my-2 mx-0">
                  <div className="col-sm-6 p-0">
                    <Link to={{ pathname: `/business/businesses/add` }}>
                      <CTooltip content={t('addBusiness')} placement="right">
                        <CButton color="success">
                          {t('add')} <i className="fas fa-plus"></i>
                        </CButton>
                      </CTooltip>
                    </Link>
                    {/*<CTooltip content={selects?.length > 1 ? t('deleteBusinesses') : t('deleteBusiness')} placement="left">*/}
                    {/*  <CButton*/}
                    {/*    color="danger"*/}
                    {/*    className="ml-2"*/}
                    {/*    style={selects.length > 0 ? {} : { display: 'none' }}*/}
                    {/*    onClick={() => toggleDeleteMulti()} >*/}
                    {/*    {t('delete')} <i className="fas fa-trash"></i>*/}
                    {/*  </CButton>*/}
                    {/*</CTooltip>*/}
                  </div>
                  <div className="col-sm-6 form-inline p-0 justify-content-sm-end">
                    <input
                      className="form-control"
                      autoComplete="off"
                      name="value"
                      type="text"
                      placeholder={t('typeString')}
                      onChange={handleSubmit(onSubmitSearch)}
                      ref={register()} />
                  </div>
                </div>
              </form>
              <CDataTable
                items={valueList}
                fields={fields}
                hover
                loading={loading}
                scopedSlots={{
                  'No': (item, index) => (
                    <td className="text-center">
                      {((dataBusiness.current_page - 1) * 10 + index + 1)}
                    </td>
                  ),
                  // 'checkBox': (item) => (
                  //   <td>
                  //     <CFormGroup>
                  //       <CInputCheckbox
                  //         className="ml-2 cursor-pointer"
                  //         onClick={() => toggleSelects(item.id)}
                  //         checked={selects.includes(item.id)}
                  //         onChange={() => { }}
                  //       />
                  //     </CFormGroup>
                  //   </td>
                  // ),
                  'featured_image': (item) => (
                    <td>
                      <img src={item.featured_image} style={{ maxWidth: '100px', maxHeight: '150px' }} alt="" />
                    </td>
                  ),
                  'type': (item) => (
                    <td>
                      {dataBusinessTypes.map((obj) => {
                        if (item.type === obj.id) { return (obj.name) }
                      })}
                    </td>
                  ),
                  'rating': (item) => (
                    <td className="text-center">
                      {item.rating}
                    </td>
                  ),
                  'active': (item) => (
                    <td>
                      {item.active === "Open" ? t('open') : t('close')}
                    </td>
                  ),
                  'functions': (item, index) => {
                    return (
                      <>
                        <td className="py-2">
                          <CCollapse className="d-inline-flex">
                            <CSwitch onClick={() => handleChangeStatusClick(item)}
                              className={'mx-1 mt-1 c-switch-opposite-warning'} variant="outline" shape={'pill'} size={'sm'} color={'warning'}
                              checked={item.status} onChange={() => { }} />
                            <CTooltip content={t('listReview')} placement="top">
                              <CButton onClick={() => handleReviewClick(item)} variant="outline" size="sm" color="dark" className="ml-1">
                                <CIcon size={'sm'} name="cil-comment-square" />
                              </CButton>
                            </CTooltip>
                            <CTooltip content={t('listEmployee')} placement="top">
                              <Link to={{ pathname: `/business/employees`, state: item }}>
                                <CButton variant="outline" size="sm" color="warning" className="ml-1">
                                  <CIcon size={'sm'} name="cilPeople" />
                                </CButton>
                              </Link>
                            </CTooltip>
                            <CTooltip content={t('viewBusiness')} placement="top">
                              <CButton onClick={() => handleViewClick(item)} variant="outline" size="sm" color="success" className="ml-1">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" className="c-icon" role="img"><path fill="var(--ci-primary-color, currentColor)" d="M256,48a208.064,208.064,0,0,1,80.953,399.668A208.062,208.062,0,0,1,175.047,64.333,206.659,206.659,0,0,1,256,48m0-32C123.452,16,16,123.452,16,256S123.452,496,256,496,496,388.549,496,256,388.548,16,256,16Z" className="ci-primary"></path><rect width="35" height="35" x="257" y="103" fill="var(--ci-primary-color, currentColor)" className="ci-primary"></rect><path fill="var(--ci-primary-color, currentColor)" d="M269.709,408a50.06,50.06,0,0,1-48.026-64.186l35.67-121.277a17.577,17.577,0,0,0-33.1-11.7l-11.718,28.234-29.557-12.265L194.7,198.574a49.577,49.577,0,0,1,93.353,32.992l-35.67,121.278A18.06,18.06,0,0,0,269.709,376a18.123,18.123,0,0,0,15.6-8.966l15.1-25.915,27.646,16.115-15.1,25.913A50.225,50.225,0,0,1,269.709,408Z" className="ci-primary"></path></svg>
                              </CButton>
                            </CTooltip>
                            <CTooltip content={t('editBusiness')} placement="top">
                              <CButton onClick={() => handleEditClick(item)} variant="outline" size="sm" color="info" className="ml-1">
                                <CIcon size={'sm'} name="cilPencil" />
                              </CButton>
                            </CTooltip>
                            <CTooltip content={t('deleteBusiness')} placement="top">
                              <CButton onClick={() => toggle(item)} variant="outline" size="sm" color="danger" className="ml-1">
                                <CIcon size={'sm'} name="cilTrash" />
                              </CButton>
                            </CTooltip>
                          </CCollapse>
                        </td>
                      </>
                    )
                  },
                }}
              />
              <CPagination activePage={dataBusiness.current_page} onActivePageChange={handlePageChange} pages={dataBusiness.last_page} />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default BusinessList;
