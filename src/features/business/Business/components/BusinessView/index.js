import React from 'react';
import {
  CCard, CCardBody, CCardHeader, CCol, CRow, CLabel, CFormGroup,
} from '@coreui/react'
import './style.css'
import { useTranslate } from 'react-redux-multilingual'

const API_ENDPOINT = process.env.REACT_APP_API_URL_IMAGE;

function BusinessView(props) {
  const { location } = props;
  const t = useTranslate()
  const businessData = location.state?.business || JSON.parse(localStorage.getItem('dataViewBusiness'));
  let dataView = {};
  businessData.translations.map((obj, j) => {
    if (obj.language_id === localStorage.getItem('locale')) {
      dataView = Object.assign({ "name": obj.name, "address": obj.address, "description": obj.description }, businessData);
    }
  })
  return (
    <CRow>
      <CCol xl={12}>
        <CCard>
          <CCardHeader>
            <h4 className="d-inline-block">{t('businessView')}</h4>
          </CCardHeader>
          <CCardBody className="pl-5">
            <CFormGroup row>
              <CCol md="2">
                <CLabel htmlFor="featured_image" className="text-bold">{t('name')}</CLabel>
              </CCol>
              <CCol xs="12" md="10">
                {dataView.name}
              </CCol>
            </CFormGroup>
            <CFormGroup row>
              <CCol md="2">
                <CLabel htmlFor="featured_image" className="text-bold">{t('image')}</CLabel>
              </CCol>
              <CCol xs="12" md="10">
                <img src={dataView.featured_image} style={{ maxWidth: '150px', maxHeight: '200px' }} alt="" />
              </CCol>
            </CFormGroup>
            <CFormGroup row>
              <CCol md="2">
                <CLabel htmlFor="featured_image" className="text-bold">{t('phone')}</CLabel>
              </CCol>
              <CCol xs="12" md="10">
                {dataView.phone}
              </CCol>
            </CFormGroup>
            <CFormGroup row>
              <CCol md="2">
                <CLabel htmlFor="featured_image" className="text-bold">{t('address')}</CLabel>
              </CCol>
              <CCol xs="12" md="10">
                {dataView.address}
              </CCol>
            </CFormGroup>
            {/* <CFormGroup row>
              <CCol md="2">
                <CLabel htmlFor="featured_image" className="text-bold">Location</CLabel>
              </CCol>
              <CCol xs="12" md="10">
                {dataView.location}
              </CCol>
            </CFormGroup> */}

            <CRow>
              <CCol md="2"><CLabel htmlFor='open_hour' className="text-bold">{t('open')}</CLabel></CCol>
              <CCol md="4" lg="4" xl="2">
                <CFormGroup>
                  {dataView.open_hour}
                </CFormGroup>
              </CCol>
              <CCol style={{ maxWidth: "100px" }} className="mt-2" className="text-bold">{t('to')}</CCol>
              <CCol md="4" lg="4" xl="2">
                <CFormGroup>
                  {dataView.close_hour}
                </CFormGroup>
              </CCol>
            </CRow>
            <CFormGroup row>
              <CCol md="2">
                <CLabel htmlFor="featured_image" className="text-bold">{t('type')}</CLabel>
              </CCol>
              <CCol xs="12" md="10">
                {dataView.type}
              </CCol>
            </CFormGroup>
            <CFormGroup row>
              <CCol md="2">
                <CLabel htmlFor="featured_image" className="text-bold">{t('description')}</CLabel>
              </CCol>
              <CCol xs="12" md="10">
                {dataView.description}
              </CCol>
            </CFormGroup>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}
export default BusinessView;
