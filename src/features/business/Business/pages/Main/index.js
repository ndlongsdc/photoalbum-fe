import React, {useEffect,Suspense} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteBusinesses, deleteMultiBusinesses, fetchBusinesses, changeStatusBusinesses } from '../../businessSlice';
import BusinessList from '../../components/BusinessList';
import { useHistory } from 'react-router-dom';

MainPage.propTypes = {};

function MainPage(props) {
  const dispatch = useDispatch();
  const businessList = useSelector(state => state.businesses.businesses.data);
  // const businessData = useSelector(state => state.businesses.businesses);
  const history = useHistory();
  const checkLanguage = localStorage.getItem("localeAPI") !== localStorage.getItem("locale");
  // dispatch(fetchBusinesses());
  useEffect(() => {
    // checkLanguage && dispatch(fetchBusinesses());
    if(!businessList || checkLanguage){ dispatch(fetchBusinesses());}
  }, [dispatch])
  // if (checkLanguage) dispatch(fetchBusinesses());
  const onEditClick = (business) => {
    history.push({
      state: {business},
      pathname: `/business/businesses/${business.id}`
    });
  }

  const onViewClick = (business) => {
    history.push({
      state: {business},
      pathname: `/business/businesses/view`
    });
  }

  const onReviewClick = (business) => {
    history.push({
      state: {business},
      params: {id:business.id},
      pathname: `/business/reviews/${business.id}`
    });
  }

  const onRemoveClick = (business) => {
    const removeBusinessId = business.id;
    const action = deleteBusinesses(removeBusinessId);
    dispatch(action);
    dispatch(fetchBusinesses());
  }

  const onRemoveMultiClick = (ids) => {
    const action = deleteMultiBusinesses(ids);
    dispatch(action);
    dispatch(fetchBusinesses());
  }

  const onChangeStatusClick = (business) => {
    const action = changeStatusBusinesses(business.id);
    dispatch(action);
  }

  return (
    <>
    <Suspense
    fallback={<h1>Loading profile...</h1>}
  >
      <BusinessList
        businessList={businessList}
        onViewClick={onViewClick}
        onReviewClick={onReviewClick}
        onEditClick={onEditClick}
        onRemoveClick={onRemoveClick}
        onRemoveMultiClick={onRemoveMultiClick}
        onChangeStatusClick={onChangeStatusClick}
        />
      </Suspense>
    </>
  );
}

export default MainPage;
