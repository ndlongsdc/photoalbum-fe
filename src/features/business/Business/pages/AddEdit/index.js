import BusinessForm from '../../components/BusinessForm';
import { updateBusinesses, addBusinesses } from '../../businessSlice';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

AddEditPage.propTypes = {};

function AddEditPage(props) {
  const businesses = useSelector(state => state.businesses.businesses);
  const dispatch = useDispatch();
  const { id } = useParams();

  const handleSubmit = async (values) => {
    await id !== 'add' ? dispatch(updateBusinesses(id, values)) : dispatch(addBusinesses(values));
  }

  const initialValues = businesses;
  const formValues = JSON.parse(localStorage.getItem("formValues"));
  const saveValue = formValues !== undefined && formValues !== null ? formValues : {
    nameVi: '',
    addressVi: '',
    descriptionVi: '',
    nameEn: '',
    addressEn: '',
    descriptionEn: '',
    phone: '',
    open_hour: '',
    close_hour: '',
    type: [],
  }
  return (
    <>
      <BusinessForm
        initialValues={id === 'add' ? initialValues : saveValue}
        // initialValues={props.location.state !== undefined ? initialValues : saveValue}
        onSubmit={handleSubmit}
      />
    </>
  );
}

export default AddEditPage;
