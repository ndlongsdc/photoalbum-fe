import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteBusinessTypes, fetchBusinessTypes, changeStatusBusinessTypes } from '../../businessTypeSlice';
import BusinessTypeList from '../../components/BusinessTypeList';
import { useHistory } from 'react-router-dom';

MainPage.propTypes = {};

function MainPage(props) {
  const dispatch = useDispatch();
  const businessTypes = useSelector(state => state.businessTypes.businessTypes);
  const history = useHistory();

  useEffect(() => {
    (props.location.state === undefined || props.location.state === null) && businessTypes.length===0 && dispatch(fetchBusinessTypes());
  }, [dispatch])

  const onEditClick = (businessType) => {

    history.push({
      state: {businessType},
      pathname: `/business/businessTypes/${businessType.id}`
    });
  }

  const onRemoveClick = (businessType) => {
    const removeBusinessTypeId = businessType.id;
    const action = deleteBusinessTypes(removeBusinessTypeId);
    dispatch(action);
  }

  const onChangeStatusClick = (businessType) => {
    const action = changeStatusBusinessTypes(businessType.id);
    dispatch(action);
  }

  return (
    <>
      <BusinessTypeList
        businessTypeList={businessTypes}
        onEditClick={onEditClick}
        onRemoveClick={onRemoveClick}
        onChangeStatusClick={onChangeStatusClick}
      />
    </>
  );
}

export default MainPage;
