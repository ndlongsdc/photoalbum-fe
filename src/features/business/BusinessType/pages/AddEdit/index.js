import BusinessTypeForm from '../../components/BusinessTypeForm';
import { addBusinessTypes, updateBusinessTypes } from '../../businessTypeSlice';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

AddEditPage.propTypes = {};

function AddEditPage(props) {
  const dispatch = useDispatch();
  const { id } = useParams();
  const error = useSelector(state => state.businessTypes.error)
  const handleSubmit = async (values) => {
    await id !== 'add' ? dispatch(updateBusinessTypes(values.id,values)) : dispatch(addBusinessTypes(values));

    !error && props.history.push('/business/businessTypes');
  }


  return (
    <BusinessTypeForm
      initialValues={props.location.state !== undefined ? props.location.state.businessType : {
        name: '',
        key: '',
        method: '',
        path: '',
      }}
      onSubmit={handleSubmit}
    />
  );
}

export default AddEditPage;
