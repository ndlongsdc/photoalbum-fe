import { createSlice } from '@reduxjs/toolkit';
import businessTypeApi from '../../../api/businessTypeApi'
import { setToast, clearToast } from '../../../custom_toast/toastSlice'

const businessType = createSlice({
  name: 'businessTypes',
  initialState: {
    businessTypes: [],
    businessType: '',
    error: '',
    loading: false,
  },
  reducers: {
    fetchBusinessTypesStart: (state, action) => {
      state.loading = true
    },
    fetchBusinessTypesSuccess: (state, action) => {
      state.loading = false
      state.businessTypes = action.payload
    },
    fetchBusinessTypesFailure: (state, action) => {
      state.error = action.payload
      // localStorage.clear();
    },
    fetchBusinessTypeByIdStart: (state, action) => {
      state.loading = true
    },
    fetchBusinessTypeByIdSuccess: (state, action) => {
      state.loading = false
      state.businessType = action.payload
    },
    fetchBusinessTypeByIdFailure: (state, action) => {
      state.error = action.payload
      // localStorage.clear();
    },
    addBusinessTypeStart: (state, action) => {
      state.loading = true
    },
    addBusinessTypeSuccess: (state, action) => {
      state.loading = false;
      state.businessTypes.unshift(action.payload)
    },
    addBusinessTypeFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    updateBusinessTypeStart: (state, action) => {
      state.loading = true
    },
    updateBusinessTypeSuccess: (state, action) => {
      state.error = false;
      state.loading = false;
      const foundIndex = state.businessTypes.findIndex(({ id }) => id === action.payload.id)
      state.businessTypes.splice(foundIndex, 1, action.payload)
    },
    updateBusinessTypeFailure: (state, action) => {
      state.error = action.payload
    },
    deleteBusinessTypeStart: (state, action) => {
      state.loading = true
    },
    deleteBusinessTypeSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.businessTypes.findIndex(({ id }) => id === action.payload)
      state.businessTypes.splice(foundIndex, 1)
    },
    deleteBusinessTypeFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
    changeStatusBusinessTypeStart: (state, action) => {
      state.loading = true
    },
    changeStatusBusinessTypeSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.businessTypes.findIndex(({ id }) => id === action.payload.id)
      state.businessTypes.splice(foundIndex, 1, action.payload)
    },
    changeStatusBusinessTypeFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
  }
});

const { reducer, actions } = businessType;
export const {
  fetchBusinessTypesStart, fetchBusinessTypesSuccess, fetchBusinessTypesFailure,
  fetchBusinessTypeByIdStart, fetchBusinessTypeByIdSuccess, fetchBusinessTypeByIdFailure,
  addBusinessTypeStart, addBusinessTypeSuccess, addBusinessTypeFailure,
  updateBusinessTypeStart, updateBusinessTypeSuccess, updateBusinessTypeFailure,
  deleteBusinessTypeStart, deleteBusinessTypeSuccess, deleteBusinessTypeFailure,
  changeStatusBusinessTypeStart, changeStatusBusinessTypeSuccess, changeStatusBusinessTypeFailure,
} = actions;
export const fetchBusinessTypes = () => async dispatch => {
  try {
    dispatch(fetchBusinessTypesStart());
    const response = await businessTypeApi.getAll();
    if (response.status = 'success') {
      localStorage.setItem("businessTypesOption", JSON.stringify(response.data.businessType))
      dispatch(fetchBusinessTypesSuccess(response.data.businessType))
    } else {
      dispatch(fetchBusinessTypesFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(fetchBusinessTypesFailure("error"));
    const valueToast = {
      message: 'Access token has expired',
      type: 'error',
      status: 401
    }
    dispatch(setToast(valueToast));
  }
  // dispatch(clearToast());
}
export const fetchBusinessTypeById = (id) => async dispatch => {
  try {
    dispatch(fetchBusinessTypeByIdStart());
    const response = await businessTypeApi.get(id);
    if (response.status = 'success') {
      dispatch(fetchBusinessTypeByIdSuccess(response.data.businessTypeById))
    } else {
      dispatch(fetchBusinessTypeByIdFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(fetchBusinessTypeByIdFailure(error));
  }
}
export const addBusinessTypes = (body) => async dispatch => {
  try {
    dispatch(addBusinessTypeStart());
    const response = await businessTypeApi.create(body);
    let valueToast = {}
    if (response.success === true) {
      dispatch(addBusinessTypeSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(addBusinessTypeFailure(response.error));
      const messages = response.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(addBusinessTypeFailure(error));
  }
  dispatch(clearToast());
}
export const updateBusinessTypes = (id, body) => async dispatch => {
  try {
    dispatch(updateBusinessTypeStart());
    const response = await businessTypeApi.update(id, body);
    let valueToast = {}
    if (response.success === true) {
      dispatch(updateBusinessTypeSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(updateBusinessTypeFailure(response.message));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(updateBusinessTypeFailure(error));
  }
  dispatch(clearToast());
}
export const deleteBusinessTypes = (id) => async dispatch => {
  try {
    dispatch(deleteBusinessTypeStart());
    const response = await businessTypeApi.delete(id);
    let valueToast = {}
    if (response.success) {
      dispatch(deleteBusinessTypeSuccess(id));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deleteBusinessTypeFailure(id));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deleteBusinessTypeFailure(error));
  }
  dispatch(clearToast());
}
export const changeStatusBusinessTypes = (id) => async dispatch => {
  try {
    dispatch(changeStatusBusinessTypeStart());
    const response = await businessTypeApi.changeStatusBusinessType(id);
    let valueToast = {}
    if (response.success) {
      dispatch(changeStatusBusinessTypeSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changeStatusBusinessTypeFailure(response.message));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeStatusBusinessTypeFailure('error'));
  }
  dispatch(clearToast());
}

export default reducer;
