import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CIcon from '@coreui/icons-react';
import '../BusinessTypeList/style.css'
import { useSelector } from 'react-redux';
import { useTranslate } from 'react-redux-multilingual'

import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CCollapse,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CSwitch,
  CTooltip
  // CPagination
} from '@coreui/react'
import { Link } from 'react-router-dom';

BusinessTypeList.propTypes = {
  businessTypeList: PropTypes.array,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
};

BusinessTypeList.defaultProps = {
  businessTypeList: [],
  onEditClick: null,
  onRemoveClick: null,
};

function BusinessTypeList(props) {
  const { businessTypeList, onEditClick, onRemoveClick, onChangeStatusClick } = props;
  const t = useTranslate()
  const handleEditClick = (item) => {
    if (onEditClick) onEditClick(item);
    localStorage.setItem("formValues", JSON.stringify(item));
  }

  const [itemSelected, setItemSelected] = useState();
  const handleRemoveClick = () => {
    if (onRemoveClick) onRemoveClick(itemSelected);
    setModal(!modal);
  }

  const [modal, setModal] = useState(false);
  const toggle = (item) => {
    setItemSelected(item)
    setModal(!modal);
  }

  const handleChangeStatusClick = (item) => {
    if (onChangeStatusClick) onChangeStatusClick(item);
  }

  const fields = [
    {
      key: 'No',
      label: t('no'),
      _classes: 'text-center',
      _style: { width: '5%' },
      sorter: false,
    },
    { key: 'name',
    label: t('name'), },
    {
      key: 'description',
      label: t('description'),
      sorter: false,
    },
    {
      key: 'functions',
      label: t('actions'),
      _style: { width: '1%', textAlign: 'center' },
      sorter: false,
      filter: false
    }
  ]

  const loading = useSelector(state => state.businessTypes.loading)
  return (
    <>
      <CModal
        show={modal}
        onClose={toggle}
      >
        <CModalHeader closeButton>{t('deleteBusinessType')}</CModalHeader>
        <CModalBody>
        {t('confirmDeleteBusinessType')} "{itemSelected?.name}"?
  </CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveClick()} color="primary">{t('agree')}</CButton>{' '}
          <CButton
            color="secondary"
            onClick={toggle}
          >{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CRow>
        <CCol xl={12}>
          <CCard>
            <CCardHeader>
              <h4 className="d-inline-block">{t('listBusinessType')}</h4>
            </CCardHeader>
            <CCardBody>
              <Link to={{ pathname: `/business/businessTypes/add` }}>
                <CTooltip content={t('addBusinessType')} placement="right">
                  <CButton color="success" className="mb-2">
                  {t('add')} <i className="fas fa-plus"></i>
                  </CButton>
                </CTooltip>
              </Link>
              <CDataTable
                items={businessTypeList}
                fields={fields}
                hover
                pagination
                loading={loading}
                scopedSlots={{
                  'No': (item, index) => (
                    <td className="text-center">
                      {index + 1}
                    </td>
                  ),
                  'functions':
                    (item, index) => {
                      return (
                        <td className="py-2">
                          <CCollapse className="d-inline-flex">
                          <CSwitch onClick={() => handleChangeStatusClick(item)}
                              className={'mx-1 mt-1 c-switch-opposite-warning'} shape={'pill'} size={'sm'} color={'warning'}
                              checked={item.status} onChange={() => { }} />
                              <CTooltip content={t('editBusinessType')} placement="top">
                            <CButton onClick={() => handleEditClick(item)} variant="outline" size="sm" color="info" className="ml-1">
                              <CIcon size={'sm'} name="cilPencil" />
                            </CButton>
                            </CTooltip>
                            <CTooltip content={t('deleteBusinessType')} placement="top">
                            <CButton onClick={() => toggle(item)} variant="outline" size="sm" color="danger" className="ml-1">
                              <CIcon size={'sm'} name="cilTrash" />
                            </CButton>
                            </CTooltip>
                          </CCollapse>
                        </td>
                      )
                    },
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default BusinessTypeList;
