import React from 'react'
import InputField from '../../../../../custom_fields/inputField';
import TextareaField from '../../../../../custom_fields/textareaField';
import { Formik, FastField, Form } from 'formik'
import * as Yup from 'yup'
import { CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CFormGroup, CSpinner } from '@coreui/react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import CheckBoxField from '../../../../../custom_fields/checkBoxField';
// import { CKEditor } from '@ckeditor/ckeditor5-react';
import { useTranslate } from 'react-redux-multilingual'

BusinessTypeForm.propTypes = {
  onSubmit: PropTypes.func,
};

BusinessTypeForm.defaultProps = {
  onSubmit: null,
}

function BusinessTypeForm(props) {
  const t = useTranslate()
  const { initialValues } = props;
  const formValues = localStorage.getItem("formValues");
  const valueForm = initialValues || formValues;
  const { id } = useParams();

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .required(t('businessTypeName_required')),
      description: Yup.string()
        .required(t('description_required')),
  })
  return (
    <Formik
      enableReinitialize
      initialValues={
        {
          id:valueForm.id,
          name:valueForm.name || '',
          description:valueForm.description || '',
          status:valueForm.status || '',
        }
      }
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
    >
      {formikProps => {
        const { isSubmitting } = formikProps;

        return (
          <>
            <CRow>
              <CCol xs="12">
                <CCard>
                  <CCardHeader>
                  <h4>{id ==='add' ? t('addBusinessType') : t('editBusinessType')}</h4>
              </CCardHeader>
                  <CCardBody>
                    <Form className="form-horizontal">
                      <CFormGroup>
                        <FastField
                          name="name"
                          component={InputField}
                          label={t('name')}
                          placeholder={t('name')}
                          requiredlabel
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="description"
                          component={TextareaField}

                          label={t('description')}
                          placeholder={t('description')}
                          requiredlabel
                        />
                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="status"
                          component={CheckBoxField}
                          label={t('status')}
                          requiredlabel
                        />
                      </CFormGroup>
                      <div className="form-actions float-right">
                        <CButton type="submit" color={id ==='add' ? 'primary' : 'success'}>{isSubmitting && <CSpinner size="sm" />}
                          {id ==='add' ? t('add') : t('edit')}</CButton>
                        <Link to={{ pathname: `/business/businessTypes` }}>
                          <CButton color="secondary ml-1">{t('cancel')}</CButton>
                        </Link>
                      </div>
                    </Form>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </>
        )
      }}
    </Formik>

  )
}

export default BusinessTypeForm
