import { createSlice } from '@reduxjs/toolkit';
import employeeApi from '../../../api/employeeApi'
import { setToast, clearToast } from '../../../custom_toast/toastSlice'

const employee = createSlice({
  name: 'employees',
  initialState: {
    employees: [],
    employee: '',
    error: '',
    loading: false,
  },
  reducers: {
    fetchEmployeesStart: (state, action) => {
      state.loading = true
    },
    fetchEmployeesSuccess: (state, action) => {
      state.loading = false
      state.employees = action.payload
    },
    fetchEmployeesFailure: (state, action) => {
      state.error = action.payload
      // localStorage.clear();
    },
    addEmployeeStart: (state, action) => {
      state.loading = true
    },
    addEmployeeSuccess: (state, action) => {
      state.loading = false;
      state.employees.unshift(action.payload)
    },
    addEmployeeFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    updateEmployeeStart: (state, action) => {
      state.loading = true
    },
    updateEmployeeSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.employees.findIndex(({ id }) => id === action.payload)
      state.employees.splice(foundIndex, 1, action.payload)
    },
    updateEmployeeFailure: (state, action) => {
      state.error = action.payload
      state.loading = false;
    },
    deleteEmployeeStart: (state, action) => {
      state.loading = true
    },
    deleteEmployeeSuccess: (state, action) => {
      state.loading = false;
      const foundIndex = state.employees.findIndex(({ id }) => id === action.payload)
      state.employees.splice(foundIndex, 1)
    },
    deleteEmployeeFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload
    },
  }
});

const { reducer, actions } = employee;
export const {
  fetchEmployeesStart, fetchEmployeesSuccess, fetchEmployeesFailure,
  addEmployeeStart, addEmployeeSuccess, addEmployeeFailure,
  updateEmployeeStart, updateEmployeeSuccess, updateEmployeeFailure,
  deleteEmployeeStart, deleteEmployeeSuccess, deleteEmployeeFailure,
} = actions;
export const fetchEmployees = (id) => async dispatch => {
  try {
    dispatch(fetchEmployeesStart());
    const response = await employeeApi.getAll(id);
    if (response.status = 'success') {
      dispatch(fetchEmployeesSuccess(response.data.employees.data))
    } else {
      dispatch(fetchEmployeesFailure(response.message));
      const valueToast = {
        message: response.message,
        type: 'error',
        status: response.status
      }
      dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(fetchEmployeesFailure("error"));
  }
  // dispatch(clearToast());
}
export const addEmployees = (body) => async dispatch => {
  try {
    dispatch(addEmployeeStart());
    const response = await employeeApi.create(body);
    let valueToast = {}
    if (response.success === true) {
      dispatch(addEmployeeSuccess(response.data.employess));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(addEmployeeFailure(response.error));
      const messages = response.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(addEmployeeFailure(error));
  }
  dispatch(clearToast());
}
export const updateEmployees = (body) => async dispatch => {
  try {
    dispatch(updateEmployeeStart());
    const response = await employeeApi.update(body.idEmployee,{role_id:body.role});
    let valueToast = {}
    if (response.success === true) {
      dispatch(updateEmployeeSuccess(response.data));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(updateEmployeeFailure(response.error));
      const messages = response.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(updateEmployeeFailure(error));
  }
  dispatch(clearToast());
}
export const deleteEmployees = (id) => async dispatch => {
  try {
    dispatch(deleteEmployeeStart());
    const response = await employeeApi.delete(id);
    let valueToast = {}
    if (response.success) {
      dispatch(deleteEmployeeSuccess(id));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(deleteEmployeeFailure(id));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(deleteEmployeeFailure('error'));
  }
  dispatch(clearToast());
}

export default reducer;
