import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteEmployees, fetchEmployees } from '../../employeeSlice';
import EmployeeList from '../../components/EmployeeList';
import { useHistory } from 'react-router-dom';

MainPage.propTypes = {};

function MainPage(props) {
  let business = '';
  if (localStorage.getItem("business")) {
    business = JSON.parse(localStorage.getItem("business"))
  } else {
    business = props.location.state;
    localStorage.setItem("business", JSON.stringify(business));
  }
  const dispatch = useDispatch();
  const employees = useSelector(state => state.employees.employees);
  const history = useHistory();
  useEffect(() => {
    dispatch(fetchEmployees(business.id));
  }, [dispatch])

  const onEditClick = (business) => {
    history.push({
      state: {business},
      pathname: `/business/employees/${business.id}`
    });
  }

  const onRemoveClick = (employee) => {
    const removeEmployeeId = employee.idBusiness;
    const action = deleteEmployees(removeEmployeeId);
    dispatch(action);
  }


  return (
    <>
      <EmployeeList
        employeeList={employees}
        business={business}
        onEditClick={onEditClick}
        onRemoveClick={onRemoveClick}
      />
    </>
  );
}

export default MainPage;
