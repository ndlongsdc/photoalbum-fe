import EmployeeForm from '../../components/EmployeeForm';
import { addEmployees, updateEmployees } from '../../employeeSlice';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

AddEditPage.propTypes = {};

function AddEditPage(props) {
  const dispatch = useDispatch();
  const error = useSelector(state => state.employees.error)
  const { id } = useParams();
  const handleSubmit = async (values) => {
    await id !== 'add' ? dispatch(updateEmployees(values)) : dispatch(addEmployees(values));

    !error && props.history.push('/business/employees');
  }


  return (
    <EmployeeForm
      initialValues={props.location.state !== undefined ? props.location.state.employee : {
        fullname: '',
        role: [],
      }}
      onSubmit={handleSubmit}
    />
  );
}

export default AddEditPage;
