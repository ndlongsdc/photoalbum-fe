import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CIcon from '@coreui/icons-react';
import '../EmployeeList/style.css'
import { useSelector } from 'react-redux';
import { useTranslate } from 'react-redux-multilingual'

import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CCollapse,
  CModal,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CTooltip
  // CPagination
} from '@coreui/react'
import { Link } from 'react-router-dom';

EmployeeList.propTypes = {
  employeeList: PropTypes.array,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
};

EmployeeList.defaultProps = {
  employeeList: [],
  onEditClick: null,
  onRemoveClick: null,
};

function EmployeeList(props) {
  const { employeeList, onEditClick, onRemoveClick, business } = props;
  let dataEmployee = [];
  employeeList.map((obj) => {
    dataEmployee.push(Object.assign({ idBusiness: obj.id, role: obj.role_id }, obj.user))
  })
  const [modal, setModal] = useState(false);
  const [itemSelected, setItemSelected] = useState();
  const t = useTranslate()

  const handleRemoveClick = () => {
    if (onRemoveClick) onRemoveClick(itemSelected);
    setModal(!modal);
  }

  const handleEditClick = (item) => {
    if (onEditClick) onEditClick(item);
    localStorage.setItem("formValues", JSON.stringify(item));
  }

  const toggle = (item) => {
    setItemSelected(item)
    setModal(!modal);
  }

  const fields = [
    // {
    //   key: 'checkBox',
    //   label: '',
    //   _style: { width: '3%' },
    //   sorter: false,
    //   filter: false
    // },
    {
      key: 'No',
      label: t('no'),
      _classes: 'text-center',
      _style: { width: '5%' },
      sorter: false,
    },
    {
      key: 'fullname',
      label: t('name'),
      _classes: 'position-relative sort'
    },
    {
      key: 'role',
      label: t('role'),
    },
    {
      key: 'phone',
      label: t('phone'),
      sorter: false,
      filter: false
    },
    {
      key: 'email',
      label: t('email'),
      sorter: false,
      filter: false
    },
    {
      key: 'functions',
      label: t('actions'),
      _classes: "text-center",
      _style: { width: '9%' },
      sorter: false,
      filter: false
    }
  ]

  const loading = useSelector(state => state.employees.loading)
  return (
    <>
      <CModal
        show={modal}
        onClose={toggle}
      >
        <CModalHeader closeButton>{t('deleteEmployee')}</CModalHeader>
        <CModalBody>
          {t('confirmDeleteEmployee')} "{itemSelected?.fullname}"?
  </CModalBody>
        <CModalFooter>
          <CButton onClick={() => handleRemoveClick()} color="primary">{t('agree')}</CButton>{' '}
          <CButton
            color="secondary"
            onClick={toggle}
          >{t('cancel')}</CButton>
        </CModalFooter>
      </CModal>
      <CRow>
        <CCol xl={12}>
          <CCard>
            <CCardHeader>
              <h4 className="d-inline-block">{t('listEmployee') + business.name}</h4>
            </CCardHeader>
            <CCardBody>
              <Link to={{ pathname: `/business/employees/add` }}>
                <CTooltip content={t('addEmployee')} placement="left">
                  <CButton color="success" className="mb-2">
                  {t('add')} <i className="fas fa-plus"></i>
                  </CButton>
                </CTooltip>
              </Link>
              <CDataTable
                items={dataEmployee}
                fields={fields}
                hover
                pagination
                loading={loading}
                scopedSlots={{
                  'No': (item, index) => (
                    <td className="text-center">
                      {index + 1}
                    </td>
                  ),
                  'role': (item) => (
                    <td>
                      {/* {item.role.map((obj, i) => ( */}
                      <CBadge className="mr-2" color="info">
                        {item.role === 71 ? 'Admin Business' : item.role === 70 ? 'Management Business' : 'User'}
                      </CBadge>
                      {/* ))} */}
                    </td>
                  ),
                  'functions':
                    (item, index) => {
                      return (
                        <td className="py-2 text-center">
                          <CCollapse className="d-inline-flex">
                            <CTooltip content={t('editEmployee')} placement="top">
                              <CButton onClick={() => handleEditClick(item)} variant="outline" size="sm" color="info" className="ml-1">
                                <CIcon size={'sm'} name="cilPencil" />
                              </CButton>
                            </CTooltip>
                            <CTooltip content={t('deleteEmployee')} placement="top">
                              <CButton onClick={() => toggle(item)} variant="outline" size="sm" color="danger" className="ml-1">
                                <CIcon size={'sm'} name="cilTrash" />
                              </CButton>
                            </CTooltip>
                          </CCollapse>
                        </td>
                      )
                    },
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default EmployeeList;
