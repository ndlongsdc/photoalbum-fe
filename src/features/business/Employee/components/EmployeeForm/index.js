import React from 'react'
import SelectField from '../../../../../custom_fields/selectField';
import { Formik, FastField, Form } from 'formik'
import * as Yup from 'yup'
import { CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CFormGroup, CSpinner } from '@coreui/react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { useTranslate } from 'react-redux-multilingual'
import { useParams } from 'react-router-dom';

EmployeeForm.propTypes = {
  onSubmit: PropTypes.func,
};

EmployeeForm.defaultProps = {
  onSubmit: null,
}

function EmployeeForm(props) {
  const t = useTranslate()
  const { id } = useParams();
  const { initialValues } = props;
  const formValues = JSON.parse(localStorage.getItem("formValues"));
  const valueForm = formValues || initialValues;

  const validationSchema = Yup.object().shape({
    // employee: Yup.string()
    //   .required(t('employee_required')),
    role: Yup.string()
      .required(t('role_required')),
  })

  const roleOption = [
    { value: 71, label: 'Admin Business' },
    { value: 70, label: 'Management Business' },
    { value: 72, label: 'User Business' }
  ];


  return (
    <Formik
      enableReinitialize
      initialValues={
        {
          id: JSON.parse(localStorage.getItem('business')).id,
          idEmployee:valueForm.idBusiness || '',
          employee: valueForm.employee || '',
          role: valueForm.role || '',
        }
      }
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
    >
      {formikProps => {
        const { isSubmitting } = formikProps;
        return (
          <>
            <CRow>
              <CCol xs="12">
                <CCard>
                  <CCardHeader>
                    <h4>{id === 'add' ? t('add_employee_to_business') : t('edit_employee_in_business')}</h4>
                  </CCardHeader>
                  <CCardBody>
                    <Form className="form-horizontal">
                      <CFormGroup>
                        {id === 'add' ?   <FastField
                          name="employee"
                          component={SelectField}
                          options={[]}
                          label={t('employeeName')}
                          placeholder={t('select_employee')}
                          requiredlabel
                          searchApi
                        /> : valueForm.employee}

                      </CFormGroup>
                      <CFormGroup>
                        <FastField
                          name="role"
                          component={SelectField}
                          options={roleOption}
                          label={t('role')}
                          placeholder={t('select_role')}
                          requiredlabel
                        />
                      </CFormGroup>
                      <div className="form-actions float-right">
                        <CButton type="submit" color='primary'>{isSubmitting && <CSpinner size="sm" />}
                      {id === 'add' ? t('add') : t('edit')}</CButton>
                        <Link to={{ pathname: `/business/employees` }}>
                          <CButton color="secondary ml-1">{t('cancel')}</CButton>
                        </Link>
                      </div>
                    </Form>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </>
        )
      }}
    </Formik>

  )
}

export default EmployeeForm
