// import Business from 'features/business/Business';
import React from 'react';

const Toaster = React.lazy(() =>
  import('./views/notifications/toaster/Toaster'));
const Tables = React.lazy(() =>
  import('./views/base/tables/Tables'));
const Breadcrumbs = React.lazy(() =>
  import('./views/base/breadcrumbs/Breadcrumbs'));
const Cards = React.lazy(() =>
  import('./views/base/cards/Cards'));
const Carousels = React.lazy(() =>
  import('./views/base/carousels/Carousels'));
const Collapses = React.lazy(() =>
  import('./views/base/collapses/Collapses'));
const BasicForms = React.lazy(() =>
  import('./views/base/forms/BasicForms'));
const Jumbotrons = React.lazy(() =>
  import('./views/base/jumbotrons/Jumbotrons'));
const ListGroups = React.lazy(() =>
  import('./views/base/list-groups/ListGroups'));
const Navbars = React.lazy(() =>
  import('./views/base/navbars/Navbars'));
const Navs = React.lazy(() =>
  import('./views/base/navs/Navs'));
const Paginations = React.lazy(() =>
  import('./views/base/paginations/Pagnations'));
const Popovers = React.lazy(() =>
  import('./views/base/popovers/Popovers'));
const ProgressBar = React.lazy(() =>
  import('./views/base/progress-bar/ProgressBar'));
const Switches = React.lazy(() =>
  import('./views/base/switches/Switches'));
const Tabs = React.lazy(() =>
  import('./views/base/tabs/Tabs'));
const Tooltips = React.lazy(() =>
  import('./views/base/tooltips/Tooltips'));
const BrandButtons = React.lazy(() =>
  import('./views/buttons/brand-buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() =>
  import('./views/buttons/button-dropdowns/ButtonDropdowns'));
const ButtonGroups = React.lazy(() =>
  import('./views/buttons/button-groups/ButtonGroups'));
const Buttons = React.lazy(() =>
  import('./views/buttons/buttons/Buttons'));
const Charts = React.lazy(() =>
  import('./views/charts/Charts'));
const Dashboard = React.lazy(() =>
  import('./views/dashboard/Dashboard'));
const CoreUIIcons = React.lazy(() =>
  import('./views/icons/coreui-icons/CoreUIIcons'));
const Flags = React.lazy(() =>
  import('./views/icons/flags/Flags'));
const Brands = React.lazy(() =>
  import('./views/icons/brands/Brands'));
const Alerts = React.lazy(() =>
  import('./views/notifications/alerts/Alerts'));
const Badges = React.lazy(() =>
  import('./views/notifications/badges/Badges'));
const Modals = React.lazy(() =>
  import('./views/notifications/modals/Modals'));
const Typography = React.lazy(() =>
  import('./views/theme/typography/Typography'));
const Widgets = React.lazy(() =>
  import('./views/widgets/Widgets'));

const Page401 = React.lazy(() => import('./views/pages/page401/Page401'))
const Profile = React.lazy(() => import('./views/pages/profile/Profile'))
const BusinessInfomation = React.lazy(() => import('./views/pages/businessInfomation/BusinessInfomation'))

const Users = React.lazy(() => import('./features/management/User/index'));
const UserAddEdit = React.lazy(() => import('./features/management/User/pages/AddEdit/index'));

const Roles = React.lazy(() => import('./features/management/Role/index'));
const RoleAddEdit = React.lazy(() => import('./features/management/Role/pages/AddEdit/index'));

const Permissions = React.lazy(() => import('./features/management/Permission/index'));
const PermissionAddEdit = React.lazy(() => import('./features/management/Permission/pages/AddEdit/index'));

const Businesses = React.lazy(() => import('./features/business/Business/index'));
const BusinessAddEdit = React.lazy(() => import('./features/business/Business/pages/AddEdit/index'));
const BusinessView = React.lazy(() => import('./features/business/Business/components/BusinessView/index'));

const BusinessTypes = React.lazy(() => import('./features/business/BusinessType/index'));
const BusinessTypeAddEdit = React.lazy(() => import('./features/business/BusinessType/pages/AddEdit/index'));

const AlbumTypes = React.lazy(() => import('./features/album/AlbumType/index'));
const AlbumTypeAddEdit = React.lazy(() => import('./features/album/AlbumType/pages/AddEdit/index'));

const Employees = React.lazy(() => import('./features/business/Employee/index'));
const EmployeeAddEdit = React.lazy(() => import('./features/business/Employee/pages/AddEdit/index'));

const Reviews = React.lazy(() => import('./features/business/Review/index'));
const ReviewDetail = React.lazy(() => import('./features/business/Review/components/ReviewDetail/index'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', name: 'Base', component: Cards, exact: true },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/forms', name: 'Forms', component: BasicForms },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/brands', name: 'Brands', component: Brands },
  { path: '/notifications', name: 'Notifications', component: Alerts, exact: true },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/notifications/toaster', name: 'Toaster', component: Toaster },
  { path: '/widgets', name: 'Widgets', component: Widgets },

  { path: '/page401', name: 'Page401', component: Page401 },
  { path: '/profile', name: 'Profile', component: Profile },
  { path: '/businessInfomation', name: 'Business Infomation', component: BusinessInfomation },
  // user
  { path: '/management/users', exact: true, name: 'Users', component: Users },
  { path: '/management/users/:id', name: 'User Edit', component: UserAddEdit },
  { path: '/management/users/add', name: 'User Add', component: UserAddEdit },
  // role
  { path: '/management/roles', exact: true, name: 'Roles', component: Roles },
  { path: '/management/roles/:id', exact: true, name: 'Edit Role', component: RoleAddEdit },
  { path: '/management/roles/add', exact: true, name: 'Add Role', component: RoleAddEdit },
  // permission
  { path: '/management/permissions', exact: true, name: 'Permissions', component: Permissions },
  { path: '/management/permissions/:id', name: 'Permission Edit', component: PermissionAddEdit },
  { path: '/management/permissions/add', name: 'Permission Add', component: PermissionAddEdit },
  // business
  { path: '/business/businesses', exact: true, name: 'Businesses', component: Businesses },
  { path: '/business/businesses/view', exact: true, name: 'BusinessesView', component: BusinessView },
  { path: '/business/businesses/:id', name: 'Business Edit', component: BusinessAddEdit },
  { path: '/business/businesses/add', name: 'Business Add', component: BusinessAddEdit },
  // businessTypes
  { path: '/business/businessTypes', exact: true, name: 'BusinessTypes', component: BusinessTypes },
  { path: '/business/businessTypes/:id', name: 'BusinessType Edit', component: BusinessTypeAddEdit },
  { path: '/business/businessTypes/add', name: 'BusinessType Add', component: BusinessTypeAddEdit },
  // albumTypes
  { path: '/album/albumTypes', exact: true, name: 'AlbumTypes', component: AlbumTypes },
  { path: '/album/albumTypes/:id', name: 'AlbumType Edit', component: AlbumTypeAddEdit },
  { path: '/album/albumTypes/add', name: 'AlbumType Add', component: AlbumTypeAddEdit },
  // employees
  { path: '/business/employees', exact: true, name: 'Employees', component: Employees },
  { path: '/business/employees/:id', name: 'Employee Edit', component: EmployeeAddEdit },
  { path: '/business/employees/add', name: 'Employee Add', component: EmployeeAddEdit },
  // employees
  { path: '/business/reviews', exact: true, name: 'Reviews', component: Reviews },
  { path: '/business/reviews/:id', exact: true, name: 'Reviews', component: Reviews },
  { path: '/business/reviews/detail', exact: true, name: 'ReviewDetail', component: ReviewDetail },

]
export default routes;
