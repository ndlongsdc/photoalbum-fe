import { createSlice } from '@reduxjs/toolkit';
// import translationApi from '../api/translationApi'
const translations= createSlice({
  name: 'Intl',
  initialState: {
    locale:'vi'
  },
  reducers: {
    changeLanguageReducer: (state, action) => {
      const curentLanguage = localStorage.getItem('locale')==="en" ? "vi":"en";

      state.loading = true
      state.locale = curentLanguage;
      localStorage.setItem('locale',curentLanguage);
      // state.locale = action.payload === "us"?"vn":"us"
      // localStorage.setItem('locale',action.payload === "us"?"vn":"us");
    },
  }
});

const { reducer, actions } = translations
export const {
  changeLanguageReducer
} = actions;
export const changeLanguage = (param) => async dispatch => {
  // const response = await translationApi.changeLanguage(param);
    dispatch(changeLanguageReducer(param));
}

export default reducer;
