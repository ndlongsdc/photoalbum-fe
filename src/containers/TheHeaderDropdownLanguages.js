import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { changeLanguage } from './../translations/translationSlice'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import ReactCountryFlag from "react-country-flag"

const TheHeaderDropdownLanguages = () => {
  const dispatch = useDispatch();
  const locale = useSelector(state => state.Intl.locale);
  const handleLanguageChange = () => {
    dispatch(changeLanguage(locale));
  }
  return (
    <>
      <CDropdown
        inNav
        className="c-header-nav-item"
      >
        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <ReactCountryFlag
            countryCode={locale === "vi" ? "vn" : "us"}
            svg
            style={{
              width: '2em',
              height: '2em',
              borderRadius: '50%'
            }}
            cdnUrl="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.4.3/flags/1x1/"
            cdnSuffix="svg"
            title={locale}
          />
        </CDropdownToggle>
        <CDropdownMenu placement="bottom-end" className="py-0">
          <CDropdownItem className="d-block" onClick={()=>handleLanguageChange()}>
            <ReactCountryFlag
              countryCode={locale === "vi" ? "us" : "vn"}
              svg
              style={{
                width: '2em',
                height: '2em',
                borderRadius: '50%'
              }}
              cdnUrl="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.4.3/flags/1x1/"
              cdnSuffix="svg"
              title={locale}
            />
            <h6 className="d-inline-flex ml-2">{locale === "vi" ? "English" : "Việt Nam"}</h6>
          </CDropdownItem>
        </CDropdownMenu>
      </CDropdown>
      {/* <div style={{ textTransform: "uppercase" }}>
        <ReactCountryFlag
          countryCode={locale}
          svg
          style={{
            width: '2em',
            height: '2em',
            borderRadius: '50%'
          }}
          cdnUrl="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.4.3/flags/1x1/"
          cdnSuffix="svg"
          title={locale}
        />
        {locale}
      </div> */}
    </>
  )
}

export default TheHeaderDropdownLanguages
