export default [
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Permission Management']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'User',
    to: '/management/users',
    icon: 'cil-user',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Role',
    to: '/management/roles',
    icon: 'cil-lock-locked',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Permission',
    to: '/management/permissions',
    icon: 'cil-settings',
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Business Management']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Business Type',
    to: '/business/businessTypes',
    icon: 'cil-list',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Store',
    to: '/business/businesses',
    icon: 'cil-star',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Album',
    to: '/features/album',
    icon: 'cil-layers',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Photo',
    to: '/features/photo',
    icon: 'cil-puzzle',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Reported Component',
    to: '/features/reportedComponent',
    icon: 'cil-warning',
  },
]
