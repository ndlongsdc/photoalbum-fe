import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { logout } from '../views/pages/authSlice';
import { useDispatch, useSelector } from 'react-redux';
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
  CTooltip
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useTranslate } from 'react-redux-multilingual'

const TheHeaderDropdown = () => {
  const t = useTranslate()
  const auths = useSelector(state => state.auths.auths)
  const history = useHistory();
  const dispatch = useDispatch();
  const data = useSelector(state => state.auths.auths.user);
  const currentBusiness = localStorage.getItem('currentBusiness');
  const business = useSelector(state => state.auths.auths.business) || currentBusiness && JSON.parse(currentBusiness);
  const dataUser = data || JSON.parse(localStorage.getItem('currentUser'))
  useEffect(() => {
    if (auths?.message) {
      localStorage.clear();
      history.push({ pathname: '/login' });
    }
  }, [auths?.message])
  const onLogout = () => {
    dispatch(logout());
    // if(auths.message==="Successfully logged out"){history.push({pathname: '/login' });}
  }
  !dataUser && history.push({ pathname: '/login' });
  const onViewProfileClick = () => {
    history.push({ pathname: '/profile' });
  }

  const onViewBusinessInfomationClick = () => {
    history.push({ pathname: '/businessInfomation' });
  }

  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      {dataUser && <CTooltip content={dataUser.fullname} placement="top">
        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <div className="c-avatar">
            <CImg
              src={dataUser.avatar || ''}
              className="c-avatar-img"
              alt={dataUser.fullname}
            />
          </div>
        </CDropdownToggle>
      </CTooltip>}
      <CDropdownMenu className="pt-0" placement="bottom-end">
        {/* <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>Account</strong>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-bell" className="mfe-2" />
          Updates
          <CBadge color="info" className="mfs-auto">42</CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-envelope-open" className="mfe-2" />
          Messages
          <CBadge color="success" className="mfs-auto">42</CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-task" className="mfe-2" />
          Tasks
          <CBadge color="danger" className="mfs-auto">42</CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-comment-square" className="mfe-2" />
          Comments
          <CBadge color="warning" className="mfs-auto">42</CBadge>
        </CDropdownItem> */}
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>{t('setting')}</strong>
        </CDropdownItem>
        <CDropdownItem onClick={() => onViewProfileClick()}>
          <CIcon name="cil-user" className="mfe-2" />{t('profile')}
        </CDropdownItem>
        {business && <CDropdownItem onClick={() => onViewBusinessInfomationClick()}>
          <CIcon name="cil-star" className="mfe-2" />{t('businessInfomation')}
        </CDropdownItem>}
        {/* <CDropdownItem>
          <CIcon name="cil-settings" className="mfe-2" />
          Settings
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-credit-card" className="mfe-2" />
          Payments
          <CBadge color="secondary" className="mfs-auto">42</CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-file" className="mfe-2" />
          Projects
          <CBadge color="primary" className="mfs-auto">42</CBadge>
        </CDropdownItem> */}
        <CDropdownItem divider />
        <CDropdownItem onClick={onLogout}>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          {t('logout')}
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdown
