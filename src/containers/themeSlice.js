import { createSlice } from '@reduxjs/toolkit';

const theme = createSlice({
  name: 'themes',
  initialState: {
    sidebarShow: 'responsive'
  },
  reducers: {
    show: (state, action) => {
      // state.loading = true
    },
    hide: (state, action) => {
      // state.loading = false
    },
  }
});

const { reducer, actions } = theme;
export const {
  show, hide
} = actions;
export const showSidebar = (params) => async dispatch => {
  dispatch(show(params));
}
export default reducer;
