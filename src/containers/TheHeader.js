import React from 'react'
// import { useSelector, useDispatch } from 'react-redux'
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  // CSubheader,
  // CBreadcrumbRouter
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useTranslate } from 'react-redux-multilingual'

// routes config
// import routes from '../routes'

import {
  TheHeaderDropdown,
  TheHeaderDropdownMssg,
  TheHeaderDropdownNotif,
  TheHeaderDropdownTasks,
  TheHeaderDropdownLanguages
} from './index'

const TheHeader = () => {
  const t = useTranslate()

  const toggleSidebar = () => {
    var sidebar = document.getElementsByClassName("c-sidebar")[0];
    if (sidebar.classList.contains("c-sidebar-lg-show")) {
      sidebar.classList.remove("c-sidebar-lg-show");
    } else {
      sidebar.classList.add("c-sidebar-lg-show");
    }
  }

  const toggleSidebarMobile = () => {
    var sidebar = document.getElementsByClassName("c-sidebar")[0];
    if (sidebar.classList.contains("c-sidebar-show")) {
      sidebar.classList.add("c-sidebar-lg-show");
      sidebar.classList.remove("c-sidebar-show");
    } else {
      sidebar.classList.add("c-sidebar-show");
      sidebar.classList.remove("c-sidebar-lg-show");
    }
  }

  setTimeout(() => {
    if (document.getElementsByClassName("c-wrapper")[0]) {
      document.getElementsByClassName("c-wrapper")[0].addEventListener("click", function () {
        var sidebar = document.getElementsByClassName("c-sidebar")[0];
        if (sidebar.classList.contains("c-sidebar-show")) {
          sidebar.classList.add("c-sidebar-lg-show");
          sidebar.classList.remove("c-sidebar-show");
        }
      });
    }
    // document.getElementsByClassName("li.c-sidebar-nav-item")[0].addEventListener("click", function () {
    //   var sidebar = document.getElementsByClassName("c-sidebar")[0];
    //   if (sidebar.classList.contains("c-sidebar-show")) {
    //     sidebar.classList.add("c-sidebar-lg-show");
    //     sidebar.classList.remove("c-sidebar-show");
    //   }
    // });

  }, 123);

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
        <CIcon name="logo" height="48" alt="Logo" />
      </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3" >
          <CHeaderNavLink to="/dashboard">{t('dashboard')}</CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          {/* <CHeaderNavLink to="/users">Users</CHeaderNavLink> */}
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          {/* <CHeaderNavLink>Settings</CHeaderNavLink> */}
        </CHeaderNavItem>
      </CHeaderNav>

      <CHeaderNav className="px-3">
        <TheHeaderDropdownNotif />
        <TheHeaderDropdownTasks />
        <TheHeaderDropdownMssg />
        <TheHeaderDropdownLanguages />
        <TheHeaderDropdown />
      </CHeaderNav>

      {/* <CSubheader className="px-3 justify-content-between">
        <CBreadcrumbRouter
          className="border-0 c-subheader-nav m-0 px-0 px-md-3"
          routes={routes}
        /> */}
      {/* <div className="d-md-down-none mfe-2 c-subheader-nav">
            <CLink className="c-subheader-nav-link"href="#">
              <CIcon name="cil-speech" alt="Settings" />
            </CLink>
            <CLink
              className="c-subheader-nav-link"
              aria-current="page"
              to="/dashboard"
            >
              <CIcon name="cil-graph" alt="Dashboard" />&nbsp;Dashboard
            </CLink>
            <CLink className="c-subheader-nav-link" href="#">
              <CIcon name="cil-settings" alt="Settings" />&nbsp;Settings
            </CLink>
          </div>
      </CSubheader>*/}
    </CHeader>
  )
}

export default TheHeader
