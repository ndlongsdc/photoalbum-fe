import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'
import { useTranslate } from 'react-redux-multilingual'

// sidebar nav config
import navigation from './_nav'

const TheSidebar = () => {
  const t = useTranslate()
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)
  localStorage.removeItem("currentPage")
  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({ type: 'set', sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        {/* <CIcon
          className="c-sidebar-brand-full"
          name="logo-negative"
          height={35}
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          name="sygnet"
          height={35}
        /> */}
      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={[{
            _tag: 'CSidebarNavTitle',
            _children: [t('permissionManagementSidebar')]
          }, {
            _tag: 'CSidebarNavItem',
            name: t('userSidebar'),
            to: '/management/users',
            icon: 'cil-user',
          },
          {
            _tag: 'CSidebarNavItem',
            name: t('roleSidebar'),
            to: '/management/roles',
            icon: 'cil-lock-locked',
          },
          {
            _tag: 'CSidebarNavItem',
            name: t('permissionSidebar'),
            to: '/management/permissions',
            icon: 'cil-settings',
          },
          {
            _tag: 'CSidebarNavTitle',
            _children: [t('businessManagementSidebar')]
          },
          {
            _tag: 'CSidebarNavItem',
            name: t('businessTypeSidebar'),
            to: '/business/businessTypes',
            icon: 'cil-list',
          },
          {
            _tag: 'CSidebarNavItem',
            name: t('businessSidebar'),
            to: '/business/businesses',
            icon: 'cil-star',
          },
          {
            _tag: 'CSidebarNavItem',
            name: t('reviewSidebar'),
            to: '/business/reviews',
            icon: 'cil-comment-square',
          },
          {
            _tag: 'CSidebarNavItem',
            name: t('albumSidebar'),
            to: '/album/albumTypes',
            icon: 'cil-layers',
          },
          {
            _tag: 'CSidebarNavItem',
            name: t('photoSidebar'),
            to: '/features/photo',
            icon: 'cil-puzzle',
          },
          {
            _tag: 'CSidebarNavItem',
            name: t('reportedComponentSidebar'),
            to: '/features/reportedComponent',
            icon: 'cil-warning',
          },
          ]}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none" />
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
