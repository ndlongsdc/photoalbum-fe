import { configureStore } from "@reduxjs/toolkit";
import authReducer from '../views/pages/authSlice';
import userReducer from '../features/management/User/userSlice';
import roleReducer from '../features/management/Role/roleSlice';
import permissionReducer from '../features/management/Permission/permissionSlice';
import businessReducer from '../features/business/Business/businessSlice';
import businessTypeReducer from '../features/business/BusinessType/businessTypeSlice';
import albumTypeReducer from '../features/album/AlbumType/albumTypeSlice';
import employeeReducer from '../features/business/Employee/employeeSlice';
import reviewReducer from '../features/business/Review/reviewSlice';
import toastReducer from '../custom_toast/toastSlice';
import translationReducer from '../translations/translationSlice';

const rootReducer = {
  auths: authReducer,
  users: userReducer,
  roles: roleReducer,
  permissions: permissionReducer,
  businesses: businessReducer,
  businessTypes: businessTypeReducer,
  albumTypes: albumTypeReducer,
  reviews: reviewReducer,
  employees: employeeReducer,
  toasts: toastReducer,
  Intl: translationReducer,
}

const store = configureStore({
  reducer: rootReducer,
});

export default store;
