import axiosClient from './axiosClient';
const API_ENDPOINT = process.env.REACT_APP_API_URL;
const url = "users";
const userApi = {
  get: (params) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}?page=${params}`);
  },
  getById: (id) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/${id}`);
  },
  create: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/${url}`, params);
  },
  update: (id, params) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/${id}`, params);
  },
  delete: (id) => {
    return axiosClient.delete(`${API_ENDPOINT}/${url}/${id}`);
  },
  deleteMulti: (ids) => {
    return axiosClient.delete(`${API_ENDPOINT}/${url}/${ids}`);
  },
  changeStatusUser: (id) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/${id}/changeStatus`);
  },
  resetPasswordUser: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/${url}/${params.id}/reset-password`, params);
  },
  filter: (params) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/filter-users?keyword=${params.keyword?params.keyword:''}&sortBy=${params.sortBy?params.sortBy:''}&field=${params.field?params.field:''}&page=${params.page?params.page:''}`, params);
  },

}

export default userApi;
