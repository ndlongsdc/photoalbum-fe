import axiosClient from './axiosClient';
const API_ENDPOINT = process.env.REACT_APP_API_URL;
const url = "businesses";
const businessApi = {
  get: (param) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/list?page=${param}`);
  },
  getById: (id) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/${id}`);
  },
  create: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/${url}`, params);
  },
  update: (id, params) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/${id}`, params);
  },
  delete: (id) => {
    return axiosClient.delete(`${API_ENDPOINT}/${url}/${id}`);
  },
  deleteMulti: (ids) => {
    return axiosClient.delete(`${API_ENDPOINT}/${url}/${ids}`);
  },
  changeStatusBusiness: (id) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/${id}/changeStatus`);
  },
  filter: (params) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/filter-businesses-by-user?keyword=${params.keyword}&field=${params.field}&sortBy=${params.sortBy}&page=${params.page}`, params);
  },
  listAddUserEmployee: () => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/list-add-user-employees`);
    // return axiosClient.get(`${API_ENDPOINT}/${url}/list-add-user-employees?perPage=${params}`);
  },

}

export default businessApi;
