import axiosClient from './axiosClient';
const API_ENDPOINT = process.env.REACT_APP_API_URL;
const url = "permissions";
const permissionApi = {
  getAll: () => {
    return axiosClient.get(`${API_ENDPOINT}/${url}`);
  },
  get: (id) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/${id}`);
  },
  create: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/${url}`, params);
  },
  update: (id, params) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/${id}`, params);
  },
  delete: (id) => {
    return axiosClient.delete(`${API_ENDPOINT}/${url}/${id}`);
  },
  changeStatusPermission: (id) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/${id}/changeStatus`);
  },
}

export default permissionApi;
