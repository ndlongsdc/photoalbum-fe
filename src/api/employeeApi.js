import axiosClient from './axiosClient';
const API_ENDPOINT = process.env.REACT_APP_API_URL;
const url = "businesses";
const roleApi = {
  getAll: (id) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/${id}/list-employees`);
  },
  get: (id) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/${id}`);
  },
  create: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/${url}/${params.id}/add-employees`, params);
  },
  update: (id,params) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/update-employees/${id}`, params);
  },
  delete: (id) => {
    return axiosClient.delete(`${API_ENDPOINT}/${url}/delete-employees/${id}`);
  },
}

export default roleApi;
