import axios from "axios";
// import { useHistory } from "react-router-dom";

class AxiosService {
  constructor() {
    localStorage.setItem("locale",'vi');
    const instance = axios.create({
      headers: {
        "Content-Type": "application/json; charset=utf-8; ",
        // 'X-localization': localStorage.getItem("locale")
      },
    });

    instance.interceptors.request.use(function (config) {
      if (localStorage.getItem("auth:access_token")) {
        const currentLanguage = localStorage.getItem("locale");
        currentLanguage || localStorage.setItem("locale",'vi');
        config.headers['Authorization'] = "Bearer " + localStorage.getItem("auth:access_token");
        config.headers['lang'] = currentLanguage || "vi";
      }
      return config;
    });

    instance.interceptors.response.use(this.handleSuccess, this.handleError);
    this.instance = instance;
  }

  handleSuccess(response) {
    const { data } = response;
    localStorage.setItem("localeAPI",data.data?.language||localStorage.getItem("locale"));
    if (data.status === 401) {
      localStorage.clear();
    }
    return data;
  }

  handleError(error) {
    console.log(error);
    // const history = useHistory();
      // history.push("/login");
      return error;
  }
  // handleError(error) {
  //   const errorResponse = error.response;

  //   if (!errorResponse) {
  //     localStorage.clear();
  //     history.push("/auth/login", {
  //       notification: { warning: true, message: "ERROR" },
  //     });
  //     return null;
  //   }

  //   if (tokenService.isTokenExpiredError(errorResponse)) {
  //     return tokenService.resetTokenAndReattemptRequest(error);
  //   }

  //   const { data } = errorResponse;
  //   return Promise.reject(data);
  // }

  get(url) {
    return this.instance.get(url);
  }

  post(url, data) {
    return this.instance.post(url, data);
  }

  put(url, data) {
    return this.instance.put(url, data);
  }

  patch(url, data) {
    return this.instance.patch(url, data);
  }

  delete(url) {
    return this.instance.delete(url);
  }
}

export default new AxiosService();
