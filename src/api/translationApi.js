import axiosClient from './axiosClient';
const API_ENDPOINT = process.env.REACT_APP_API_URL;
const url = "languages";
const translationApi = {
  changeLanguage: (value) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/change-language/${value}`);
  },
}

export default translationApi;
