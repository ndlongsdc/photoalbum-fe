import axiosClient from './axiosClient';
const API_ENDPOINT = process.env.REACT_APP_API_URL;
const url = "ratings";
const reviewApi = {
  getAll: (param) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/list?page=${param}`);
  },
  get: (id) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/detail/${id}`);
  },
  getByBusinessId: (id) => {
    return axiosClient.get(`${API_ENDPOINT}/${url}/list-by-business/${id}`);
  },
  changeStatus: (id) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/comment/${id}/changeStatus`);
  },
  changeStatusReply: (id) => {
    return axiosClient.put(`${API_ENDPOINT}/${url}/reply/${id}/changeStatus`);
  },
}

export default reviewApi;
