import axiosClient from './axiosClient';
const API_ENDPOINT = process.env.REACT_APP_API_URL;
// const url = "login";
const authApi = {
  login: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/login`, params);
  },
  logout: () => {
    return axiosClient.get(`${API_ENDPOINT}/logout`);
  },
  getOTP: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/otp/generate`, params);
  },
  submitOTP: (params) => {
    return axiosClient.put(`${API_ENDPOINT}/auth/forgot-password`, params);
  },
  checkOTP: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/otp/confirm`, params);
  },
  changePassword: (params) => {
    return axiosClient.post(`${API_ENDPOINT}/users/change-password`, params);
  },
}

export default authApi;
