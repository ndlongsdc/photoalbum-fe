import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { PrivateRoute } from "../src/PrivateRoute";
import 'react-toastify/dist/ReactToastify.css';
import './scss/style.scss';
import { withTranslate } from 'react-redux-multilingual'
// import 'semantic-ui-css/semantic.min.css'
import Toast from '../src/custom_toast/index'
const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'));
const Register = React.lazy(() => import('./views/pages/register/Register'));
const ForgotPassword = React.lazy(() => import('./views/pages/forgot-password/ForgotPassword'));
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));
const Share = React.lazy(() => import('./views/pages/share/Share'));

function App() {
const toasts = useSelector(state => state.toasts);
    return (
      <>
      <Toast
        message={toasts.toastMessage}
        type={toasts.toastType}
        time={toasts.toastTime}
      />
      <HashRouter>
          <React.Suspense fallback={loading}>
            <Switch>
              <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <Route exact path="/forgotpassword" name="ForgotPassword Page" render={props => <ForgotPassword {...props}/>} />
              <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
              <Route exact path="/share" name="Page 500" render={props => <Share {...props}/>} />
              <PrivateRoute path="/" name="Home" role="role" component={TheLayout} />
              {/* <Route path="/" name="Home" render={props => <TheLayout {...props}/>} /> */}
            </Switch>
          </React.Suspense>
      </HashRouter></>
    );
  }

export default withTranslate(App);
