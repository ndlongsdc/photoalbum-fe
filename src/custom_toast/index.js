import React from 'react';
import { ToastContainer, toast, Slide } from 'react-toastify';

function Toast(props) {
  const time = props.time;
  let mess = [];
  mess = typeof (props.message) === "string" ? props.message.split() : props.message
  props.message && toast[props.type](
    <div>
      {
          mess.map((obj, index) => {
            return (
              <div key={index}>
                {obj} <br />
              </div>
            )
          })
      }
    </div>, {
    position: "top-right",
    autoClose: parseInt(time) || 3000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    transition: Slide
  });
  return (
    <ToastContainer />
  )
}

export default Toast;
