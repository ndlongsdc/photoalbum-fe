import { createSlice } from '@reduxjs/toolkit';
// import { history } from '../../helpers/history';
const toast = createSlice({
  name: 'toasts',
  initialState: {
    toastMessage: '',
    toastType: ''
  },
  reducers: {
    setToastReducer: (state,action) => {
      state.toastMessage = action.payload.message
      state.toastType = action.payload.type
      state.toastTime = action.payload.time
    },
    clearToastReducer: (state) => {
      state.toastMessage = ''
      state.toastType = ''
      state.toastTime = ''
    }
  }
});

const { reducer, actions } = toast;
export const {
  setToastReducer, clearToastReducer
} = actions;
export const setToast = (params) => async dispatch => {
  params.status === 401 && localStorage.clear();
  dispatch(setToastReducer(params));
}
export const clearToast = () => async dispatch => {
    dispatch(clearToastReducer());
}
export default reducer;
