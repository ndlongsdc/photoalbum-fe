import React from 'react';
import PropTypes from 'prop-types';

Pagination.propTypes = {
  pagination: PropTypes.object.isRequired,
  onPageChange: PropTypes.func,
};

Pagination.defaultProps = {
  onPageChange: null,
}

function Pagination(props) {
  const { pagination, onPageChange } = props;
  const { page, limit, totalRows } = pagination;
  const totalPages = Math.ceil(totalRows / limit);

  function handlePageChange(newPage) {
    if (onPageChange) {
      onPageChange(newPage);
    }
  }

  return (
    <>
      {/* <nav aria-label="pagination" style={{ display: "inline" }}>
        <ul className="pagination justify-content-start">
          <li className={"page-item " + (page <= 1 ? "disabled" : "")}><a className={"page-link " + (page <= 1 ? "disabled" : "")} aria-label="Go to first page" aria-disabled="true" onClick={() => handlePageChange(page - 1)}>Prev</a></li>
          <li className={"page-item " + (page >= totalPages ? "disabled" : "")}><a className={"page-link " + (page >= totalPages ? "disabled" : "")} aria-label="Go to previous page" aria-disabled="true" onClick={() => handlePageChange(page + 1)}>Next</a></li>
        </ul>
      </nav> */}
    </>
  );
}

export default Pagination;
