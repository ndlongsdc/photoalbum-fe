import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form'
import { changeAvatar, changeProfile, changePassword } from '../authSlice'
import '../profile/style.css'
import {
  CButton,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useTranslate } from 'react-redux-multilingual'

const API_ENDPOINT = process.env.REACT_APP_API_URL_IMAGE;
const ListGroups = () => {
  const t = useTranslate()
  const dispatch = useDispatch();
  const [preview, setPreview] = useState()
  const [avatar, setAvatar] = useState()
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState()
  const [currentPassword, setcurrentPassword] = useState('')
  const [errorCurrentPassword, setErrorCurrentPassword] = useState()
  const [errorMessageCurrentPassword, setErrorMessageCurrentPassword] = useState()
  const [newPassword, setnewPassword] = useState('')
  const [errorNewPassword, setErrorNewPassword] = useState()
  const [errorMessageNewPassword, setErrorMessageNewPassword] = useState()
  const [confirmPassword, setconfirmPassword] = useState('')
  const [errorConfirmPassword, setErrorConfirmPassword] = useState()
  const [errorMessageConfirmPassword, setErrorMessageConfirmPassword] = useState()
  const data = useSelector(state => state.auths.auths.user);
  const dataUser = data || JSON.parse(localStorage.getItem('currentUser'))
  const { register, errors, handleSubmit, reset } = useForm({
    defaultValues: {
      fullname: dataUser.fullname,
      phone: dataUser.phone,
      email: dataUser.email,
      address: dataUser.address || ""
    }
  });
  useEffect(() => {
    if (!avatar) {
      setPreview(undefined)
      return
    }

    const objectUrl = URL.createObjectURL(avatar)
    setPreview(objectUrl)

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl)
  }, [avatar])
  const onChangeAvatar = (value) => {
    dispatch(changeAvatar(value))
    setAvatar(value);
  }
  const onChangeCurrentPassword = (value) => {
    if (value) {
      setErrorCurrentPassword(false)
    }
    setcurrentPassword(value);
  }
  const onChangeNewPassword = (value) => {
    if (confirmPassword && value !== newPassword) {
      setErrorNewPassword(true)
    } if (confirmPassword === value) {
      setErrorConfirmPassword(false)
    }
    if (value) {
      setErrorNewPassword(false)
    }
    setnewPassword(value);
  }
  const onChangeConfirmPassword = (value) => {
    if (newPassword !== value) {
      setErrorConfirmPassword(true)
      setErrorMessageConfirmPassword(t('passwords_do_not_match'))
    } else if (newPassword === value) {
      setErrorConfirmPassword(false)
    }
    setconfirmPassword(value);
  }
  const onSubmitProfile = data => {
    delete data.avatar
    delete data.currentPassword
    delete data.newPassword
    delete data.confirmPassword
    dispatch(changeProfile(data));
  }
  const onSubmitChangePassWord = data => {
    if (!currentPassword) {
      setErrorCurrentPassword(true)
      setErrorMessageCurrentPassword(t('field_required'))
    }
    if (!newPassword) {
      setErrorNewPassword(true)
      setErrorMessageNewPassword(t('field_required'))
    }
    if (!confirmPassword) {
      setErrorConfirmPassword(true)
      setErrorMessageConfirmPassword(t('field_required'))
    }
    const value = { "oldPassword": currentPassword, "newPassword": newPassword, "confirmPassword": confirmPassword }
      !errorCurrentPassword && !errorNewPassword && !errorConfirmPassword && dispatch(changePassword(value));
  }
  return (
    <>
      {/* <div className="">
        {t('overview')}
      </div> */}
      <h2 className="mb-3">
        {t('userProfile')}
      </h2>
      <div className="row">
        <div className="col-lg-4">
          <div className="card card-small pt-3">
            <div className="card-header border-bottom text-center">
              <div className="mb-3 mx-auto">
                <img className="rounded-circle" src={preview || dataUser.avatar} alt="User Avatar" width="110" />
                {/* {avatar &&  <img src={preview} /> } */}
              </div>
              <h4 className="mb-0">
                {dataUser.fullname}
              </h4>
              <span className="text-muted d-block mb-2">
                {dataUser.email}
              </span>
              <div className="upload-btn-wrapper">
                <button type="button" className="mb-2 btn btn-sm btn-pill btn-outline-primary mr-2 cursor-pointer">
                  <i className="fas fa-camera"></i> {t('change')}</button>
                <input type="file" name="myfile" onChange={(value) => onChangeAvatar(value.currentTarget.files[0])} />
              </div>
            </div>
            <ul className="list-group list-group-flush">
              <p className="text-center py-3">
                <CButton className="btn-facebook btn-brand mx-1"><CIcon name="cib-facebook" /></CButton>
                <CButton className="btn-twitter btn-brand mx-1"><CIcon name="cib-twitter" /></CButton>
                <CButton className="btn-instagram btn-brand mx-1"><CIcon name="cib-instagram" /></CButton>
              </p>
            </ul>
          </div>
        </div>
        <div className="col-lg-8">
          <div className="card card-small mb-4">
            <div className="card-header border-bottom">
              <h6 className="m-0">{t('accountId')}: {dataUser.id} </h6>
            </div>
            <ul className="list-group list-group-flush">
              <li className="list-group-item p-3">
                <div className="row">
                  <div className="col">
                    <form onSubmit={handleSubmit(onSubmitProfile)}>
                      <div className="row">
                        <div className="form-group mb-3 col-6">
                          <label>{t('email')}</label>
                          <input
                            type="text"
                            className={!!errors.email ? 'form-control is-invalid' : 'form-control'}
                            name="email"
                            placeholder={t('email')}
                            ref={register()} />
                        </div>
                        <div className="form-group mb-3 col-6">
                          <label>{t('fullname')}</label>
                          <input
                            type="text"
                            className={!!errors.fullname ? 'form-control is-invalid' : 'form-control'}
                            name="fullname"
                            placeholder={t('fullname')}
                            ref={register()} />
                        </div>
                        <div className="form-group mb-3 col-12">
                          <label>{t('phone')}</label>
                          <input
                            type="text"
                            className={!!errors.phone ? 'form-control is-invalid' : 'form-control'}
                            name="phone"
                            placeholder={t('phone')}
                            ref={register()} />
                        </div>
                        <div className="form-group mb-3 col-12">
                          <label>{t('address')}</label>
                          <input
                            type="text"
                            className={!!errors.address ? 'form-control is-invalid' : 'form-control'}
                            name="address"
                            placeholder={t('address')}
                            ref={register()} />
                        </div>
                        <div className="col">
                          <button className="btn btn-primary" type="submit">{t('save')}</button>
                          {/* <span id="validationPhoneEmail" class="invalid-feedback">Please provide a valid city.</span> */}
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </li>
            </ul>
            <ul className="list-group list-group-flush">
              <li className="list-group-item p-3">
                <div className="row">
                  <div className="col">
                    <form onSubmit={handleSubmit(onSubmitChangePassWord)}>
                      <div className="row">
                        <div className="form-group mb-3 col-12">
                          <label>{t('currentPassword')}</label>
                          <input
                            type="password"
                            className={errorCurrentPassword ? 'form-control is-invalid' : 'form-control'}
                            name="current_password"
                            placeholder={t('currentPassword')}
                            onChange={(e) => onChangeCurrentPassword(e.target.value)}
                            ref={register()} />
                          {errorCurrentPassword && <div className="text-danger" style={{ fontSize: '80%' }} >{errorMessageCurrentPassword}</div>}
                        </div>
                        <div className="form-group mb-3 col-12">
                          <label>{t('newPassword')}</label>
                          <input
                            type="password"
                            className={errorNewPassword ? 'form-control is-invalid' : 'form-control'}
                            name="new_password"
                            placeholder={t('newPassword')}
                            onChange={(e) => onChangeNewPassword(e.target.value)}
                            ref={register()} />
                          {errorNewPassword && <div className="text-danger" style={{ fontSize: '80%' }} >{errorMessageNewPassword}</div>}
                        </div>
                        <div className="form-group mb-3 col-12">
                          <label>{t('confirmPassword')}</label>
                          <input
                            type="password"
                            className={errorConfirmPassword ? 'form-control is-invalid' : 'form-control'}
                            name="confirm_password"
                            placeholder={t('confirmPassword')}
                            onChange={(e) => onChangeConfirmPassword(e.target.value)}
                            ref={register()} />
                          {errorConfirmPassword && <div className="text-danger" style={{ fontSize: '80%' }} >{errorMessageConfirmPassword}</div>}
                        </div>
                        <div className="col">
                          <button className="btn btn-primary" type="submit">{t('changePassword')}</button>
                          {/* <span id="validationPhoneEmail" class="invalid-feedback">Please provide a valid city.</span> */}
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}

export default ListGroups
