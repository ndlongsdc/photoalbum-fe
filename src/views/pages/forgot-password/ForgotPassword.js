import React, { useState, useRef, useEffect } from 'react'
// import { CountDown } from './CountDown'
import { Formik, Field, Form } from 'formik'
import { useForm } from 'react-hook-form'
import '../forgot-password/style.css'
import { getOTP, submitOTP, checkOTP, resetAuth } from '../authSlice'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CRow,
  CSpinner
} from '@coreui/react'
import { useTranslate } from 'react-redux-multilingual'
import CountDown from './CountDown'
// import { Label } from 'semantic-ui-react'

const ForgotPassword = (props) => {

  const t = useTranslate()
  const dispatch = useDispatch();
  const [fogotPassword, setForgotPassword] = useState(true);
  const [viaSMS, setViaSMS] = useState(false);
  const [viaEmail, setViaEmail] = useState(false);
  const [error, setError] = useState(false);
  const [newPassword, setNewPassword] = useState(false);
  const [time, setTime] = useState(0);
  const [forgotPasswordParams, setforgotPasswordParams] = useState({});
  const [email, setEmail] = useState();
  const [otp, setotp] = useState();

  const checkGetOTP = useSelector(state => state?.auths.getOTP);
  const checkGetOTPWeb = useSelector(state => state?.auths.getOTPWeb);
  const confirmOTP = useSelector(state => state?.auths.checkOTP);
  const checkSubmitOTP = useSelector(state => state?.auths.submitOTP);
  const loading = useSelector(state => state?.auths.loading);

  const newPasswordR = useRef({});
  const code1Ref = useRef();

  const { register, errors, handleSubmit, reset, watch } = useForm();
  newPasswordR.current = watch("newPassword", "");

  const params = props.location.search.replace("?check=", "").split(".");
  useEffect(() => {
    if (params[0]) {
      setEmail(atob(params[0]));
      setotp(atob(params[1]));
      if (params[0]) {
        setForgotPassword(false);
        setViaSMS(false);
        setViaEmail(false);
        setNewPassword(true)
      }
    }
  }, [])

  useEffect(() => {
    dispatch(resetAuth());
  }, [])

  useEffect(() => {
    checkSubmitOTP && props.history.push({ pathname: '/login' });

  }, [checkSubmitOTP])
  useEffect(() => {
    if (checkGetOTP) {
      setForgotPassword(false);
      setViaSMS(true);
      setViaEmail(false);
    }
  }, [checkGetOTP])
  // useEffect(() => {
  //   if (checkGetOTPWeb) {
  //     setTimeout(() => {
  //       window.location.replace("https://mail.google.com/mail/u/0/#inbox");
  //       // window.open("https://mail.google.com/mail/u/0/#inbox", "_blank");

  //     }, 5000);
  //   }
  // }, [checkGetOTPWeb])
  useEffect(() => {
    confirmOTP && setViaSMS(false);
    confirmOTP && setNewPassword(true);

  }, [confirmOTP])

  const onSubmitPhone = data => {
    var regExPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;
    var regExEmail = /\S+@\S+\.\S+/;
    var phone_email = data.phone_email;
    var matchPhone = regExPhone.test(phone_email);
    var matchEmail = regExEmail.test(phone_email);
    matchPhone && viasms(phone_email);
    matchEmail && viaemail(phone_email);
    if (!matchPhone && !matchEmail) { setError(true); } else { setError(false); }
  }
  const onSubmitOTP = (data) => {
    const OTP = data.code1 + data.code2 + data.code3 + data.code4 + data.code5 + data.code6;
    setforgotPasswordParams((forgotPasswordParams) => ({
      ...forgotPasswordParams,
      ["otp"]: OTP
    }));
    dispatch(checkOTP(forgotPasswordParams));
    forgotPasswordParams.otp = OTP;
  }
  // const onSubmitEmail = (data) => {
  //   console.log(data);
  // }
  const onSubmitNewPassword = (data) => {
    if (params) {
      forgotPasswordParams.phone = email;
      forgotPasswordParams.otp = otp;
      forgotPasswordParams.web = true;
    }
    forgotPasswordParams.password = data.newPassword;
    forgotPasswordParams.confirmPassword = data.confirm_password;
    dispatch(submitOTP(forgotPasswordParams));
  }
  const viaemail = (phone_email) => {
    const data = { "phone": phone_email,"web": true }
    setforgotPasswordParams((forgotPasswordParams) => ({
      ...forgotPasswordParams,
      ["phone"]: phone_email
    }));
    dispatch(getOTP(data));


    // setForgotPassword(false);
    // setViaSMS(true);
    // setViaEmail(false);
  }
  const viasms = (phone_email) => {
    const data = { "phone": phone_email }
    dispatch(getOTP(data));
    setforgotPasswordParams((forgotPasswordParams) => ({
      ...forgotPasswordParams,
      ["phone"]: phone_email
    }));

  }

  const handleCancelClick = () => {
    setForgotPassword(true);
    setViaSMS(false);
    setViaEmail(false);
    setNewPassword(false);
  }
  const handleResendClick = (value) => {
    setTime(value)
  }
  const fogotpassword = () => {
    setForgotPassword(true);
    setViaSMS(false);
    setViaEmail(false);
  }

  const handleInputChange = (e) => {
    if (e.charCode >= 48 && e.charCode <= 57 && e.currentTarget.nextElementSibling != null) {
      // i++;
      e.currentTarget.nextElementSibling.focus();
    } else if (e.charCode < 48 || e.charCode > 57 || e.currentTarget.nextElementSibling != null) {
      e.currentTarget.blur()
      reset()
      // i = 0;
    }
    // else {
    //   i++;
    //   if (i > 4) {
    //     e.currentTarget.blur()
    //     reset()
    //   }
    // }
  }

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="7">
            <CCardGroup>
              <CCard className="p-4">
                <div className={fogotPassword ? "show" : "hide"}>
                  {fogotPassword && <CCardBody>
                    <h1 className="text-center">{t('forgot_your_password')}</h1>
                    <h4 className="">{t('subtitle_fogotPassword')}</h4>
                    <div className="justify-content-center">
                      <form onSubmit={handleSubmit(onSubmitPhone)}>
                        <div className="input-group mb-3">
                          <input
                            type="text"
                            className={!!errors.phone_email || error ? 'form-control is-invalid' : 'form-control'}
                            name="phone_email"
                            placeholder="Phone/Email"
                            aria-describedby="validationPhoneEmail"
                            autoComplete="off"
                            ref={register({ required: true })} />
                          <div className="input-group-append">
                            <CButton className={loading === true ? "btn btn-primary disabled" : "btn btn-primary"} type="submit">{loading && <CSpinner size="sm" />} {t('continue')}</CButton>
                          </div>
                          {/* <span id="validationPhoneEmail" class="invalid-feedback">Please provide a valid city.</span> */}
                        </div>
                      </form>
                      <div className="form-actions float-right">
                        <Link to="/login">
                          <CButton color="link" className="px-0">{t('back_to_login')}</CButton>
                        </Link>
                      </div>
                    </div>
                  </CCardBody>}
                </div>
                <div className={viaSMS ? "show" : "hide"}>
                  {viaSMS && <CCardBody>
                    <h1 className="text-center">{t('verification')}</h1>
                    <h5 className="text-center">{t('subtitle_fogotPassword_2')}</h5>
                    <div className="justify-content-center" id="submitOTP">
                      <form onSubmit={handleSubmit(onSubmitOTP)}>
                        <div className="text-center mb-3">
                          <input name="code1" autoComplete="off" id="code1" type="text" maxLength="1" className="input-code" onKeyPress={handleInputChange} ref={(e) => { register(e, { required: true }); code1Ref.current = e }} autoFocus />
                          <input name="code2" autoComplete="off" id="code2" type="text" maxLength="1" className="input-code" onKeyPress={handleInputChange} ref={register({ required: true })} />
                          <input name="code3" autoComplete="off" id="code3" type="text" maxLength="1" className="input-code" onKeyPress={handleInputChange} ref={register({ required: true })} />
                          <input name="code4" autoComplete="off" id="code4" type="text" maxLength="1" className="input-code" onKeyPress={handleInputChange} ref={register({ required: true })} />
                          <input name="code5" autoComplete="off" id="code5" type="text" maxLength="1" className="input-code" onKeyPress={handleInputChange} ref={register({ required: true })} />
                          <input name="code6" autoComplete="off" id="code6" type="text" maxLength="1" className="input-code" onKeyPress={handleInputChange} ref={register({ required: true })} />
                        </div>
                        <div className="text-center">
                          <div className="mb-2">
                            <CButton type="submit" color="primary" className="px-4 mr-2" size="lg">{t('verify')}</CButton>
                            <CButton color="secondary" className="px-4" size="lg" onClick={fogotpassword}>{t('cancel')}</CButton>
                          </div>
                          <div><span className="mr-1">{t('if_you_did_not_receive_a_code')}</span><CButton onClick={() => { handleResendClick(3000) }} color="link" className={"px-0 mb-1"}> {t('resend')}</CButton>{time !== 0 && <span className="ml-1"><CountDown timeCountdown={time} /></span>}</div>

                        </div>
                      </form>
                    </div>
                  </CCardBody>}
                </div>
                <div className={newPassword ? "show" : "hide"}>
                  {newPassword && <CCardBody>
                    <h1 className="text-center">{t('reset_account_password')}</h1>
                    <h4 className="text-center">{t('subtitle_fogotPassword_3')}</h4>
                    <div className="justify-content-center">
                      <form onSubmit={handleSubmit(onSubmitNewPassword)}>
                        <div className="mb-2">
                          <input
                            type="password"
                            className={!!errors.newPassword || error ? 'form-control is-invalid' : 'form-control'}
                            name="newPassword"
                            placeholder={t('password')}
                            aria-describedby="validationNewPassword"
                            autoComplete="off"
                            ref={register({
                              required: t('enter_password'),
                              minLength: {
                                value: 8,
                                message: t('password_must_be_at_least_8_characters')
                              }
                            })} />
                          {errors.newPassword && <div className="invalid-feedback">{errors.newPassword.message}</div>}
                        </div>
                        <div className="mb-3">
                          <input
                            type="password"
                            className={!!errors.confirm_password || error ? 'form-control is-invalid' : 'form-control'}
                            name="confirm_password"
                            placeholder={t('confirmPassword')}
                            aria-describedby="validationConfirmPassword"
                            autoComplete="off"
                            ref={register({
                              validate: value =>
                                value === newPasswordR.current || t('passwords_do_not_match')
                            })} />
                          {errors.confirm_password && <div className="invalid-feedback">{errors.confirm_password.message}</div>}
                        </div>
                        <button className="btn btn-primary float-right" type="submit">{t('verify')}</button>
                        <CButton onClick={() => { handleCancelClick() }} color="secondary mr-1 float-right">{t('cancel')}</CButton>
                      </form>
                    </div>
                  </CCardBody>}
                </div>
                {/* {viaEmail && <CCardBody>
                  <h1 className="text-center">Reset Password</h1>
                  <h5>A temporary has been send to your email. Please enter new password:</h5>
                  <Formik
                    initialValues={{ newPassword: '' }}

                    onSubmit={fields => onSubmitEmail(fields)}>
                    {({ errors, status, touched }) => (
                      <>
                        <Form>
                          <CFormGroup>
                            <CLabel>New Password</CLabel>
                            <Field name="newPassword" type="password" placeholder="********" className="form-control my-2"></Field>

                          </CFormGroup>
                          <div className="text-center">
                            <div>
                              <CButton type="submit" color="primary" className="px-4 mb-2 btn-w1" size="lg">Reset Password</CButton>
                            </div>
                            <div>
                              <CButton color="secondary" className="px-4 btn-w1" size="lg" onClick={fogotpassword}>Cancel</CButton>
                            </div>
                          </div>
                        </Form>
                      </>
                    )}
                  </Formik>
                </CCardBody>} */}
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default ForgotPassword
