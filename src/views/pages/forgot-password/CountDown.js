import React, { useState, useRef, useEffect } from 'react'
import Countdown from "react-countdown";

const CountDown = (props) => {
  const { timeCountdown } = props;
  console.log(timeCountdown);

  const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
      // Render a complete state
      return null;
    } else {
      // Render a countdown
      return (
        <span>
          {minutes * 60 + seconds}
        </span>
      );
    }
  };
  return (
    <Countdown date={Date.now() + timeCountdown} renderer={renderer} />
  )
}
export default CountDown
