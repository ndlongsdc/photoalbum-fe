import React from 'react'
import {
  CCol,
  CContainer,
  CRow
} from '@coreui/react'

const Share = () => {
  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="6">
            <a className="btn btn-secondary" href="app://sdc.udn.photobook">Test deep Linking</a>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Share
