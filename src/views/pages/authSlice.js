import { createSlice } from '@reduxjs/toolkit';
import authApi from '../../api/auth'
// import { history } from '../../helpers/history';
import { setToast, clearToast } from '../../custom_toast/toastSlice'
import { post } from 'axios';
const API_ENDPOINT = process.env.REACT_APP_API_URL;

const initial = {
  auths: [],
  error: '',
  loading: false,
  toastMessage: '',
  toastType: '',
  getOTP: false,
  getOTPWeb: false,
  checkOTP: '',
  submitOTP: '',
}
const auth = createSlice({
  name: 'auths',
  initialState: initial,
  reducers: {
    // reset: state => {
    //   state = initial
    // },
    reset: (state) => {
      state = initial
    },
    loginStart: (state, action) => {
      state.loading = true
      state.toastMessage = ''
      state.toastType = ''
    },
    loginSuccess: (state, action) => {
      state.loading = false
      state.auths = action.payload
      localStorage.setItem("auth:access_token", action.payload.access_token)
      localStorage.setItem("currentUser", JSON.stringify(action.payload.user))
      localStorage.setItem("currentBusiness", action.payload.business ? JSON.stringify(Object.assign({}, action.payload.business, { countFollow: action.payload.countFollow })) : '')
      state.error = ''
    },
    loginFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
      state.toastMessage = 'Email or password is incorrect'
      state.toastType = 'error'
    },

    logoutStart: (state, action) => {
      state.loading = true
    },
    logoutSuccess: (state, action) => {
      state.loading = false
      state.auths = action.payload
      // localStorage.clear();
      // history.push({pathname: '/' });
    },
    logoutFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },

    getOTPStart: (state, action) => {
      state.loading = true
    },
    getOTPSuccess: (state, action) => {
      state.loading = false
      state.auths = action.payload
      state.getOTP = true
    },
    getOTPFailure: (state, action) => {
      state.loading = false
      state.getOTP = false
    },
    getOTPWebSuccess: (state, action) => {
      state.loading = false
      state.auths = action.payload
      state.getOTPWeb = true
    },
    getOTPWebFailure: (state, action) => {
      state.loading = false
      state.getOTPWeb = false
    },

    checkOTPStart: (state, action) => {
      state.loading = true
    },
    checkOTPSuccess: (state, action) => {
      state.loading = false
      state.checkOTP = action.payload
    },
    checkOTPFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },

    submitOTPStart: (state, action) => {
      state.loading = true
    },
    submitOTPSuccess: (state, action) => {
      state.loading = false
      state.submitOTP = action.payload
    },
    submitOTPFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },

    changeAvatarStart: (state, action) => {
      state.loading = true
    },
    changeAvatarSuccess: (state, action) => {
      state.loading = false
      state.auths = action.payload
      localStorage.setItem('currentUser', JSON.stringify(action.payload))
    },
    changeAvatarFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },

    changeProfileStart: (state, action) => {
      state.loading = true
    },
    changeProfileSuccess: (state, action) => {
      state.loading = false
      state.auths = action.payload
      localStorage.setItem('currentUser', JSON.stringify(action.payload))
    },
    changeProfileFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },

    changePasswordStart: (state, action) => {
      state.loading = true
    },
    changePasswordSuccess: (state, action) => {
      state.loading = false
    },
    changePasswordFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },

    changeBusinessInfomationStart: (state, action) => {
      state.loading = true
    },
    changeBusinessInfomationSuccess: (state, action) => {
      state.loading = false;
      localStorage.setItem("currentBusiness", JSON.stringify(action.payload))
    },
    changeBusinessInfomationFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
  }
});

const { reducer, actions } = auth;
export const {
  loginStart, loginSuccess, loginFailure,
  logoutStart, logoutSuccess, logoutFailure,
  getOTPStart, getOTPSuccess, getOTPFailure,getOTPWebSuccess, getOTPWebFailure,
  checkOTPStart, checkOTPSuccess, checkOTPFailure,
  submitOTPStart, submitOTPSuccess, submitOTPFailure,
  changeAvatarStart, changeAvatarSuccess, changeAvatarFailure,
  changeProfileStart, changeProfileSuccess, changeProfileFailure,
  changePasswordStart, changePasswordSuccess, changePasswordFailure,
  changeBusinessInfomationStart, changeBusinessInfomationSuccess, changeBusinessInfomationFailure,
  reset
} = actions;
export const login = (params) => async dispatch => {
  try {
    dispatch(loginStart());
    const response = await authApi.login(params);
    if (response.success) {
      dispatch(loginSuccess(response.data))
      const valueToast = {
        message: response.message,
        type: 'success',
        status: response.code
      }
      dispatch(setToast(valueToast));
    } else {
      dispatch(loginFailure(response.message));
      // const valueToast = {
      //   message: response.message,
      //   type: 'error',
      //   status: response.code
      // }
      // dispatch(setToast(valueToast));
    }
  } catch (error) {
    dispatch(loginFailure('error'));
  }
  dispatch(clearToast());
}
export const logout = () => async dispatch => {
  try {
    dispatch(logoutStart());
    const response = await authApi.logout();
    dispatch(logoutSuccess(response));
    let valueToast = {
      message: response.message,
      type: 'success'
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(logoutFailure(error));
  }
  dispatch(clearToast());
  // dispatch(reset());

}
export const getOTP = (params) => async dispatch => {
  try {
    dispatch(getOTPStart());
    const response = await authApi.getOTP(params);
    // console.log(response);
    // dispatch(getOTPSuccess(response));
    // let valueToast = {
    //   message: response.message,
    //   type: 'success'
    // }
    // dispatch(setToast(valueToast));

    let valueToast = {}
    if (response.success === true) {
      params.web ? dispatch(getOTPWebSuccess(response)) : dispatch(getOTPSuccess(response));
      params.web ? valueToast = {
        message: `Email đã được gửi đến địa chỉ ${params.phone}. Vui lòng kiểm tra hộp thư đến`,
        type: 'success',
        time: 5000
      } : valueToast = {
        type: 'success'
      }
    } else {
      params.web ? dispatch(getOTPWebFailure('error')) :dispatch(getOTPFailure('error'));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(getOTPFailure("error"));
  }
  dispatch(clearToast());
}

export const checkOTP = (params) => async dispatch => {
  try {
    dispatch(checkOTPStart());
    const response = await authApi.checkOTP(params);
    let valueToast = {}
    if (response.success === true) {
      dispatch(checkOTPSuccess(response));
      valueToast = {
        type: 'success'
      }
    } else {
      dispatch(checkOTPFailure(response));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(submitOTPFailure("error"));
  }
  dispatch(clearToast());
}

export const submitOTP = (params) => async dispatch => {
  try {
    dispatch(submitOTPStart());
    const response = await authApi.submitOTP(params);
    let valueToast = {}
    if (response.success === true) {
      dispatch(submitOTPSuccess(response));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(submitOTPFailure(response));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(submitOTPFailure("error"));
  }
  dispatch(clearToast());
}

export const changeAvatar = (param) => async dispatch => {
  try {
    dispatch(changeAvatarStart());
    const formData = new FormData();
    formData.append("image", param);
    const url = API_ENDPOINT + '/users/avatar';
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: 'Bearer ' + localStorage.getItem("auth:access_token"),
        lang: localStorage.getItem("locale")
      }
    }
    const response = await post(url, formData, config)
    let valueToast = {}
    if (response.data.success === true) {
      dispatch(changeAvatarSuccess(response.data.data));
      valueToast = {
        message: response.data.message,
        type: 'success'
      }
    } else {
      dispatch(changeAvatarSuccess(response.data.data));
      // dispatch(changeAvatarFailure(response.data.errors));
      const messages = typeof response.data.message === 'array' ? Object.values(response.data.message) : response.data.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeAvatarFailure("error"));
  }
  dispatch(clearToast());
}

export const changeProfile = (params) => async dispatch => {
  try {
    params._method = "PUT";
    dispatch(changeProfileStart());
    const formData = new FormData();
    Object.keys(params).forEach(key => formData.append(key, params[key]));
    const url = API_ENDPOINT + '/users/change-profile';
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: 'Bearer ' + localStorage.getItem("auth:access_token"),
        lang: localStorage.getItem("locale")
      }
    }
    const response = await post(url, formData, config)
    let valueToast = {}
    if (response.data.success === true) {
      dispatch(changeProfileSuccess(response.data.data));
      valueToast = {
        message: response.data.message,
        type: 'success'
      }
    } else {
      dispatch(changeProfileFailure(response.data.errors));
      const messages = typeof response.data.message === 'array' ? Object.values(response.data.message) : response.data.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changeProfileFailure("error"));
  }
  dispatch(clearToast());
}


export const changePassword = (params) => async dispatch => {
  try {
    dispatch(changePasswordStart());
    const response = await authApi.changePassword(params);
    let valueToast = {}
    if (response.success === true) {
      dispatch(changePasswordSuccess(response));
      valueToast = {
        message: response.message,
        type: 'success'
      }
    } else {
      dispatch(changePasswordFailure(response));
      valueToast = {
        message: response.message,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    dispatch(changePasswordFailure("error"));
  }
  dispatch(clearToast());
}
export const changeBusinessInfomations = (body) => async dispatch => {
  const id = body.id
  delete body.id;
  body.latitudes = 0;
  body.longitudes = 0;
  try {
    dispatch(changeBusinessInfomationStart());
    body._method = "PUT";
    const formData = new FormData();
    Object.keys(body).forEach(key => formData.append(key, body[key]));
    const url = API_ENDPOINT + '/businesses/' + id;
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: 'Bearer ' + localStorage.getItem("auth:access_token"),
        lang: localStorage.getItem("locale")
      }
    }
    // for (var pair of formData.entries()) {
    //   console.log(pair[0] + ', ' + pair[1]);
    // }
    const response = await post(url, formData, config)
    let valueToast = {}
    if (response.data.success === true) {
      dispatch(changeBusinessInfomationSuccess(response.data.data.business));
      valueToast = {
        message: response.data.message,
        type: 'success'
      }
    } else {
      dispatch(changeBusinessInfomationFailure(response.data.errors));
      const messages = typeof response.data.message === 'array' ? Object.values(response.data.message) : response.data.message;
      valueToast = {
        message: messages,
        type: 'error'
      }
    }
    dispatch(setToast(valueToast));
  } catch (error) {
    console.log(error);
    dispatch(changeBusinessInfomationFailure('error'));
  }
  dispatch(clearToast());
}
export const resetAuth = () => async dispatch => {
  dispatch(reset());
}
export default reducer;
