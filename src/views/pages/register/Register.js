import React from 'react'
import { Formik, Field, Form, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import { MdMailOutline, MdPermIdentity, MdLockOutline } from "react-icons/md";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'

const Register = () => {
  return (
    <Formik
      initialValues={{
        username: '',
        email: '',
        password: '',
        repeatpassword: ''
      }}
      validationSchema={Yup.object().shape({
        username: Yup.string()
          .required('Username is required'),
        email: Yup.string()
          .email('Email is invalid')
          .required('Email is required'),
        password: Yup.string()
          .min(6, 'Password must be at least 6 characters')
          .required('Password is required'),
        repeatpassword: Yup.string()
          .oneOf([Yup.ref('password'), null], 'Passwords must match')
          .required('Confirm Password is required')
      })}
      onSubmit={fields => {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(fields, null, 4))
      }}>
      {({ errors, status, touched }) => (
        <div className="c-app c-default-layout flex-row align-items-center">
          <CContainer>
            <CRow className="justify-content-center">
              <CCol md="9" lg="7" xl="6">
                <CCard className="mx-4">
                  <CCardBody className="p-4">
                    <Form>
                      <h1>Register</h1>
                      <p className="text-muted">Create your account</p>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <MdPermIdentity/>
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <Field name="username" placeholder="Username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
                        <ErrorMessage name="username" component="div" className="invalid-feedback" />
                      </CInputGroup>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <MdMailOutline/>
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <Field name="email" type="email" placeholder="Email" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                        <ErrorMessage name="email" component="div" className="invalid-feedback" />
                      </CInputGroup>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <MdLockOutline />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <Field name="password" type="password" placeholder="Password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                        <ErrorMessage name="password" component="div" className="invalid-feedback" />
                      </CInputGroup>
                      <CInputGroup className="mb-4">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <MdLockOutline />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <Field name="repeatpassword" type="password" placeholder="Repeat password" className={'form-control' + (errors.repeatpassword && touched.repeatpassword ? ' is-invalid' : '')} />
                        <ErrorMessage name="repeatpassword" component="div" className="invalid-feedback" />
                      </CInputGroup>
                      <CButton type="submit" color="success" block>Create Account</CButton>
                    </Form>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
          </CContainer>
        </div>
      )}
    </Formik>
  )
}

export default Register
