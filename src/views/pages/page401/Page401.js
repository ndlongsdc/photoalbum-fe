import React from 'react'
import { useHistory } from 'react-router-dom';
import {
  CCol,
  CContainer,
  CRow,
} from '@coreui/react'

const Page404 = () => {
const history = useHistory();
  const redirect = () =>{
    history.push("/dashboard");
  }
  return (
    <>
    <div className="c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center pt-5">
          <CCol md="7">
            <div className="clearfix">
              <h1 className="float-left display-1 mr-4 font-weight-bold text-danger">401</h1>
              <h4 className="pt-2 display-4 text-danger">Oops! You{'\''}re lost.</h4>
              <p className="text-muted float-left">You are not authorized to access this page.</p>
            </div>
            <h4 className="float-right text-primary cursor-pointer" onClick={()=>redirect()} ><i className="fas fa-arrow-left mr-2"></i>Return to home page</h4>
          </CCol>
        </CRow>
      </CContainer>
    </div>
    </>
  )
}

export default Page404
