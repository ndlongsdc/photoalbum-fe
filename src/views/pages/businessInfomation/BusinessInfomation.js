import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  CButton, CCard, CCardBody, CCol, CRow, CFormGroup, CSpinner, CLabel,
  CTabs, CNav, CNavItem, CNavLink, CTabContent, CTabPane
} from '@coreui/react'
import { changeBusinessInfomations } from '../authSlice'
import '../businessInfomation/style.scss'
import CIcon from '@coreui/icons-react'
import InputField from '../../../custom_fields/inputField';
import TextareaField from '../../../custom_fields/textareaField';
import selectField from '../../../custom_fields/selectField';
import { Link } from 'react-router-dom'
import * as Yup from 'yup'
import { Formik, FastField, Form } from 'formik'
import { useTranslate } from 'react-redux-multilingual'

const API_ENDPOINT = process.env.REACT_APP_API_URL_IMAGE;
const ListGroups = () => {
  const t = useTranslate()
  const dispatch = useDispatch();
  const [preview, setPreview] = useState()
  const [avatar, setAvatar] = useState()
  const data = useSelector(state => state.auths.auths.data);
  const businessTypes = useSelector(state => state.businessTypes.businessTypes);
  // dispatch(fetchBusinessTypes());
  const dataBusiness = data ? data[0] : JSON.parse(localStorage.getItem('currentBusiness'))
  const valueForm = JSON.parse(JSON.stringify(dataBusiness))
  valueForm.translations.map((obj) => {
    if (obj.language_id === "vi" || obj.language_id === 1) {
      valueForm.nameVi = obj.name;
      valueForm.addressVi = obj.address;
      valueForm.descriptionVi = obj.description;
    } else if (obj.language_id === "en" || obj.language_id === 3) {
      valueForm.nameEn = obj.name;
      valueForm.addressEn = obj.address;
      valueForm.descriptionEn = obj.description;
    }
  })
  const validationSchema = Yup.object().shape({
    nameVi: Yup.string()
      .required('Name is required'),
    addressVi: Yup.string()
      .required('Address is required'),
    nameEn: Yup.string()
      .required('Name is required'),
    addressEn: Yup.string()
      .required('Address is required'),
    phone: Yup.string()
      .required('Phone is required'),
  })

  // useEffect(()=>{
  // },[dispatch||businessTypes])

  let businessTypesOption = [];
  businessTypes.length!==0 && businessTypes.map((obj) => {
    businessTypesOption.push({ "label": obj.name, "value": obj.id })
    return null;
  })
  businessTypesOption.length !== 0 && localStorage.setItem("businessTypesOption", JSON.stringify(businessTypesOption));
  businessTypesOption = businessTypesOption.length !== 0 ? businessTypesOption : JSON.parse(localStorage.getItem("businessTypesOption"));
  useEffect(() => {
    if (!avatar) {
      setPreview(undefined)
      return
    }

    const objectUrl = URL.createObjectURL(avatar)
    setPreview(objectUrl)

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl)
  }, [avatar])
  const onChangeAvatar = (value) => {
    setAvatar(value);
  }
  const onSubmitBusinessInfomation = (value) => {
    dispatch(changeBusinessInfomations(value))
  }
  return (
    <>
      <div className="">
        {t('overview')}
      </div>
      <h2 className="mb-3">
        {t('businessInfomation')}
      </h2>
      <div className="row">
        <div className="col-lg-4">
          <div className="card card-small pt-3">
            <div className="card-header border-bottom text-center">
              <div className="mb-3 mx-auto">
                <img className="rounded-circle" src={preview || API_ENDPOINT + dataBusiness.featured_image} alt="Business Avatar" width="110" />
                {/* {avatar &&  <img src={preview} /> } */}
              </div>
              <div className="row text-center justify-content-md-center">
                <div className="col-4">
                  <h4>{dataBusiness.rating}</h4>
                  <span className="text-muted d-block">
                    {t('rating')}
                  </span>
                </div>
                <div className="col-4">
                  <h4>{dataBusiness.view}</h4>
                  <span className="text-muted d-block">
                    {t('view')}
                  </span>
                </div>
                <div className="col-4">
                  <h4>{dataBusiness.countFollow}</h4>
                  <span className="text-muted d-block mb-2">
                    {t('follow')}
                  </span>
                </div>
                {/* <div className="upload-btn-wrapper">
                  <button type="button" className="mb-2 btn btn-sm btn-pill btn-outline-primary mr-2 cursor-pointer">
                    <i className="fas fa-camera"></i> {t('change')}</button>
                  <input type="file" name="myfile" onChange={(value) => onChangeAvatar(value.currentTarget.files[0])} />
                </div> */}
              </div>
            </div>
            <ul className="list-group list-group-flush">
              <p className="text-center py-3">
                <CButton className="btn-facebook btn-brand mx-1"><CIcon name="cib-facebook" /></CButton>
                <CButton className="btn-twitter btn-brand mx-1"><CIcon name="cib-twitter" /></CButton>
                <CButton className="btn-instagram btn-brand mx-1"><CIcon name="cib-instagram" /></CButton>
              </p>
            </ul>
          </div>
        </div>
        <div className="col-lg-8">
          <div className="card card-small mb-4">
            <div className="card-header border-bottom">
              <h6 className="m-0">{t('accountId')}: {dataBusiness.id} </h6>
            </div>
            <ul className="list-group list-group-flush">
              <li className="list-group-item p-3">
                <>
                  <Formik
                    enableReinitialize
                    initialValues={
                      {
                        id: valueForm.id || '',
                        nameVi: valueForm.nameVi || '',
                        addressVi: valueForm.addressVi || '',
                        descriptionVi: valueForm.descriptionVi || '',
                        nameEn: valueForm.nameEn || '',
                        addressEn: valueForm.addressEn || '',
                        descriptionEn: valueForm.descriptionEn || '',
                        phone: valueForm.phone || '',
                        open_hour: valueForm.open_hour || '',
                        close_hour: valueForm.close_hour || '',
                        type: valueForm.type,
                      }
                    }
                    validationSchema={validationSchema}
                    onSubmit={(value) => { onSubmitBusinessInfomation(value) }
                      // props.onSubmit
                    }
                  >
                    {formikProps => {
                      const { isSubmitting } = formikProps;
                      const { submitCount } = formikProps;
                      const { errors } = formikProps;
                      const errorTabVi = errors.addressVi || errors.nameVi || errors.phone;
                      const errorTabEn = errors.addressEn || errors.nameEn || errors.phone;
                      const flickerVi = errorTabVi && submitCount;
                      const flickerEn = errorTabEn && submitCount;
                      const checkErrTab = (value) => {
                        if (value) {
                          return "blink-bg";
                        } else {
                          return "";
                        }
                      }
                      return (
                        <>

                          <div style={{ height: "100%", width: "100%" }}>
                            <div id="map"></div>
                          </div>
                          <Form className="form-horizontal">
                            <CTabs activeTab="vi">
                              <CNav variant="tabs">
                                <CNavItem>
                                  <CNavLink data-tab="vi" className={checkErrTab(flickerVi)}>
                                    {t('vietnamese')} {errorTabVi && <span className="text-danger">*</span>}
                                  </CNavLink>
                                </CNavItem>
                                <CNavItem>
                                  <CNavLink data-tab="en" className={checkErrTab(flickerEn)}>
                                    {t('english')} {errorTabEn && <span className="text-danger">*</span>}
                                  </CNavLink>
                                </CNavItem>
                              </CNav>
                              <CTabContent style={{ marginBottom: "-45px" }}>
                                <CTabPane data-tab="vi">
                                  <CCard style={{ border: 0 }}>
                                    <CCardBody>
                                      <CFormGroup>
                                        <FastField
                                          name="nameVi"
                                          component={InputField}
                                          label={t('name')}
                                          placeholder={t('name')}
                                          requiredlabel
                                        />
                                      </CFormGroup>
                                      <CFormGroup>
                                        <FastField
                                          name="addressVi"
                                          component={InputField}
                                          label={t('address')}
                                          placeholder={t('address')}
                                          requiredlabel
                                        />
                                      </CFormGroup>
                                      <CFormGroup>
                                        <FastField
                                          name="descriptionVi"
                                          component={TextareaField}
                                          label={t('description')}
                                          placeholder={t('description')}
                                        />
                                      </CFormGroup>
                                    </CCardBody>
                                  </CCard>
                                </CTabPane>
                                <CTabPane data-tab="en">
                                  <CCard style={{ border: 0 }}>
                                    <CCardBody>
                                      <CFormGroup>
                                        <FastField
                                          name="nameEn"
                                          component={InputField}
                                          label={t('name')}
                                          placeholder={t('name')}
                                          requiredlabel
                                        />
                                      </CFormGroup>
                                      <CFormGroup>
                                        <FastField
                                          name="addressEn"
                                          component={InputField}
                                          label={t('address')}
                                          placeholder={t('address')}
                                          requiredlabel
                                        />
                                      </CFormGroup>
                                      <CFormGroup>
                                        <FastField
                                          name="descriptionEn"
                                          component={TextareaField}
                                          label={t('description')}
                                          placeholder={t('description')}
                                        />
                                      </CFormGroup>
                                    </CCardBody>
                                  </CCard>
                                </CTabPane>
                              </CTabContent>
                            </CTabs>
                            <div style={{ margin: "0 1.2rem 0 1.3rem" }}>
                              <CFormGroup row>
                                <CCol md="2">
                                  <CLabel htmlFor="featured_image">{t('image')}</CLabel>
                                </CCol>
                                <CCol xs="12" md="10">
                                  <input
                                    name="featured_image"
                                    type="file"
                                    onChange={(event) => {
                                      formikProps.setFieldValue("featured_image", event.currentTarget.files[0])
                                      onChangeAvatar(event.currentTarget.files[0])
                                    }}
                                    className={`${formikProps.errors.featured_image ? "is-invalid" : ""}`}
                                  />
                                  <div className="text-danger" style={{ fontSize: '80%' }} >{formikProps.errors.featured_image}</div></CCol>
                              </CFormGroup>
                              <CFormGroup>
                                <FastField
                                  name="phone"
                                  component={InputField}
                                  label={t('phone')}
                                  placeholder={t('phone')}
                                  requiredlabel
                                />
                              </CFormGroup>
                              <CRow>
                                <CCol md="2"><CLabel htmlFor='open_hour'>{t('open')}</CLabel></CCol>
                                <CCol md="4" lg="4" xl="3">
                                  <CFormGroup>
                                    <FastField
                                      type="time"
                                      component={InputField}
                                      className='form-control'
                                      name="open_hour" />
                                  </CFormGroup>
                                </CCol>
                                <CCol style={{ maxWidth: "100px" }} className="mt-2">{t('to')}</CCol>
                                <CCol md="4" lg="4" xl="3">
                                  <CFormGroup>
                                    <FastField
                                      type="time"
                                      component={InputField}
                                      className='form-control'
                                      name="close_hour" />
                                  </CFormGroup>
                                </CCol>
                              </CRow>
                              <CFormGroup>
                                <FastField
                                  name="type"
                                  component={selectField}
                                  options={businessTypesOption}
                                  placeholder={t('type')}
                                  label={t('type')}
                                />
                              </CFormGroup>
                            </div>
                            <div className="form-actions float-right">
                            <CButton type="submit" color={'success'}>{t('edit')}</CButton>
                              <Link to={{ pathname: `/business/businesses` }}>
                                <CButton color="secondary ml-1">{t('cancel')}</CButton>
                              </Link>
                            </div>
                          </Form>
                        </>
                      )
                    }}
                  </Formik>
                </>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}

export default ListGroups
