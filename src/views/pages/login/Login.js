import React, { useEffect } from 'react'
import { login } from '../authSlice';
import { fetchBusinessTypes } from 'features/business/BusinessType/businessTypeSlice';
import { fecthCurrentPermissions } from 'features/management/Role/roleSlice';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom'
import { Formik, Field, Form, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import '../login/style.css'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CSpinner,
  CAlert
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useTranslate } from 'react-redux-multilingual'
// import { Label } from 'semantic-ui-react'

const Login = (props) => {
  const dispatch = useDispatch();
  const t = useTranslate()
  // const history = useHistory();
  const loading = useSelector(state => state.auths.loading)
  const toast = useSelector(state => state.auths.toastMessage)
  const userRoles = useSelector(state => state.auths.auths.user?.role)
  const dataBusinessTypes = useSelector(state => state.businessTypes.businessTypes)
  const token = localStorage.getItem('auth:access_token');
  if (userRoles && token) {
    let userRoleIds = Object.keys(userRoles).toString();
    dispatch(fecthCurrentPermissions(userRoleIds));
    dataBusinessTypes.length === 0 && dispatch(fetchBusinessTypes());
  }

  if (token) {
    props.history.push({ pathname: '/dashboard' });
  }
  // useEffect(() => {
  //   if (auths.success) {
  //     props.history.push({ pathname: '/dashboard' });
  //   }
  // }, [auths.success])
  const onLogin = (fields) => {
    dispatch(login(fields))
  }
  // if(auths.success){props.history.push({pathname: '/dashboard' });}
  var regExPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;
  var regExEmail = /\S+@\S+\.\S+/;
  return (
    <Formik
      initialValues={{
        email: '',
        password: '',
        // rememberme: ''
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string()
          // .min(6, 'Email is invalid')
          // .email('Email is invalid')
          .required(t('email_required')),
        password: Yup.string()
          .min(8, t('password_must_be_at_least_8_characters'))
          .required(t('password_required'))
      })}
      onSubmit={fields => { onLogin(fields) }}>
      {formikProps => {
        const { errors, status, touched } = formikProps;
        var error = false;
        var phone_email = formikProps.values.email;

        if (isNaN(Math.floor(formikProps.values.email))&&formikProps.values.email.includes("@")) {
          error = regExEmail.test(phone_email);
          if (!error) errors.email = t('email_invalid');
        } else if (Math.floor(formikProps.values.email) > 0) {
          error = regExPhone.test(phone_email);
          if (!error) errors.email = t('phone_invalid');
        }
        return (
          // )
          // ({ errors, status, touched }) => (
          <div className="c-app c-default-layout flex-row align-items-center">
            <CContainer>
              <CRow className="justify-content-center">
                <CCol md="6">
                  <CCardGroup>
                    <CCard className="p-4">
                      <CCardBody>
                        <Form>
                          <h1 className="text-center">{t('login')}</h1>
                          {toast && <CAlert color="danger">
                            {toast}
                          </CAlert>}
                          <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                              <CInputGroupText>
                                <CIcon name="cil-user" />
                              </CInputGroupText>
                            </CInputGroupPrepend>
                            <Field name="email" type="text" placeholder="Email" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                            {/* <ErrorMessage name="email" component="div" className="invalid-feedback" /> */}
                            {!error && <div className="invalid-feedback">{errors.email}</div>}
                          </CInputGroup>
                          <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                              <CInputGroupText>
                                <CIcon name="cil-lock-locked" />
                              </CInputGroupText>
                            </CInputGroupPrepend>
                            <Field name="password" type="password" placeholder={t('password')} className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                            <ErrorMessage name="password" component="div" className="invalid-feedback" />
                          </CInputGroup>
                          <div className="form-check mb-3">
                            <input name="rememberme" type="checkbox" className="form-check-input" id="exampleCheck1" />
                            <label className="form-check-label" htmlFor="exampleCheck1">{t('remember_me')}</label>
                          </div>
                          <div className="text-center">
                            <CButton type="submit" color="primary" className="px-4" disabled={loading}>{loading && <CSpinner size="sm" className="mr-1" />} {t('login')}</CButton>
                          </div>
                          <div className="text-center">
                            <Link to="/forgotpassword">
                              <CButton color="link" className="px-0">{t('forgot_your_password')}</CButton>
                            </Link>
                          </div>
                        </Form>
                      </CCardBody>
                    </CCard>
                  </CCardGroup>
                </CCol>
              </CRow>
            </CContainer>
          </div>
        )
      }}
    </Formik>

  )
}

export default Login
