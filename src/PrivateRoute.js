import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function PrivateRoute({ component: Component, role, ...rest }) {
  const filterItems = (needle, heystack) => {
    let query = needle.toLowerCase();
    return heystack?.filter(item => item.toLowerCase().indexOf(query) >= 0);
  }
  return (
    <Route {...rest} render={props => {
      let locationComponent = props.location.pathname;
      const curentPermissions = localStorage.getItem('curentPermissions')?.split(',');
      curentPermissions && curentPermissions.map((obj) => {
        if (obj.includes('*') && locationComponent.split('/').length === 4) {
          locationComponent = locationComponent.substring(0, locationComponent.lastIndexOf('/')) + "*"
        }
        else if (obj.includes('*') && locationComponent.split('/').length === 3) {
          locationComponent = locationComponent + "*"
        }
        if (obj.split('/').length === 2) {
          locationComponent = locationComponent.substring(0, locationComponent.indexOf('/', 1)) + "*"
        }
        locationComponent = locationComponent.replace('**', '*')
      })
      const allowLocation = !!filterItems(locationComponent, curentPermissions)?.length || curentPermissions?.includes("*");

      if (props.location.pathname === "/permissions" || props.location.pathname === "/page401" || props.location.pathname === "/dashboard"|| props.location.pathname === "/forgotpassword"|| props.location.pathname === "/profile") {
        return <Component {...props} />
      }
      else if (!localStorage.getItem('auth:access_token')) {
        localStorage.clear();
        return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      }
      else if (allowLocation === true) {
        return <Component {...props} />
      } else {
        return <Redirect to={{ pathname: '/page401', state: { from: props.location } }} />
      }
    }} />
  );
}

export { PrivateRoute };
