import { ErrorMessage } from 'formik';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import MultiSelect from "react-multi-select-component";
import { CFormGroup, CLabel, CCol } from '@coreui/react';
import { useTranslate } from 'react-redux-multilingual'

MultiSelectField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.array,
};

MultiSelectField.defaultProps = {
  label: '',
  placeholder: '',
  disabled: false,
  options: [],
}

function MultiSelectField(props) {
  const { field, form, options, label, requiredLabel, placeholder } = props;
  const t = useTranslate()
  const { initialValues } = form;
  const { name } = field;
  const temp = initialValues.role || [];
  initialValues[name].map((obj) => {
    const o = options.find(x => x.value === obj);
    o !== undefined && temp.push(o);
    return null;
  });
  // const res = options.find(x => x.value === 1);
  const [selected, setSelected] = useState(temp);

  useEffect(() => {
    const selectedValue = selected;
    const arrValue = [];
    selectedValue.map((obj) => {
      arrValue.push(obj.value)
      return null;
    });
    const changeEvent = {
      target: {
        name: name,
        value: arrValue
      }
    };
    field.onChange(changeEvent);

  }, [selected])
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];

  if (!selected.length && document.getElementsByClassName('dropdown-heading-value')[0]) {
    document.getElementsByClassName('dropdown-heading-value')[0].children[0].innerHTML = placeholder;
  }
  // console.log(!selected.length && document.getElementsByClassName('dropdown-heading')[0]);
  // const dropdown = document.getElementsByClassName('dropdown-container');
  // useEffect(() => {
  //   document.getElementsByClassName('dropdown-container')[0].addEventListener("click", function () {

  //   })
  // }, [dropdown])
  return (
    <CFormGroup row>
      {label && <CCol md="2"><CLabel htmlFor={name}>{label}</CLabel>{requiredLabel && <span className="text-danger">*</span>}</CCol>}
      <CCol xs="12" md="10">
        <MultiSelect
          id={name}
          {...field}
          options={options}
          onChange={setSelected}
          value={selected}
          disableSearch={true}
          className={showError ? 'is-invalid' : ''}
        />
        {showError && <ErrorMessage name={name} component="div" className="invalid-feedback" />}
      </CCol>
    </CFormGroup>
  );
}

export default MultiSelectField;
