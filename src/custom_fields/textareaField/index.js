import PropTypes from 'prop-types';
import React from 'react';
import { CFormGroup, CTextarea, CLabel, CCol } from '@coreui/react';
import { ErrorMessage } from 'formik';

TextareaField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
};

TextareaField.defaultProps = {
  type: 'text',
  label: '',
  placeholder: '',
  disabled: false,
}

function TextareaField(props) {
  const {
    field, form,
    type, label, placeholder, disabled, requiredlabel, rows
  } = props;
  const { name } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];

  return (
    <CFormGroup row>
      {label && <CCol md="2"><CLabel htmlFor={name}>{label}</CLabel>{requiredlabel && <span className="text-danger">*</span>}</CCol>}
      <CCol xs="12" md="10">
        <CTextarea
          id={name}
          {...field}

          type={type}
          disabled={disabled}
          placeholder={placeholder}
          // requiredLabel={requiredLabel}
          rows={rows || 5}
          invalid={showError}
        />
        {showError && <ErrorMessage name={name} component="div" className="invalid-feedback" />}
      </CCol>
    </CFormGroup>
  );
}

export default TextareaField;
