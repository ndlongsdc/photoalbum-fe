import { ErrorMessage } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';
import { CFormGroup, CLabel, CCol } from '@coreui/react';

RadioButtonField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.array,
};

RadioButtonField.defaultProps = {
  label: '',
  placeholder: '',
  disabled: false,
  options: [],
}

function RadioButtonField(props) {
  const { field, form, options, label } = props;
  const { name } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];

  return (
    <CFormGroup row>
      {label && <CCol md="2"><CLabel htmlFor={name}>{label}</CLabel></CCol>}
      <CCol xs="12" md="10">
        {options.map(option => {
          return (
            <React.Fragment key={option.key}>
              <input
                className='mr-2'
                type='radio'
                id={option.key}
                {...field}
                value={option.value}

                checked={field.value === option.value}
              />
              <label htmlFor={option.key} className='mr-5'>{option.key}</label>
            </React.Fragment>
          )
        })}
        {showError && <ErrorMessage name={name} component="div" className="invalid-feedback" />}
      </CCol>
    </CFormGroup>
  );
}

export default RadioButtonField;
