import { ErrorMessage, FastField } from 'formik';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { CFormGroup, CLabel, CCol } from '@coreui/react';

RadioButtonField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.array,
};

RadioButtonField.defaultProps = {
  label: '',
  placeholder: '',
  disabled: false,
  options: [],
}

function RadioButtonField(props) {
  const { field, form, label, requiredlabel } = props;
  const { name } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];
  const [cheked, setChecked] = useState(field.value ? field.value.toString() : "0");
  useEffect(() => {
    const changeEvent = {
      target: {
        name: name,
        value: cheked
      }
    };
    field.onChange(changeEvent);
  })
  return (
    <CFormGroup row>
      {label && <CCol md="2"><CLabel htmlFor={name}>{label}</CLabel>{requiredlabel && <span className="text-danger">*</span>}</CCol>}
      <CCol xs="12" md="10">
        <FastField
          type='checkbox'
          {...field}
          checked={cheked==="1"}
          onChange={() => setChecked(field.value === "1" ? "0" : "1")}
          style={{width:"20px",height:"20px"}}
        />
        {showError && <ErrorMessage name={name} component="div" className="invalid-feedback" />}
      </CCol>
    </CFormGroup>
  );
}

export default RadioButtonField;
