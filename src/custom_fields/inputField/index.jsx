import PropTypes from 'prop-types';
import React from 'react';
import { CFormGroup, CInput, CLabel, CCol } from '@coreui/react';
import { ErrorMessage } from 'formik';
import { useTranslate } from 'react-redux-multilingual'

InputField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
};

InputField.defaultProps = {
  type: 'text',
  label: '',
  placeholder: '',
  disabled: false,
}

function InputField(props) {
  const t = useTranslate()
  const {
    field, form,
    type, label, placeholder, disabled, requiredlabel, autoCapitalize, checkMatch, checkMin
  } = props;
  const { name } = field;
  const { errors, touched, values } = form;
  const showError = errors[name] && touched[name];
  let checkMatchErr = true;
  let checkMinErr = true;
  let errorMessage = "";
  autoCapitalize && field.onChange({
    target: {
      name: name,
      value: field.value.toUpperCase()
    }
  });
  if (checkMatch) {
    if (values[checkMatch] && values[checkMatch] === field.value) {
      checkMatchErr = true;
    } else if (values[checkMatch] !== field.value) {
      checkMatchErr = false;
      errorMessage = t('passwords_do_not_match');
    }
  }
  if (checkMin) {
    if (field.value && checkMin === field.value.length) {
      checkMinErr = true;
    } else if (field.value &&checkMin > field.value.length) {
      checkMinErr = false;
      errorMessage = t('password_must_be_at_least_8_characters');
    }
  }
  return (
    <CFormGroup row>
      {label && <CCol md="2"><CLabel htmlFor={name}>{label}</CLabel>{requiredlabel && <span className="text-danger">*</span>}</CCol>}
      <CCol xs="12" md="10">
        <CInput
          id={name}
          {...field}
          // value={autoCapitalize ? field.value.toUpperCase() : field.value}
          type={type}
          disabled={disabled}
          placeholder={placeholder}
          autoComplete={"new-" + name}

          invalid={showError || !checkMatchErr || !checkMinErr}
        />
        {showError && <ErrorMessage name={name} component="div" className="invalid-feedback" />}
        {!checkMatchErr && <div className="invalid-feedback">{t('passwords_do_not_match')}</div>}
        {!checkMinErr && <div className="invalid-feedback">{t('password_must_be_at_least_8_characters')}</div>}
      </CCol>
    </CFormGroup>
  );
}

export default InputField;
