import { ErrorMessage } from 'formik';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import { CFormGroup, CLabel, CCol } from '@coreui/react';
import { useDispatch, useSelector } from 'react-redux';
import { listAddUserEmployee } from '../../features/business/Business/businessSlice';

SelectField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.array,
};

SelectField.defaultProps = {
  label: '',
  placeholder: '',
  disabled: false,
  options: [],
}

function SelectField(props) {
  const dispatch = useDispatch();
  const dataUser = useSelector(state => state.businesses.userEmployee)
  const [perPage, setPerPage] = useState(10)
  const { field, form, options, label, placeholder, disabled, requiredlabel, searchApi } = props;
  const { name, value } = field;
  const { errors, touched } = form;
  const showError = errors[name] && touched[name];

  const status = value;
  // const status = value === true ? 1 : value === false ? 0 : value;
  const selectedOption = options.find(option => option.value === status);
  let listUser = []
    useEffect(()=>{
    dispatch(listAddUserEmployee());
    },[])
  if (searchApi) {
    console.log(dataUser)
    if (dataUser) {
      dataUser.map((obj) => {
        listUser.push({ value: obj.id, label: obj.fullname })
      })
    }
  }
  // const onMenuScrollToBottom = () => {
  //   if (dataUser.total < dataUser.perPage) {
  //     setPerPage(perPage + 10)
  //     dispatch(listAddUserEmployee(perPage + 10));
  //     if (dataUser.data) {
  //       dataUser.data.map((obj) => {
  //         listUser.push({ value: obj.id, label: obj.fullname })
  //       })
  //     }
  //   }
  // }

  const handleSelectedOptionChange = (selectedOption) => {
    const selectedValue = selectedOption ? selectedOption.value : selectedOption;

    const changeEvent = {
      target: {
        name: name,
        value: selectedValue
      }
    };
    field.onChange(changeEvent);
  }

  return (
    <CFormGroup row>
      {label && <CCol md="2"><CLabel htmlFor={name}>{label}</CLabel>{requiredlabel && <span className="text-danger">*</span>}</CCol>}
      <CCol xs="12" md="10">
        <Select
          id={name}
          {...field}
          value={selectedOption}
          onChange={handleSelectedOptionChange}
          // onMenuScrollToBottom={() => onMenuScrollToBottom()}
          // onKeyDown={(v) => handleInputChange(v)}
          placeholder={placeholder}
          isDisabled={disabled}
          options={searchApi ? listUser : options}

          className={showError ? 'is-invalid' : ''}
        />
        {showError && <ErrorMessage name={name} component="div" className="invalid-feedback" />}
      </CCol>
    </CFormGroup>
  );
}

export default SelectField;
